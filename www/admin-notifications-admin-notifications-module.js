(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin-notifications-admin-notifications-module"],{

/***/ "3DbA":
/*!*******************************************************************************!*\
  !*** ./src/app/components/updatenotification/updatenotification.component.ts ***!
  \*******************************************************************************/
/*! exports provided: UpdatenotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatenotificationComponent", function() { return UpdatenotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_updatenotification_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./updatenotification.component.html */ "sUn6");
/* harmony import */ var _updatenotification_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./updatenotification.component.scss */ "WjV5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _services_push_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/push.service */ "H+l1");
/* harmony import */ var _services_img_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/img.service */ "t3Fp");









let UpdatenotificationComponent = class UpdatenotificationComponent {
    constructor(pushService, toastController, firestore, modalController, loadingController, imgService) {
        this.pushService = pushService;
        this.toastController = toastController;
        this.firestore = firestore;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.imgService = imgService;
        this.validationRules = {
            required: ['name', 'description', 'order', 'priority', 'state'],
        };
        this.file = null;
    }
    ngOnInit() {
        this.infocategory();
        this.infoOrder();
        this.infoPriority();
    }
    infocategory() {
        this.firestore
            .collection('/category', (ref) => ref.where('state', '==', 'public'))
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.Categoris = res.map((e) => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        description: e.payload.doc.data()['description'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    infoPriority() {
        this.firestore
            .collection('/priority', (ref) => ref.where('state', '==', 'public').orderBy('name', 'asc'))
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.Priorities = res.map((e) => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    infoOrder() {
        this.firestore
            .collection('/order', (ref) => ref.where('state', '==', 'public').orderBy('value', 'asc'))
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.Orders = res.map((e) => {
                    return {
                        id: e.payload.doc.id,
                        value: e.payload.doc.data()['value'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    updateNotification() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.file !== null) {
                this.notification.img = yield this.imgService.uploadImage('notification', this.file);
            }
            this.notification.updateDate = firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].firestore.Timestamp.now().toDate();
            this.firestore
                .doc('/notification/' + this.notification.uid)
                .update(this.notification)
                .then(() => {
                this.mensaje("Notificación actualizada correctamente");
                this.quit();
                this.mensaje('Noticia Actualizada');
            });
        });
    }
    quit() {
        this.modalController.dismiss();
    }
    enviar() {
        this.pushService.onesignal(this.notification.name, this.notification.name, this.notification.description, this.notification.uid);
        this.quit();
        this.mensaje('Notificación enviada');
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000,
            });
            toast.present();
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.notification.img = event.target.result;
            this.file = $event.target.files[0];
            this.fileName = $event.target.files[0].name;
        };
    }
};
UpdatenotificationComponent.ctorParameters = () => [
    { type: _services_push_service__WEBPACK_IMPORTED_MODULE_7__["PushService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _services_img_service__WEBPACK_IMPORTED_MODULE_8__["ImgService"] }
];
UpdatenotificationComponent.propDecorators = {
    notification: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
UpdatenotificationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-updatenotification',
        template: _raw_loader_updatenotification_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_updatenotification_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpdatenotificationComponent);



/***/ }),

/***/ "CwMn":
/*!*******************************************************************!*\
  !*** ./src/app/admin-notifications/admin-notifications.module.ts ***!
  \*******************************************************************/
/*! exports provided: AdminNotificationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminNotificationsPageModule", function() { return AdminNotificationsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _admin_notifications_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin-notifications-routing.module */ "H6Gf");
/* harmony import */ var _admin_notifications_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./admin-notifications.page */ "n1xm");
/* harmony import */ var _components_updatenotification_updatenotification_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/updatenotification/updatenotification.component */ "3DbA");








let AdminNotificationsPageModule = class AdminNotificationsPageModule {
};
AdminNotificationsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _admin_notifications_routing_module__WEBPACK_IMPORTED_MODULE_5__["AdminNotificationsPageRoutingModule"]
        ],
        entryComponents: [_components_updatenotification_updatenotification_component__WEBPACK_IMPORTED_MODULE_7__["UpdatenotificationComponent"]],
        declarations: [_components_updatenotification_updatenotification_component__WEBPACK_IMPORTED_MODULE_7__["UpdatenotificationComponent"], _admin_notifications_page__WEBPACK_IMPORTED_MODULE_6__["AdminNotificationsPage"]]
    })
], AdminNotificationsPageModule);



/***/ }),

/***/ "GCLn":
/*!*******************************************************************!*\
  !*** ./src/app/admin-notifications/admin-notifications.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos de l banner  */\n\n.banner {\n  padding: 0px 5%;\n  margin-top: 2%;\n}\n\n.banner .card-banner {\n  width: 100%;\n  height: 210px;\n  border-radius: 21px;\n  margin: 10px;\n  background-repeat: no-repeat;\n  background-size: cover;\n  color: white;\n  position: relative;\n  z-index: 1;\n}\n\n.banner .card-banner ion-card-header {\n  color: white;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.banner .card-banner ion-card-header .container-name {\n  display: flex;\n  align-items: center;\n}\n\n.banner .card-banner ion-card-header .container-name p {\n  margin: 0px;\n}\n\n.banner .card-banner ion-card-header .container-name ion-icon {\n  color: black;\n  font-size: 30px;\n}\n\n.banner .card-banner ion-card-header .pencil {\n  font-size: 20px;\n  margin-left: 10px;\n}\n\n.banner .card-banner ion-card-content {\n  margin-top: 5%;\n}\n\n.banner .card-banner::after {\n  content: \"\";\n  position: absolute;\n  background: rgba(0, 0, 0, 0.5);\n  width: 100%;\n  height: 100%;\n  top: 0px;\n  right: 0px;\n  left: 0px;\n  bottom: 0px;\n  z-index: -1;\n}\n\n.container-time {\n  display: flex;\n  align-items: center;\n  justify-content: space-around;\n}\n\n.container-time p {\n  margin: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGFkbWluLW5vdGlmaWNhdGlvbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBQTtFQUNBLHdDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQUFKOztBQUVBO0VBQ00sY0FBQTtBQUNOOztBQUNBLHdCQUFBOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFFSjs7QUFBSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBRVI7O0FBQUk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBRVI7O0FBRUEsc0NBQUE7O0FBQ0E7RUFDSSxzQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFBSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFFUjs7QUFBSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUVBLG9DQUFBOztBQUNBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFBSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUVBLHlCQUFBOztBQUNBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFSTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFBUjs7QUFDUTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQUNaOztBQUFZO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FBRWhCOztBQURnQjtFQUNJLFdBQUE7QUFHcEI7O0FBRGdCO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUFHcEI7O0FBRVk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUFBaEI7O0FBSVE7RUFDSSxjQUFBO0FBRlo7O0FBTUk7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSw4QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUFKUjs7QUFRQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0FBTEo7O0FBTUk7RUFFSSxXQUFBO0FBTFIiLCJmaWxlIjoiYWRtaW4tbm90aWZpY2F0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2ZvbmRvIGRlIGxhIGltYWdlbiBkZWwgY29udGFpbmVyXHJcbi5jb250YWluZXJ7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8vLi4vYXNzZXRzL2ltZy9iYWNrZ3JvdW5kMi5wbmdcIik7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICB9XHJcbi5oM2F6dWx7XHJcbiAgICAgIGNvbG9yOiMwMTQ4OTg7XHJcbiAgfVxyXG4vKiBFc3RpbG9zIGRlbCBoZWFkZXIgICovXHJcbmlvbi10b29sYmFye1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OjYlO1xyXG4gICAgLS1wYWRkaW5nLWVuZDo2JTtcclxuICAgIC0tcGFkZGluZy10b3A6NiU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcclxuXHJcbiAgICAuaW1nLWxvZ297XHJcbiAgICAgICAgd2lkdGg6NTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVyLWltZy1oZWFkZXJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo4OSU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIG1lbnNhamUgZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzM3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCB0aXR1bG8gZGUgbGEgcGFnaW5hICovXHJcbi5jb250YWluZXItdGl0bGV7XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZSBsIGJhbm5lciAgKi9cclxuLmJhbm5lcntcclxuICAgIHBhZGRpbmc6IDBweCA1JTtcclxuICAgIG1hcmdpbi10b3A6IDIlO1xyXG4gICAgXHJcblxyXG4gICAgLmNhcmQtYmFubmVye1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMjEwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjFweDtcclxuICAgICAgICBtYXJnaW46IDEwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgICAgIHotaW5kZXg6MTtcclxuICAgICAgICBpb24tY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIC5jb250YWluZXItbmFtZXtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsMCwwLDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLnBlbmNpbHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jYXJkLWJhbm5lcjo6YWZ0ZXJ7XHJcbiAgICAgICAgY29udGVudDonJztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYmFja2dyb3VuZDpyZ2JhKDAsMCwwLDAuNSk7XHJcbiAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICBoZWlnaHQ6MTAwJTtcclxuICAgICAgICB0b3A6MHB4O1xyXG4gICAgICAgIHJpZ2h0OiAwcHg7XHJcbiAgICAgICAgbGVmdDogMHB4O1xyXG4gICAgICAgIGJvdHRvbTogMHB4O1xyXG4gICAgICAgIHotaW5kZXg6LTE7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb250YWluZXItdGltZXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBwe1xyXG5cclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgIH1cclxufVxyXG5cclxuIl19 */");

/***/ }),

/***/ "H6Gf":
/*!***************************************************************************!*\
  !*** ./src/app/admin-notifications/admin-notifications-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: AdminNotificationsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminNotificationsPageRoutingModule", function() { return AdminNotificationsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _admin_notifications_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-notifications.page */ "n1xm");




const routes = [
    {
        path: '',
        component: _admin_notifications_page__WEBPACK_IMPORTED_MODULE_3__["AdminNotificationsPage"]
    }
];
let AdminNotificationsPageRoutingModule = class AdminNotificationsPageRoutingModule {
};
AdminNotificationsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AdminNotificationsPageRoutingModule);



/***/ }),

/***/ "WjV5":
/*!*********************************************************************************!*\
  !*** ./src/app/components/updatenotification/updatenotification.component.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del header  */\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 90%;\n}\nion-toolbar ion-icon {\n  font-size: 25px;\n}\n.btn-subir {\n  background-color: #94c9d4;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\n/* Estilos del mensaje de bienvenido */\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n/* Estilos del titulo de la pagina */\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n.container-title-input {\n  justify-content: space-between;\n  align-items: center;\n}\n.container-title-input ion-icon {\n  font-size: 19px;\n  padding: 9px;\n  font-weight: bold;\n}\n/* Estilos del icono de subir categoria */\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  background-repeat: repeat;\n  background-size: cover;\n  align-items: center;\n  border: 2px solid #979797;\n  width: 100%;\n  height: 174px;\n  border-radius: 21px;\n}\n.container-icon ion-item {\n  --background: rgba(0,0,0,0);\n}\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n#file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n/* Estilos del contenedor del boton para agregar imagen */\n.container-btn {\n  display: flex;\n  justify-content: center;\n  margin-top: 1%;\n}\n/* Estilos de la tarjeta de los inputs */\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n.container-card ion-card {\n  width: 90%;\n  background-color: #DDF2FB;\n  box-shadow: none;\n  border-radius: 15px;\n}\n.container-card ion-card ion-card-content {\n  padding: 20px;\n  padding-top: 10px;\n}\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n.container-card ion-card ion-card-content .categories {\n  margin-top: 5%;\n}\n.container-card ion-card ion-card-content .categories ion-select {\n  --placeholder-opacity:1;\n}\n.select-gray {\n  --background: #4e5b61;\n  color: white;\n}\n.container-link {\n  padding: 0px 6%;\n  display: flex;\n  justify-content: space-between;\n}\n.container-link p {\n  font-weight: bold;\n  font-size: 17px;\n}\n.container-link .circle {\n  width: 40px;\n  height: 40px;\n  background: #333333;\n  color: white;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx1cGRhdGVub3RpZmljYXRpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0Esd0JBQUE7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQUo7QUFFSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBQVI7QUFFSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFBUjtBQUVJO0VBQ0ksZUFBQTtBQUFSO0FBT0E7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQUpKO0FBT0Esc0NBQUE7QUFDQTtFQUNJLHNCQUFBO0VBQ0EsZUFBQTtBQUpKO0FBS0k7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBSFI7QUFLSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUhSO0FBT0Esb0NBQUE7QUFDQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0FBSko7QUFLSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUhSO0FBT0E7RUFDSSw4QkFBQTtFQUNBLG1CQUFBO0FBSko7QUFNSTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFKUjtBQVNBLHlDQUFBO0FBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQU5KO0FBT0k7RUFDSSwyQkFBQTtBQUxSO0FBT0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUxSO0FBVUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQVBKO0FBVUUseURBQUE7QUFDRjtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFQSjtBQWNBLHdDQUFBO0FBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFYSjtBQVlJO0VBQ0ksVUFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQVZSO0FBV1E7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7QUFUWjtBQVVZO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0FBUmhCO0FBVWM7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUFSbEI7QUFVYztFQUNFLG9CQUFBO0FBUmhCO0FBWWM7RUFDSSxjQUFBO0FBVmxCO0FBV2tCO0VBQ0UsdUJBQUE7QUFUcEI7QUFpQkE7RUFDSSxxQkFBQTtFQUNBLFlBQUE7QUFkSjtBQWlCQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7QUFkSjtBQWVJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBYlI7QUFlSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQWJSIiwiZmlsZSI6InVwZGF0ZW5vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vKiBFc3RpbG9zIGRlbCBoZWFkZXIgICovXHJcbmlvbi10b29sYmFye1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OjYlO1xyXG4gICAgLS1wYWRkaW5nLWVuZDo2JTtcclxuICAgIC0tcGFkZGluZy10b3A6NiU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcclxuXHJcbiAgICAuaW1nLWxvZ297XHJcbiAgICAgICAgd2lkdGg6NTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVyLWltZy1oZWFkZXJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo5MCU7XHJcbiAgICB9XHJcbiAgICBpb24taWNvbntcclxuICAgICAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuLy9Fc3RpbG9zIGRlIGJvdG9uIHN1YmlyIGltZ1xyXG4uYnRuLXN1Ymlye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojOTRjOWQ0O1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDQ1cHg7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgcGFkZGluZzogNSU7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICB9XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBtZW5zYWplIGRlIGJpZW52ZW5pZG8gKi9cclxuLmNvbnRhaW5lci13ZWxjb21le1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzNweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGl0dWxvIGRlIGxhIHBhZ2luYSAqL1xyXG4uY29udGFpbmVyLXRpdGxle1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICAgcHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb250YWluZXItdGl0bGUtaW5wdXR7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgICAgICBwYWRkaW5nOjlweCA7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4vKiBFc3RpbG9zIGRlbCBpY29ubyBkZSBzdWJpciBjYXRlZ29yaWEgKi9cclxuLmNvbnRhaW5lci1pY29ue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6Y292ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgIzk3OTc5NztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxNzRweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBpb24taXRlbXtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMCk7XHJcbiAgICB9XHJcbiAgICAuY2lyY2xle1xyXG4gICAgICAgIHdpZHRoOiA1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjQ0JEOERDO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICBcclxufVxyXG5cclxuI2ZpbGUtaW5wdXQge1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICB9XHJcblxyXG4gIC8qIEVzdGlsb3MgZGVsIGNvbnRlbmVkb3IgZGVsIGJvdG9uIHBhcmEgYWdyZWdhciBpbWFnZW4gKi9cclxuLmNvbnRhaW5lci1idG57XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tdG9wOiAxJTtcclxuXHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi8qIEVzdGlsb3MgZGUgbGEgdGFyamV0YSBkZSBsb3MgaW5wdXRzICovXHJcbi5jb250YWluZXItY2FyZHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGlvbi1jYXJke1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojRERGMkZCOyBcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGlvbi1zZWxlY3Qge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaW9uLWl0ZW17XHJcbiAgICAgICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6MTBweDtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAuY2F0ZWdvcmllc3tcclxuICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgICAgICAgICAgIGlvbi1zZWxlY3R7XHJcbiAgICAgICAgICAgICAgICAgICAgLS1wbGFjZWhvbGRlci1vcGFjaXR5OjE7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnNlbGVjdC1ncmF5e1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNGU1YjYxO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uY29udGFpbmVyLWxpbmt7XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgcHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcbiAgICAuY2lyY2xle1xyXG4gICAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzMzMzMzO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBcclxuICAgIH1cclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "ezcZ":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/admin-notifications/admin-notifications.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content >\r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Bienvenido</h1>\r\n    <p>Administrador</p>\r\n  </ion-row>\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Administrar Noticias</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"banner margindown\" *ngFor=\"let data of notifications; let i = index\" >\r\n    <ion-card class=\"card-banner\" style=\"  background-image: url('{{data.img}}');\">\r\n      <ion-card-header>\r\n        <div class=\"container-name\">\r\n          <ion-icon name=\"person-circle\"></ion-icon>\r\n          <p>Administrador</p>\r\n        </div>      \r\n        <div>\r\n          <ion-icon class=\"pencil\" name=\"pencil-outline\" (click)=\"update(data)\"></ion-icon>\r\n          <ion-icon class=\"pencil\" (click)=\"delete(data.uid)\" name=\"close-outline\"></ion-icon>\r\n        </div>\r\n      </ion-card-header>\r\n      <ion-card-content >\r\n        <ion-row>\r\n          <ion-col size=\"8\">\r\n            <h1>{{data.name}}</h1>\r\n            <p>{{reduceNotificationDescription(data.description)}}</p>\r\n          <p><ion-icon name=\"time-outline\"></ion-icon>  {{tiempo(data.updateDate)}}</p>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"container-time\">\r\n          </ion-col>  \r\n        </ion-row>      \r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-row>\r\n \r\n\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "n1xm":
/*!*****************************************************************!*\
  !*** ./src/app/admin-notifications/admin-notifications.page.ts ***!
  \*****************************************************************/
/*! exports provided: AdminNotificationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminNotificationsPage", function() { return AdminNotificationsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_admin_notifications_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./admin-notifications.page.html */ "ezcZ");
/* harmony import */ var _admin_notifications_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-notifications.page.scss */ "GCLn");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_updatenotification_updatenotification_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/updatenotification/updatenotification.component */ "3DbA");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/alert.service */ "3LUQ");








let AdminNotificationsPage = class AdminNotificationsPage {
    constructor(firestore, alertService, toastController, modalController) {
        this.firestore = firestore;
        this.alertService = alertService;
        this.toastController = toastController;
        this.modalController = modalController;
    }
    ngOnInit() {
        this.getNotifications().subscribe(res => {
            this.notifications = res;
        });
    }
    getNotifications() {
        const ref = this.firestore
            .collection('notification').valueChanges({ idField: 'uid' });
        return ref;
    }
    delete(notificationUid) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const confirmDelete = yield this.alertService.presentAlertConfirm('ITCA App', 'Desea Eliminar esta noticia?');
            if (confirmDelete) {
                this.deleteNotification(notificationUid);
                this.alertService.presentAlert('Notifica Eliminada🤯');
            }
        });
    }
    deleteNotification(uid) {
        this.firestore.doc(`/notification/${uid}`).delete();
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000,
            });
            toast.present();
        });
    }
    reduceNotificationDescription(notificationDescription) {
        return `${notificationDescription.substring(0, 30)} ...`;
    }
    tiempo(time) {
        //  console.log(Date.now() + "");
        let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
        this.publicado = '';
        this.trasformar = 0;
        //Segundos
        if (tiempo > 0 && tiempo < 60) {
            this.trasformar = tiempo;
            this.publicado = this.trasformar + ' Seg';
        }
        //minustos
        else if (tiempo > 60 && tiempo < 3600) {
            this.trasformar = (tiempo / 60).toFixed(0);
            this.publicado = this.trasformar + ' Min';
        }
        //horas
        else if (tiempo > 3600 && tiempo < 86400) {
            this.trasformar = (tiempo / 3600).toFixed(0);
            this.publicado = this.trasformar + ' Horas';
        }
        //Dias
        else if (tiempo > 86400 && tiempo < 604800) {
            this.trasformar = (tiempo / 86400).toFixed(0);
            this.publicado = this.trasformar + ' Dias';
        }
        else if (tiempo > 604800 && tiempo < 2629743) {
            this.trasformar = (tiempo / 604800).toFixed(0);
            this.publicado = this.trasformar + ' Semanas';
        }
        return this.publicado;
    }
    update(notification) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_updatenotification_updatenotification_component__WEBPACK_IMPORTED_MODULE_6__["UpdatenotificationComponent"],
                cssClass: 'my-custom-class',
                componentProps: {
                    notification
                },
            });
            return yield modal.present();
        });
    }
};
AdminNotificationsPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_7__["AlertService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] }
];
AdminNotificationsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-admin-notifications',
        template: _raw_loader_admin_notifications_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_admin_notifications_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AdminNotificationsPage);



/***/ }),

/***/ "sUn6":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/updatenotification/updatenotification.component.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-icon (click)=\"quit()\"  name=\"arrow-back-outline\" slot=start></ion-icon>\r\n    <ion-row class=\"container-img-header\">\r\n      <img class=\"img-logo\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Actualizar notificación</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-icon\" style=\"background-image: url({{notification.img}});\">\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"file\" accept=\"image/png, image/jpeg\" id=\"file-input\"  (change)=\"uploadImageTemporary($event)\">\r\n          </ion-input>\r\n          <button class=\"btn-subir\">\r\n            <ion-icon name=\"arrow-up-outline\"></ion-icon>\r\n          </button>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  \r\n  <ion-row class=\"container-card\">\r\n    <ion-card>\r\n      <ion-card-content>\r\n        <ion-row class=\"container-title-input form\" >\r\n          <p>Nombre</p>\r\n          <ion-icon name=\"pencil-outline\"></ion-icon>\r\n        </ion-row>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"notification.name\"></ion-input>\r\n        </ion-item>\r\n        <ion-row class=\"container-title-input\">\r\n          <p>Descripción</p>\r\n          <ion-icon name=\"pencil-outline\"></ion-icon>\r\n        </ion-row>\r\n        <ion-item lines=\"none\">\r\n          <ion-textarea [(ngModel)]=\"notification.description\"></ion-textarea>\r\n        </ion-item>\r\n  \r\n  \r\n  \r\n        <ion-item lines=\"none\" class=\"categories \">\r\n          <ion-select placeholder=\"Categorías\" [(ngModel)]=\"notification.category\">\r\n            <div *ngFor=\"let category of Categoris; let i = index\">\r\n              <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n  \r\n  \r\n        </ion-item>\r\n        <ion-item lines=\"none\" class=\"categories \">\r\n          <ion-select placeholder=\"Estado\" [(ngModel)]=\"notification.state\">\r\n            <ion-select-option value=\"public\">Público</ion-select-option>\r\n            <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-card-content>\r\n  \r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn margindown\">\r\n    <ion-button (click)=\"updateNotification()\">\r\n      Actualizar\r\n    </ion-button>\r\n    <ion-button (click)=\"enviar()\">\r\n      Enviar notificación\r\n    </ion-button>\r\n\r\n  </ion-row>\r\n\r\n</ion-content>\r\n\r\n");

/***/ })

}]);
//# sourceMappingURL=admin-notifications-admin-notifications-module.js.map