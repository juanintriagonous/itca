(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["list-invoice-admin-list-invoice-admin-module"],{

/***/ "B7Pn":
/*!***************************************************************!*\
  !*** ./src/app/list-invoice-admin/list-invoice-admin.page.ts ***!
  \***************************************************************/
/*! exports provided: ListInvoiceAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceAdminPage", function() { return ListInvoiceAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_list_invoice_admin_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./list-invoice-admin.page.html */ "Zi3S");
/* harmony import */ var _list_invoice_admin_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list-invoice-admin.page.scss */ "Wf6w");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_invoicedetails_invoicedetails_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/invoicedetails/invoicedetails.component */ "IPoj");
/* harmony import */ var _services_banks_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/banks.service */ "3mc2");
/* harmony import */ var _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/upinvoice.service */ "b8cs");
/* harmony import */ var _services_xls_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/xls.service */ "lzbr");









//xls export

let ListInvoiceAdminPage = class ListInvoiceAdminPage {
    constructor(upinvoiceService, router, modalController, bankService, xlsService) {
        this.upinvoiceService = upinvoiceService;
        this.router = router;
        this.modalController = modalController;
        this.bankService = bankService;
        this.xlsService = xlsService;
    }
    ngOnInit() {
        var _a;
        this.getInvoices();
        this.getBankbyId((_a = this.invoice) === null || _a === void 0 ? void 0 : _a.bank);
    }
    getInvoices() {
        this.upinvoiceService.getInvoicesPendding().subscribe(res => {
            this.invoices = res;
            // if (!this.invoices) {
            //   const message = 'No exiten Comprobantes registrados 😣';
            // }
        });
    }
    getBankbyId(bankUid) {
        return this.bankService.getBankByUid(bankUid).subscribe(res => {
            this.bank = res;
        });
    }
    exportXLS() {
        this.xlsService.exportToExcel(this.invoices, 'pagos recibidos');
    }
    //update
    UpdateInvoice(id, name, description, levels, tittleType, isActivate) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_invoicedetails_invoicedetails_component__WEBPACK_IMPORTED_MODULE_6__["InvoicedetailsComponent"],
                cssClass: 'my-custom-class',
                componentProps: {
                    'id': id,
                    'name': name,
                    'description': description,
                    'levels': levels,
                    'tittleType': tittleType,
                    'isActivate': isActivate,
                }
            });
            return yield modal.present();
        });
    }
    goToSeeVoucher(invoice) {
        this.router.navigate(['see-detail-invoice/', invoice.uid]);
    }
};
ListInvoiceAdminPage.ctorParameters = () => [
    { type: _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_8__["UpinvoiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _services_banks_service__WEBPACK_IMPORTED_MODULE_7__["BanksService"] },
    { type: _services_xls_service__WEBPACK_IMPORTED_MODULE_9__["XlsService"] }
];
ListInvoiceAdminPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-list-invoice-admin',
        template: _raw_loader_list_invoice_admin_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_list_invoice_admin_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ListInvoiceAdminPage);



/***/ }),

/***/ "BmDs":
/*!*****************************************************************!*\
  !*** ./src/app/list-invoice-admin/list-invoice-admin.module.ts ***!
  \*****************************************************************/
/*! exports provided: ListInvoiceAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceAdminPageModule", function() { return ListInvoiceAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _list_invoice_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-invoice-admin-routing.module */ "We4L");
/* harmony import */ var _list_invoice_admin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-invoice-admin.page */ "B7Pn");







let ListInvoiceAdminPageModule = class ListInvoiceAdminPageModule {
};
ListInvoiceAdminPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_invoice_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListInvoiceAdminPageRoutingModule"]
        ],
        declarations: [_list_invoice_admin_page__WEBPACK_IMPORTED_MODULE_6__["ListInvoiceAdminPage"]]
    })
], ListInvoiceAdminPageModule);



/***/ }),

/***/ "We4L":
/*!*************************************************************************!*\
  !*** ./src/app/list-invoice-admin/list-invoice-admin-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: ListInvoiceAdminPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceAdminPageRoutingModule", function() { return ListInvoiceAdminPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _list_invoice_admin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-invoice-admin.page */ "B7Pn");




const routes = [
    {
        path: '',
        component: _list_invoice_admin_page__WEBPACK_IMPORTED_MODULE_3__["ListInvoiceAdminPage"]
    },
];
let ListInvoiceAdminPageRoutingModule = class ListInvoiceAdminPageRoutingModule {
};
ListInvoiceAdminPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListInvoiceAdminPageRoutingModule);



/***/ }),

/***/ "Wf6w":
/*!*****************************************************************!*\
  !*** ./src/app/list-invoice-admin/list-invoice-admin.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGxpc3QtaW52b2ljZS1hZG1pbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBQUo7O0FBRUE7RUFDTSxjQUFBO0FBQ047O0FBQ0Esd0JBQUE7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUVKOztBQUFJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFFUjs7QUFBSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFFUiIsImZpbGUiOiJsaXN0LWludm9pY2UtYWRtaW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy9mb25kbyBkZSBsYSBpbWFnZW4gZGVsIGNvbnRhaW5lclxyXG4uY29udGFpbmVye1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIucG5nXCIpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgfVxyXG4uaDNhenVse1xyXG4gICAgICBjb2xvcjojMDE0ODk4O1xyXG4gIH1cclxuLyogRXN0aWxvcyBkZWwgaGVhZGVyICAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIC0tcGFkZGluZy1zdGFydDo2JTtcclxuICAgIC0tcGFkZGluZy1lbmQ6NiU7XHJcbiAgICAtLXBhZGRpbmctdG9wOjYlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNiU7XHJcblxyXG4gICAgLmltZy1sb2dve1xyXG4gICAgICAgIHdpZHRoOjUxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lci1pbWctaGVhZGVye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6ODklO1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "Zi3S":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/list-invoice-admin/list-invoice-admin.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img\r\n        slot=\"end\"\r\n        class=\"img-logo ion-text-center\"\r\n        src=\"../../assets/img/logo.png\"\r\n        alt=\"\"\r\n      />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"container\">\r\n  <ion-title color=\"tertiary\"><strong>Lista de Pagos</strong></ion-title>\r\n  <ion-card class=\"card-invoice\" *ngFor=\"let invoice of invoices\">\r\n    <ion-card-header (click)=\"goToSeeVoucher(invoice)\">\r\n      <ion-card-title>{{invoice.identifynumber}}</ion-card-title>\r\n      <ion-card-subtitle>{{invoice.updateAt.toDate() | date:'dd/MM/yyyy' }}</ion-card-subtitle>\r\n    </ion-card-header>\r\n    <ion-card-content (click)=\"goToSeeVoucher(invoice)\">\r\n     <p><strong>Banco: </strong>{{bank?.name}}</p>\r\n     <p><strong>Valor:</strong> ${{invoice.value}} </p>\r\n     <p><strong>Razón del pago:  </strong>{{invoice.reason}}</p>\r\n     <p><strong>Detalle del pago:  </strong>{{invoice.detail}}</p>\r\n    </ion-card-content>\r\n  </ion-card>\r\n  <ion-button color=\"success\" (click)=\"exportXLS()\">Exportar a Excel</ion-button>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=list-invoice-admin-list-invoice-admin-module.js.map