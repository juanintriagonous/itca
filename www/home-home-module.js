(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "A3+G":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "zpKS");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "WcN3":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<ion-content scrollY=\"true\">\r\n   <div class=\"container\">\r\n      <ion-row class=\"logo-container\">\r\n         <img src=\"../../assets/img/logonuevo.png\" alt=\"\">\r\n      </ion-row>\r\n      <div class=\"container-welcome\">\r\n         <ion-row>\r\n            <ion-col size=\"12\">\r\n              <h2>Bienvenido</h2>\r\n            </ion-col>\r\n     \r\n            <ion-col size=\"12\">\r\n              <h3>Porque el mundo no se detiene, nos adaptamos a una nueva era digital, estamos más cerca de ti</h3>\r\n            </ion-col>\r\n            \r\n            <ion-col size=\"12\" class=\"container-btn\">\r\n              <ion-button routerLink=\"/main\">\r\n                 Comencemos\r\n              </ion-button>\r\n              <img src=\"../../assets/img/logo_nous.png\" alt=\"\">\r\n            </ion-col>\r\n         </ion-row>\r\n     \r\n      </div>\r\n   </div>\r\n \r\n</ion-content>\r\n");

/***/ }),

/***/ "ct+p":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "zpKS");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "A3+G");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "f6od":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 60%;\n  background-image: url('itcabackground.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n  background-position: center;\n}\n\n/* Estilos del contenedor del logo y la imagen del logo */\n\n.logo-container {\n  display: flex;\n  justify-content: center;\n  margin-bottom: 20%;\n}\n\n.logo-container img {\n  margin-top: 0%;\n  width: 85%;\n  height: 85%;\n}\n\n/* Se coloca en blanco el texo */\n\nh2, h3 {\n  color: #fff;\n}\n\n/* Clase para cambiar la fuerza del texto de h2 */\n\nh2 {\n  font-weight: 700;\n}\n\n/* Contenedor donde va el texto de bienvenido */\n\n.container-welcome {\n  width: 100%;\n  height: 100%;\n  padding: 5%;\n  position: relative;\n  bottom: 20%;\n  background-color: #014C9A;\n  border-radius: 35px 35px 0px 0px;\n}\n\n/* Contenedor del boton que sirve para ubicar el boton y el logo de nousclic */\n\n.container-btn {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n  margin-top: 5%;\n}\n\n/* Estilos del boton de comencemos */\n\nion-button {\n  --background:white;\n  --background-activated:white;\n  color: black;\n  --border-radius:30px;\n  font-weight: bold;\n  width: 80%;\n  height: 50px;\n}\n\n/* Media Querys para adaptar el estilo a varias pantallas */\n\n/* @media (min-height: 800px) {\n  .container-btn{\n    margin-top: 40%;\n  }\n }\n\n @media (min-width: 700px) {\n  .logo-container{\n    margin-bottom: 15%;\n\n\n  }\n\n  .container-btn{\n    margin-top: 25%;\n   ion-button{\n    height: 70px;\n   }\n  }\n }\n\n @media (min-width: 900px) {\n\n.logo-container{\n  margin-top: 20%;\n  margin-bottom: 20%;\n\n\n}\n  .container-btn{\n    margin-top: 10%;\n   ion-button{\n    height: 70px;\n   }\n  }\n }\n */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLDJDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0FBQ0Y7O0FBSUEseURBQUE7O0FBQ0E7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQURGOztBQUVFO0VBQ0UsY0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBQUo7O0FBSUEsZ0NBQUE7O0FBQ0E7RUFDRSxXQUFBO0FBREY7O0FBSUEsaURBQUE7O0FBQ0E7RUFDRSxnQkFBQTtBQURGOztBQUlBLCtDQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxnQ0FBQTtBQURGOztBQUlBLDhFQUFBOztBQUNBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFERjs7QUFJQSxvQ0FBQTs7QUFDQTtFQUNFLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FBREY7O0FBS0EsMkRBQUE7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQUFBIiwiZmlsZSI6ImhvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICBoZWlnaHQ6IDYwJTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8vLi4vYXNzZXRzL2ltZy9pdGNhYmFja2dyb3VuZC5wbmdcIik7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbiBcclxuLyogRXN0aWxvcyBkZWwgY29udGVuZWRvciBkZWwgbG9nbyB5IGxhIGltYWdlbiBkZWwgbG9nbyAqL1xyXG4ubG9nby1jb250YWluZXJ7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW4tYm90dG9tOiAyMCU7XHJcbiAgaW1ne1xyXG4gICAgbWFyZ2luLXRvcDogMCU7XHJcbiAgICB3aWR0aDogODUlO1xyXG4gICAgaGVpZ2h0OiA4NSU7XHJcbn1cclxufVxyXG5cclxuLyogU2UgY29sb2NhIGVuIGJsYW5jbyBlbCB0ZXhvICovXHJcbmgyLGgze1xyXG4gIGNvbG9yOiNmZmY7XHJcbn1cclxuXHJcbi8qIENsYXNlIHBhcmEgY2FtYmlhciBsYSBmdWVyemEgZGVsIHRleHRvIGRlIGgyICovXHJcbmgye1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi8qIENvbnRlbmVkb3IgZG9uZGUgdmEgZWwgdGV4dG8gZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHBhZGRpbmc6IDUlO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3R0b206IDIwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDE0QzlBO1xyXG4gIGJvcmRlci1yYWRpdXM6IDM1cHggMzVweCAwcHggMHB4O1xyXG59XHJcblxyXG4vKiBDb250ZW5lZG9yIGRlbCBib3RvbiBxdWUgc2lydmUgcGFyYSB1YmljYXIgZWwgYm90b24geSBlbCBsb2dvIGRlIG5vdXNjbGljICovXHJcbi5jb250YWluZXItYnRue1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiA1JTtcclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgYm90b24gZGUgY29tZW5jZW1vcyAqL1xyXG5pb24tYnV0dG9ue1xyXG4gIC0tYmFja2dyb3VuZDp3aGl0ZTtcclxuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOndoaXRlO1xyXG4gIGNvbG9yOmJsYWNrO1xyXG4gIC0tYm9yZGVyLXJhZGl1czozMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcblxyXG5cclxuLyogTWVkaWEgUXVlcnlzIHBhcmEgYWRhcHRhciBlbCBlc3RpbG8gYSB2YXJpYXMgcGFudGFsbGFzICovXHJcbi8qIEBtZWRpYSAobWluLWhlaWdodDogODAwcHgpIHtcclxuICAuY29udGFpbmVyLWJ0bntcclxuICAgIG1hcmdpbi10b3A6IDQwJTtcclxuICB9XHJcbiB9XHJcblxyXG4gQG1lZGlhIChtaW4td2lkdGg6IDcwMHB4KSB7XHJcbiAgLmxvZ28tY29udGFpbmVye1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTUlO1xyXG4gIFxyXG4gICBcclxuICB9XHJcblxyXG4gIC5jb250YWluZXItYnRue1xyXG4gICAgbWFyZ2luLXRvcDogMjUlO1xyXG4gICBpb24tYnV0dG9ue1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICB9XHJcbiAgfVxyXG4gfVxyXG5cclxuIEBtZWRpYSAobWluLXdpZHRoOiA5MDBweCkge1xyXG4gICBcclxuLmxvZ28tY29udGFpbmVye1xyXG4gIG1hcmdpbi10b3A6IDIwJTtcclxuICBtYXJnaW4tYm90dG9tOiAyMCU7XHJcblxyXG5cclxufVxyXG4gIC5jb250YWluZXItYnRue1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICBpb24tYnV0dG9ue1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICB9XHJcbiAgfVxyXG4gfVxyXG4gKi9cclxuIl19 */");

/***/ }),

/***/ "zpKS":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./home.page.html */ "WcN3");
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.page.scss */ "f6od");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let HomePage = class HomePage {
    constructor() { }
};
HomePage.ctorParameters = () => [];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map