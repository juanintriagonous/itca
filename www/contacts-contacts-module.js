(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contacts-contacts-module"],{

/***/ "+gzF":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contacts/contacts.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Gestor de Información de Contacto</h1>\r\n  </ion-row>\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Añadir Contactos</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-icon\" (click)=\"openModal()\">\r\n        <div class=\"circle\" *ngIf=\"contact.icon === ''\">\r\n          <img src=\"../../assets/img/add-notification.png\" alt=\"\">\r\n        </div>\r\n        <div class=\"circle\" *ngIf=\"contact.icon !== ''\">\r\n          <ion-icon slot=\"start\" name=\"{{contact.icon}}\"></ion-icon>\r\n        </div>\r\n        <p>Agregar ícono</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-card\">\r\n    <ion-card>\r\n      <ion-card-content class=\"form\">\r\n        <p>Categoría</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"contact.categoria\" ></ion-input>\r\n        </ion-item>\r\n        <p>Hipervínculo</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"contact.hiper\"></ion-input>\r\n        </ion-item>\r\n        <p>Estado</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-select [(ngModel)]=\"contact.state\">\r\n            <ion-select-option value=\"public\">Público</ion-select-option>\r\n            <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button (click)=\"Addcontact()\" >\r\n      Registrar\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Administrar Contactos</p>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-card margindown\" *ngFor=\"let data of contacts; let i = index\">\r\n    <ion-card style=\"margin-top: 20px;\">\r\n      <ion-card-content class=\"form\">\r\n        <p>Categoría</p>\r\n        <ion-item lines=\"none\">\r\n          {{data.categoria}}\r\n        </ion-item>\r\n        <p>Hipervínculo</p>\r\n        <ion-item lines=\"none\" (click)=\"ventana(data.hiper)\">\r\n            <p>Click para abrir hipervínculo</p>\r\n        </ion-item>\r\n    \r\n        <p>Ícono</p>\r\n        <div class=\"container-query-icon\">\r\n          <ion-icon slot=\"start\" name=\"{{data.icon}}\"></ion-icon>\r\n        </div>  \r\n        <p>Estado</p>\r\n        <ion-item lines=\"none\">\r\n          {{data.state}}\r\n        </ion-item>\r\n        <p>Acción</p>\r\n        <ion-row class=\"container-options\">\r\n          <ion-col size=\"6\">\r\n            <ion-item (click)=\"delete( data.id)\">\r\n              <ion-label  class=\"icon\">\r\n                <ion-icon name=\"trash-outline\" ></ion-icon>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-col>\r\n\r\n          <ion-col size=\"6\">\r\n            <ion-item (click)=\"update( data.id,data.categoria,data.hiper,data.icon,data.state)\">\r\n              <ion-label class=\"icon\">\r\n                <ion-icon name=\"pencil-outline\" ></ion-icon>\r\n              </ion-label>\r\n            </ion-item> \r\n\r\n          </ion-col>\r\n      \r\n       \r\n\r\n        </ion-row>\r\n       \r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "/rNG":
/*!*****************************************************!*\
  !*** ./src/app/contacts/contacts-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ContactsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPageRoutingModule", function() { return ContactsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _contacts_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contacts.page */ "V8xu");




const routes = [
    {
        path: '',
        component: _contacts_page__WEBPACK_IMPORTED_MODULE_3__["ContactsPage"]
    }
];
let ContactsPageRoutingModule = class ContactsPageRoutingModule {
};
ContactsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactsPageRoutingModule);



/***/ }),

/***/ "0z1h":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/choose-icon/choose-icon.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-icon name=\"close-circle-outline\" (click)=\"closeModal()\" slot=\"end\"></ion-icon>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n    <ion-radio-group [(ngModel)]=\"icon\">\r\n      <ion-item>\r\n        <ion-icon name=\"cash-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"cash-outline\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-icon name=\"briefcase-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"briefcase-outline\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-icon name=\"document-text-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"document-text-outline\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-icon name=\"pencil-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"pencil-outline\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-icon name=\"glasses-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"glasses-outline\"></ion-radio>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-icon name=\"information-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"information-outline\"></ion-radio>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-icon name=\"card-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"card-outline\"></ion-radio>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-icon name=\"desktop-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"desktop-outline\"></ion-radio>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-icon name=\"documents-outline\"></ion-icon>\r\n        <ion-radio slot=\"start\" value=\"documents-outline\"></ion-radio>\r\n      </ion-item>\r\n    </ion-radio-group>\r\n\r\n  <ion-row class=\"container-btn\">\r\n    <ion-button (click)=\"sendData()\" >\r\n      Guardar\r\n    </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "1s7i":
/*!*********************************************!*\
  !*** ./src/app/contacts/contacts.module.ts ***!
  \*********************************************/
/*! exports provided: ContactsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPageModule", function() { return ContactsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacts-routing.module */ "/rNG");
/* harmony import */ var _contacts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contacts.page */ "V8xu");
/* harmony import */ var _components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/choose-icon/choose-icon.component */ "dwgJ");
/* harmony import */ var _components_updatecontacts_updatecontacts_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/updatecontacts/updatecontacts.component */ "rL8e");









let ContactsPageModule = class ContactsPageModule {
};
ContactsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactsPageRoutingModule"]
        ],
        entryComponents: [_components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_7__["ChooseIconComponent"], _components_updatecontacts_updatecontacts_component__WEBPACK_IMPORTED_MODULE_8__["UpdatecontactsComponent"]],
        declarations: [_contacts_page__WEBPACK_IMPORTED_MODULE_6__["ContactsPage"], _components_updatecontacts_updatecontacts_component__WEBPACK_IMPORTED_MODULE_8__["UpdatecontactsComponent"], _components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_7__["ChooseIconComponent"]]
    })
], ContactsPageModule);



/***/ }),

/***/ "8XhK":
/*!*************************************************************************!*\
  !*** ./src/app/components/updatecontacts/updatecontacts.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del header  */\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\nion-toolbar ion-icon {\n  font-size: 25px;\n}\n/* Estilos del mensaje de bienvenido */\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n/* Estilos del titulo de la pagina */\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n/* Estilos del icono de subir categoria */\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.container-icon .circle ion-icon {\n  font-size: 20px;\n}\n/* Estilos de la tarjeta de los inputs */\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n.container-card ion-card {\n  width: 90%;\n  background-color: #DDF2FB;\n  box-shadow: none;\n  border-radius: 15px;\n}\n.container-card ion-card ion-card-content {\n  padding: 20px;\n}\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n/* Estilos del contenedor del boton para agregar imagen */\n.container-btn {\n  display: flex;\n  justify-content: center;\n}\n.container-btn ion-button {\n  height: 60px;\n  width: 90%;\n  --background:#161616;\n  --background-activated:#DDF2FB;\n  --border-radius: 15px;\n  --box-shadow:0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 18px;\n  font-weight: bold;\n  color: #fcfcfc;\n}\n.form {\n  padding: 10px 0 8px 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx1cGRhdGVjb250YWN0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSx3QkFBQTtBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFBSjtBQUVJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFBUjtBQUVJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUFSO0FBRUk7RUFDSSxlQUFBO0FBQVI7QUFJQSxzQ0FBQTtBQUNBO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0FBREo7QUFFSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFBUjtBQUVJO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBQVI7QUFJQSxvQ0FBQTtBQUNBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUFESjtBQUVJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBQVI7QUFNQSx5Q0FBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQUhKO0FBSUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUZSO0FBSVE7RUFDSSxlQUFBO0FBRlo7QUFPQSx3Q0FBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBSko7QUFLSTtFQUNJLFVBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFIUjtBQUlRO0VBQ0ksYUFBQTtBQUZaO0FBR1k7RUFDSSxXQUFBO0VBQ0EsdUJBQUE7QUFEaEI7QUFHYztFQUNJLGlCQUFBO0VBQ0EsWUFBQTtBQURsQjtBQUdjO0VBQ0Usb0JBQUE7QUFEaEI7QUFRQSx5REFBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBTEo7QUFNSTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSw4QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbURBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBSlI7QUFVQTtFQUNJLHVCQUFBO0FBUEoiLCJmaWxlIjoidXBkYXRlY29udGFjdHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLyogRXN0aWxvcyBkZWwgaGVhZGVyICAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIC0tcGFkZGluZy1zdGFydDo2JTtcclxuICAgIC0tcGFkZGluZy1lbmQ6NiU7XHJcbiAgICAtLXBhZGRpbmctdG9wOjYlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNiU7XHJcblxyXG4gICAgLmltZy1sb2dve1xyXG4gICAgICAgIHdpZHRoOjUxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lci1pbWctaGVhZGVye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6ODklO1xyXG4gICAgfVxyXG4gICAgaW9uLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBtZW5zYWplIGRlIGJpZW52ZW5pZG8gKi9cclxuLmNvbnRhaW5lci13ZWxjb21le1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzNweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGl0dWxvIGRlIGxhIHBhZ2luYSAqL1xyXG4uY29udGFpbmVyLXRpdGxle1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICAgcHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuLyogRXN0aWxvcyBkZWwgaWNvbm8gZGUgc3ViaXIgY2F0ZWdvcmlhICovXHJcbi5jb250YWluZXItaWNvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0NCRDhEQztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgaW9uLWljb257XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGUgbGEgdGFyamV0YSBkZSBsb3MgaW5wdXRzICovXHJcbi5jb250YWluZXItY2FyZHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGlvbi1jYXJke1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojRERGMkZCOyBcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgICAgaW9uLXNlbGVjdCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpb24taXRlbXtcclxuICAgICAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czoxMHB4O1xyXG4gICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlbCBib3RvbiBwYXJhIGFncmVnYXIgaW1hZ2VuICovXHJcbi5jb250YWluZXItYnRue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaW9uLWJ1dHRvbntcclxuICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgd2lkdGg6OTAlO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDojMTYxNjE2O1xyXG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6I0RERjJGQjtcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgLS1ib3gtc2hhZG93OjBweCA0cHggMjBweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMjUpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjojZmNmY2ZjO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcbi5mb3Jte1xyXG4gICAgcGFkZGluZzogMTBweCAwIDhweCAgNXB4O1xyXG59Il19 */");

/***/ }),

/***/ "In7F":
/*!*******************************************************************!*\
  !*** ./src/app/components/choose-icon/choose-icon.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --padding-start:25px;\n  --padding-end:25px;\n}\nion-toolbar ion-icon {\n  font-size: 25px;\n}\n/* Estilos del contenedor del boton para agregar imagen */\n.container-btn {\n  display: flex;\n  justify-content: center;\n}\n.container-btn ion-button {\n  height: 60px;\n  width: 80%;\n  --background:#DDF2FB;\n  --background-activated:#DDF2FB;\n  --border-radius: 15px;\n  --box-shadow:0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 18px;\n  font-weight: bold;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxjaG9vc2UtaWNvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFBO0VBQ0Esa0JBQUE7QUFDSjtBQUFJO0VBQ0ksZUFBQTtBQUVSO0FBRUEseURBQUE7QUFDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUNKO0FBQUk7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0VBQ0EsOEJBQUE7RUFDQSxxQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQUVSIiwiZmlsZSI6ImNob29zZS1pY29uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6MjVweDtcclxuICAgIC0tcGFkZGluZy1lbmQ6MjVweDtcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweFxyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlbCBib3RvbiBwYXJhIGFncmVnYXIgaW1hZ2VuICovXHJcbi5jb250YWluZXItYnRue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaW9uLWJ1dHRvbntcclxuICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgd2lkdGg6ODAlO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDojRERGMkZCO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6I0RERjJGQjtcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgLS1ib3gtc2hhZG93OjBweCA0cHggMjBweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMjUpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjpibGFjaztcclxuICAgIH1cclxufSJdfQ== */");

/***/ }),

/***/ "V8xu":
/*!*******************************************!*\
  !*** ./src/app/contacts/contacts.page.ts ***!
  \*******************************************/
/*! exports provided: ContactsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPage", function() { return ContactsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_contacts_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./contacts.page.html */ "+gzF");
/* harmony import */ var _contacts_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contacts.page.scss */ "cj2o");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/choose-icon/choose-icon.component */ "dwgJ");
/* harmony import */ var _components_updatecontacts_updatecontacts_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/updatecontacts/updatecontacts.component */ "rL8e");








let ContactsPage = class ContactsPage {
    constructor(firestore, toastController, modalController) {
        this.firestore = firestore;
        this.toastController = toastController;
        this.modalController = modalController;
        this.contact = { categoria: "", hiper: "", icon: "", state: "" };
    }
    ngOnInit() {
        this.firestore
            .collection("/contacts").snapshotChanges().subscribe(res => {
            if (res) {
                this.contacts = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        categoria: e.payload.doc.data()["categoria"],
                        hiper: e.payload.doc.data()["hiper"],
                        icon: e.payload.doc.data()["icon"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    Addcontact() {
        if (this.contact.categoria != "" && this.contact.hiper != "" && this.contact.icon != "" && this.contact.state != "") {
            let addcontact = {};
            addcontact['categoria'] = this.contact.categoria;
            addcontact['hiper'] = this.contact.hiper;
            addcontact['icon'] = this.contact.icon;
            addcontact['state'] = this.contact.state;
            this.firestore.collection('/contacts/').add(addcontact).then(() => {
                this.contact = { categoria: "", hiper: "", icon: "", state: "" };
                this.mensaje("Contacto agregado");
            });
        }
        else {
            this.mensaje("Ingrese todos los datos");
        }
    }
    delete(valor) {
        this.firestore.doc('/contacts/' + valor).delete();
        this.mensaje("Contacto eliminada");
    }
    ventana(valor) {
        window.open(valor, '_system');
    }
    update(id, categoria, hiper, icon, state) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(icon);
            const modal = yield this.modalController.create({
                component: _components_updatecontacts_updatecontacts_component__WEBPACK_IMPORTED_MODULE_7__["UpdatecontactsComponent"],
                cssClass: 'my-custom-class',
                componentProps: {
                    'id': id,
                    'categoria': categoria,
                    'hiper': hiper,
                    'icon': icon,
                    'state': state
                }
            });
            return yield modal.present();
        });
    }
    openModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_6__["ChooseIconComponent"],
                cssClass: 'my-custom-class-2',
                showBackdrop: false,
                backdropDismiss: false,
            });
            modal.onDidDismiss().then((nav) => {
                this.contact.icon = nav.data.option;
            });
            return yield modal.present();
        });
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000
            });
            toast.present();
        });
    }
};
ContactsPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
ContactsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contacts',
        template: _raw_loader_contacts_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contacts_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ContactsPage);



/***/ }),

/***/ "cj2o":
/*!*********************************************!*\
  !*** ./src/app/contacts/contacts.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos del icono de subir categoria */\n\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.container-icon .circle ion-icon {\n  font-size: 20px;\n}\n\n/* Estilos de la tarjeta de los inputs */\n\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n\n.container-card ion-card {\n  width: 90%;\n  background-color: #DDF2FB;\n  box-shadow: none;\n  border-radius: 15px;\n}\n\n.container-card ion-card ion-card-content {\n  padding: 20px;\n  padding-top: 10px;\n}\n\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n\n/* Estilos del contenedor del boton para agregar imagen */\n\n.container-query-icon {\n  display: flex;\n  justify-content: center;\n}\n\n.container-query-icon ion-icon {\n  font-size: 40px;\n}\n\n.icon {\n  display: flex;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGNvbnRhY3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLFlBQUE7RUFDQSx3Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFBSjs7QUFFQTtFQUNNLGNBQUE7QUFDTjs7QUFDQSx3QkFBQTs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBQ0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUNSOztBQUNJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUNSOztBQUdBLHNDQUFBOztBQUNBO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0FBQUo7O0FBQ0k7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBQ1I7O0FBQ0k7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUFDUjs7QUFHQSxvQ0FBQTs7QUFDQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0FBQUo7O0FBQ0k7RUFDSSxpQkFBQTtFQUNBLGVBQUE7QUFDUjs7QUFHQSx5Q0FBQTs7QUFDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFBSjs7QUFDSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQ1I7O0FBQ1E7RUFDSSxlQUFBO0FBQ1o7O0FBSUEsd0NBQUE7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFESjs7QUFFSTtFQUNJLFVBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFBUjs7QUFDUTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtBQUNaOztBQUFZO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0FBRWhCOztBQUFjO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FBRWxCOztBQUFjO0VBQ0Usb0JBQUE7QUFFaEI7O0FBS0EseURBQUE7O0FBR0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFKSjs7QUFLSTtFQUNJLGVBQUE7QUFIUjs7QUFPQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUpKIiwiZmlsZSI6ImNvbnRhY3RzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vZm9uZG8gZGUgbGEgaW1hZ2VuIGRlbCBjb250YWluZXJcclxuLmNvbnRhaW5lcntcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy8uLi9hc3NldHMvaW1nL2JhY2tncm91bmQyLnBuZ1wiKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIH1cclxuLmgzYXp1bHtcclxuICAgICAgY29sb3I6IzAxNDg5ODtcclxuICB9XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuXHJcbmlvbi10b29sYmFye1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OjYlO1xyXG4gICAgLS1wYWRkaW5nLWVuZDo2JTtcclxuICAgIC0tcGFkZGluZy10b3A6NiU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcclxuXHJcbiAgICAuaW1nLWxvZ297XHJcbiAgICAgICAgd2lkdGg6NTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVyLWltZy1oZWFkZXJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo4OSU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIG1lbnNhamUgZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzM3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCB0aXR1bG8gZGUgbGEgcGFnaW5hICovXHJcbi5jb250YWluZXItdGl0bGV7XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgaWNvbm8gZGUgc3ViaXIgY2F0ZWdvcmlhICovXHJcbi5jb250YWluZXItaWNvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0NCRDhEQztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgaW9uLWljb257XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGUgbGEgdGFyamV0YSBkZSBsb3MgaW5wdXRzICovXHJcbi5jb250YWluZXItY2FyZHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGlvbi1jYXJke1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojRERGMkZCOyBcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGlvbi1zZWxlY3Qge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaW9uLWl0ZW17XHJcbiAgICAgICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6MTBweDtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgY29udGVuZWRvciBkZWwgYm90b24gcGFyYSBhZ3JlZ2FyIGltYWdlbiAqL1xyXG5cclxuXHJcbi5jb250YWluZXItcXVlcnktaWNvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIH1cclxufVxyXG5cclxuLmljb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "dwgJ":
/*!*****************************************************************!*\
  !*** ./src/app/components/choose-icon/choose-icon.component.ts ***!
  \*****************************************************************/
/*! exports provided: ChooseIconComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChooseIconComponent", function() { return ChooseIconComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_choose_icon_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./choose-icon.component.html */ "0z1h");
/* harmony import */ var _choose_icon_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./choose-icon.component.scss */ "In7F");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let ChooseIconComponent = class ChooseIconComponent {
    constructor(modalController) {
        this.modalController = modalController;
        this.icon = "";
    }
    ngOnInit() { }
    /* Metodo para cerrar el modal */
    closeModal() {
        this.modalController.dismiss({
            option: ""
        });
    }
    /* Metodo que envia el dato a la pagina principal */
    sendData() {
        this.modalController.dismiss({
            'option': this.icon || "",
        });
    }
};
ChooseIconComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
ChooseIconComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-choose-icon',
        template: _raw_loader_choose_icon_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_choose_icon_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ChooseIconComponent);



/***/ }),

/***/ "rL8e":
/*!***********************************************************************!*\
  !*** ./src/app/components/updatecontacts/updatecontacts.component.ts ***!
  \***********************************************************************/
/*! exports provided: UpdatecontactsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatecontactsComponent", function() { return UpdatecontactsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_updatecontacts_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./updatecontacts.component.html */ "rdth");
/* harmony import */ var _updatecontacts_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./updatecontacts.component.scss */ "8XhK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/choose-icon/choose-icon.component */ "dwgJ");







let UpdatecontactsComponent = class UpdatecontactsComponent {
    constructor(firebase, toastController, modalController) {
        this.firebase = firebase;
        this.toastController = toastController;
        this.modalController = modalController;
    }
    ngOnInit() { }
    quit() {
        this.modalController.dismiss();
    }
    update() {
        if (this.categoria != '' && this.hiper != '' && this.state != '') {
            let contact = {};
            contact['categoria'] = this.categoria;
            contact['hiper'] = this.hiper;
            contact['icon'] = this.icon;
            contact['state'] = this.state;
            this.firebase.doc('/contacts/' + this.id).update(contact).then(() => {
                this.quit();
                this.mensaje("Contacto Actualizada");
            });
        }
        else {
            this.mensaje("Todos los datos debe estar llenos");
        }
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000
            });
            toast.present();
        });
    }
    openModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_choose_icon_choose_icon_component__WEBPACK_IMPORTED_MODULE_6__["ChooseIconComponent"],
                cssClass: 'my-custom-class-2',
                showBackdrop: false,
                backdropDismiss: false,
            });
            modal.onDidDismiss().then((nav) => {
                console.log(nav.data.option);
                this.icon = nav.data.option;
            });
            return yield modal.present();
        });
    }
    ventana(valor) {
        window.open(valor, '_system');
    }
};
UpdatecontactsComponent.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] }
];
UpdatecontactsComponent.propDecorators = {
    id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    categoria: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    hiper: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    state: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
UpdatecontactsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-updatecontacts',
        template: _raw_loader_updatecontacts_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_updatecontacts_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpdatecontactsComponent);



/***/ }),

/***/ "rdth":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/updatecontacts/updatecontacts.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-icon (click)=\"quit()\"  name=\"arrow-back-outline\" slot=start></ion-icon>\r\n    <ion-row class=\"container-img-header\">\r\n      <img class=\"img-logo\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Contactos</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-icon\" (click)=\"openModal()\">\r\n    \r\n        <div class=\"circle\" >\r\n          <ion-icon slot=\"start\" name=\"{{icon}}\"></ion-icon>\r\n        </div> \r\n        <p>Agregar ícono</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"container-card\">\r\n    <ion-card >\r\n      <ion-card-content>\r\n        <p class=\"form\">Categoría</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"categoria\" ></ion-input>\r\n        </ion-item>\r\n        <p class=\"form\">Hipervínculo</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"hiper\" ></ion-input>\r\n        </ion-item>       \r\n        <p class=\"form\">Estado</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-select [(ngModel)]=\"state\">\r\n            <ion-select-option value=\"public\">Público</ion-select-option>\r\n            <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn margindown\">\r\n    <ion-button (click)=\"update()\" >\r\n      Actualizar\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=contacts-contacts-module.js.map