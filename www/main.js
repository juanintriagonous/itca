(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Daniel\Documents\ionic\itca\src\main.ts */"zUnb");


/***/ }),

/***/ "2xQN":
/*!*********************************************************************!*\
  !*** ./src/app/components/update-career/update-career.component.ts ***!
  \*********************************************************************/
/*! exports provided: UpdateCareerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCareerComponent", function() { return UpdateCareerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_update_career_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./update-career.component.html */ "9GGu");
/* harmony import */ var _update_career_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./update-career.component.scss */ "Xb3o");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/alert.service */ "3LUQ");
/* harmony import */ var src_app_services_career_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/career.service */ "Uvke");
/* harmony import */ var src_app_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/Errors/listErrors */ "Q98m");
/* harmony import */ var src_app_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/utilities/formValidation */ "ejrE");









let UpdateCareerComponent = class UpdateCareerComponent {
    constructor(modalController, loadingController, alertService, careerService) {
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.alertService = alertService;
        this.careerService = careerService;
        this.validationRules = {
            required: ['name', 'description', 'levels', 'parallel', 'tittleType', 'isActivate'],
        };
        this.errors = {
            id: null,
            name: null,
            description: null,
            levels: null,
            parallel: null,
            tittleType: null,
            isActivate: null
        };
    }
    ngOnInit() {
    }
    UpdateCareer() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (!this.validForm(this.career)) {
                return;
            }
            try {
                yield this.alertService.presentLoading('Actualizando Carrera ...');
                this.careerService.updateCareer(this.career).then(() => {
                    this.alertService.dismissLoading();
                    this.closeModal();
                }).catch();
            }
            catch (e) {
                this.alertService.loading.dismiss();
                this.alertService.presentAlert(src_app_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__["listErrors"][e.code] || src_app_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_7__["listErrors"]['app/general']);
            }
        });
    }
    closeModal() {
        this.modalController.dismiss();
    }
    /**
       * Método para validar los campos
       * @returns Retorna verdadero si no hay errores y falso si hay algún error
       */
    validForm(formValues) {
        const errors = Object(src_app_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_8__["validateForm"])(formValues, this.validationRules);
        this.errors = errors;
        const validForm = Object.keys(errors).length;
        return !validForm;
    }
};
UpdateCareerComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: src_app_services_career_service__WEBPACK_IMPORTED_MODULE_6__["CareerService"] }
];
UpdateCareerComponent.propDecorators = {
    career: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
UpdateCareerComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-update-career',
        template: _raw_loader_update_career_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_update_career_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpdateCareerComponent);



/***/ }),

/***/ "3LUQ":
/*!*******************************************!*\
  !*** ./src/app/services/alert.service.ts ***!
  \*******************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "gcOT");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");




const { Toast } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let AlertService = class AlertService {
    constructor(alertController, loadingController, modalController) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.modalController = modalController;
    }
    presentLoading(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message,
            });
            yield this.loading.present();
        });
    }
    dismissLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading.dismiss();
        });
    }
    presentAlert(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message,
                buttons: [
                    {
                        text: 'Aceptar',
                        role: 'cancel',
                    },
                ],
            });
            yield alert.present();
        });
    }
    presentAlertWithHeader(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: 'Aceptar',
                        role: 'cancel'
                    },
                ],
            });
            yield alert.present();
        });
    }
    presentAlertUpdated(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: 'Aceptar',
                        role: 'cancel',
                        handler: () => {
                            this.modalController.dismiss();
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    presentAlertConfirm(header, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const alert = yield this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: header,
                    message: message,
                    buttons: [
                        {
                            text: 'Cancelar',
                            role: 'cancel',
                            cssClass: 'secondary',
                            handler: (blah) => {
                                return resolve(false);
                            },
                        },
                        {
                            text: 'Aceptar',
                            handler: () => {
                                return resolve(true);
                            },
                        },
                    ],
                });
                yield alert.present();
            }));
        });
    }
    presentModal(modalPageName, data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: modalPageName,
                componentProps: data,
            });
            yield modal.present();
        });
    }
    presentAlertConfirmTest(categoryId, remove) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: '¡Confirme!',
                message: '¿Esta seguro de que desea <strong>eliminar</strong> esta categoría?',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                    },
                    {
                        text: 'Ok',
                        handler: remove(categoryId),
                    },
                ],
            });
            yield alert.present();
        });
    }
    toastShow(messenger) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield Toast.show({
                text: messenger,
            });
        });
    }
};
AlertService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] }
];
AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AlertService);



/***/ }),

/***/ "9GGu":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/update-career/update-career.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button (click)=\"closeModal()\">\r\n        <ion-icon slot=\"icon-only\" style=\"color: white;\" name=\"arrow-back-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-row class=\"ion-text-center align-items-center\">\r\n    <ion-col size=\"12\">\r\n      <ion-text>\r\n        <h1>Editar Carrera</h1>\r\n      </ion-text>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <ion-item>\r\n        <ion-input placeholder=\"Nombre\" [(ngModel)]=\"this.career.name\" required></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.name\">\r\n        <span>\r\n          <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.name}}\r\n        </span>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <ion-item>\r\n        <ion-input placeholder=\"Descripción\" [(ngModel)]=\"this.career.description\"></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.description\">\r\n        <span>\r\n          <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.description}}\r\n        </span>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <ion-label>Niveles</ion-label>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.levels[0].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Primer Periódo</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.levels[1].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Segundo Periódo</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.levels[2].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Tercero Periódo</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.levels[3].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Cuarto Periódo</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.levels[4].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Quinto Periódo</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.levels[5].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Sexto Periódo</label>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.levels\">\r\n        <span>\r\n          <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.levels}}\r\n        </span>\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <ion-label>Paralelos</ion-label>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.parallel[0].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Paralelo A</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.parallel[1].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Paralelo B</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.parallel[2].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Paralelo C</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.parallel[3].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Paralelo D</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.parallel[4].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Paralelo E</label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-checkbox color=\"light\" [(ngModel)]=\"this.career.parallel[5].isActivate\" slot=\"start\"> </ion-checkbox>\r\n        <label>Paralelo F</label>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.parallel\">\r\n        <span>\r\n          <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.parallel}}\r\n        </span>\r\n      </div>\r\n    </ion-col>\r\n    <div class=\"input-error ion-margin-top\" *ngIf=\"errors.levels\">\r\n      <span>\r\n        <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.levels}}\r\n      </span>\r\n    </div>\r\n\r\n    <ion-col size=\"12\">\r\n      <ion-item>\r\n        <ion-select value=\"{{this.career.tittleType}}\" [(ngModel)]=\"this.career.tittleType\" okText=\"Aceptar\"\r\n          cancelText=\"Cancelar\" required>\r\n          <ion-select-option value=\"Técnico\">Técnico</ion-select-option>\r\n          <ion-select-option value=\"Técnologo\">Tecnólogo</ion-select-option>\r\n          <ion-select-option value=\"Otros\">Otros</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.tittleType\">\r\n        <span>\r\n          <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.tittleType}}\r\n        </span>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <ion-item>\r\n        <ion-select placeholder=\"Estado de Publicación\" value=\"{{this.career.isActivate}}\"\r\n          [(ngModel)]=\"this.career.isActivate\" okText=\"Aceptar\" cancelText=\"Cancelar\" required>\r\n          <ion-select-option value=\"Habilitado\">Habilitado</ion-select-option>\r\n          <ion-select-option value=\"Deshabilitado\">Deshabilitado</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.isActivate\">\r\n        <span>\r\n          <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.isActivate}}\r\n        </span>\r\n      </div>\r\n    </ion-col>\r\n\r\n\r\n\r\n    <ion-col size=\"10\" offset=\"1\">\r\n      <ion-button class=\"btn\" expand=\"block\" (click)=\"UpdateCareer()\">Actualizar datos de Carrera</ion-button>\r\n    </ion-col>\r\n\r\n  </ion-row>\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyClCc1klRd99Jg3MpsnRJpcAHRLraXDZT0",
        authDomain: "itca-2674a.firebaseapp.com",
        projectId: "itca-2674a",
        storageBucket: "itca-2674a.appspot.com",
        messagingSenderId: "325051574126",
        appId: "1:325051574126:web:2cd6862052229a09e6037f",
        measurementId: "G-P33JE48VY4"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "H+l1":
/*!******************************************!*\
  !*** ./src/app/services/push.service.ts ***!
  \******************************************/
/*! exports provided: PushService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushService", function() { return PushService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let PushService = class PushService {
    constructor(oneSignal, http, router) {
        this.oneSignal = oneSignal;
        this.http = http;
        this.router = router;
    }
    configuracionInicial() {
        this.oneSignal.startInit('b9ffa2ea-ff54-40ba-85fd-7f7f29ad32d0', '325051574126');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe((noti) => {
            // do something when notification is received
            console.log("Notificacion recibida " + noti.payload.additionalData["notification"]);
            this.notiId = noti.payload.additionalData["notification"] + "";
        });
        this.oneSignal.handleNotificationOpened().subscribe((noti) => {
            // do something when a notification is opened
            console.log("Notificacion abierta " + this.notiId + noti);
            this.router.navigate(['/notice/' + this.notiId]);
        });
        //obtener uid de onsignal
        this.oneSignal.getIds().then(info => {
            this.useId = info.userId;
        });
        this.oneSignal.endInit();
    }
    onesignal(title, subtitle, detalle, uidnoti) {
        //dato a mandar
        let datos = {
            app_id: "b9ffa2ea-ff54-40ba-85fd-7f7f29ad32d0", included_segments: ["Active Users", "Inactive Users"],
            headings: { "en": title }, subtitle: { "en": subtitle },
            data: { "notification": uidnoti, "fecha": "100-210-250" }, contents: { "en": detalle }
        };
        const headers = {
            'Authorization': 'Basic MjFlYzExNmItNzNlNy00OTVmLTg5ZjUtZmYwODU0NzNkZjky'
        };
        console.log(JSON.stringify(datos));
        var url = 'https://onesignal.com/api/v1/notifications';
        return new Promise(resolve => {
            this.http.post(url, datos, { headers })
                .subscribe(data => {
                console.log(data);
                resolve(data);
            });
        });
    }
};
PushService.ctorParameters = () => [
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], PushService);



/***/ }),

/***/ "Q98m":
/*!*********************************************!*\
  !*** ./src/app/shared/Errors/listErrors.ts ***!
  \*********************************************/
/*! exports provided: listErrors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "listErrors", function() { return listErrors; });
const listErrors = {
    'auth/user-not-found': `No hay registro de usuario correspondiente a este identificador. El usuario puede haber sido eliminado`,
    'auth/invalid-email:': `La dirección de correo electrónico está mal formateada`,
    'auth/wrong-password': `Credenciales incorrectas`,
    'auth/too-many-requests': `Demasiados intentos de inicio de sesión fallidos. Por favor, inténtelo de nuevo más tarde`,
    'app/general': `Ha ocurrido un error intentelo de nuevo por favor`,
    'auth/email-already-in-use': 'La dirección de correo electrónico ya está siendo utilizada por otra cuenta.',
    'failed-precondition': 'La consulta requiere un índice.',
};


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _services_push_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/push.service */ "H+l1");







let AppComponent = class AppComponent {
    constructor(menu, PushService) {
        this.menu = menu;
        this.PushService = PushService;
        this.PushService.configuracionInicial();
        this.login = false;
        this.seccion();
    }
    toggleMenu() {
        this.menu.toggle(); //Add this method to your button click function
    }
    seccion() {
        firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].auth().onAuthStateChanged((user) => {
            if (user) {
                this.login = true;
            }
            else {
                this.login = false;
                console.log('El usuario no está conectado');
            }
        });
    }
    logoutUser() {
        this.menu.toggle(); //Add this method to your button click function
        return firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].auth().signOut();
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _services_push_service__WEBPACK_IMPORTED_MODULE_6__["PushService"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "Uvke":
/*!********************************************!*\
  !*** ./src/app/services/career.service.ts ***!
  \********************************************/
/*! exports provided: CareerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CareerService", function() { return CareerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let CareerService = class CareerService {
    constructor(firebase) {
        this.firebase = firebase;
    }
    addCareer(career) {
        career.createdAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        career.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/career').add(career);
    }
    updateCareer(career) {
        career.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        return this.firebase.collection('/career').doc(career.uid).update(career);
    }
    deleteCareer(uid) {
        this.firebase.doc(`/career/${uid}`).delete();
    }
    getCareers() {
        const ref = this.firebase
            .collection('career')
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getCareerByUid(careerUid) {
        return this.firebase
            .collection('career').doc(careerUid)
            .valueChanges({ idField: 'uid' });
    }
};
CareerService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
CareerService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CareerService);



/***/ }),

/***/ "V/Mn":
/*!*************************************************!*\
  !*** ./src/app/shared/utilities/ccValidator.ts ***!
  \*************************************************/
/*! exports provided: ccValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ccValidator", function() { return ccValidator; });
const ccValidator = (value) => {
    let newValue = value;
    // accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(newValue)) {
        return false;
    }
    //  The Luhn Algorithm. It's so pretty.
    let nCheck = 0;
    let nDigit = 0;
    let bEven = false;
    newValue = newValue.replace(/\D/g, '');
    for (let n = newValue.length - 1; n >= 0; n--) {
        const cDigit = newValue.charAt(n);
        nDigit = parseInt(cDigit, 10);
        if (bEven) {
            if ((nDigit *= 2) > 9) {
                nDigit -= 9;
            }
        }
        nCheck += nDigit;
        bEven = !bEven;
    }
    return nCheck % 10 === 0;
};


/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <ion-menu type=\"overlay\" side=\"start\" autoHide=\"true\"  menuId=\"administrador\" content-id=\"main\">\r\n    <div class=\"container-menu\">\r\n      <ion-icon (click)=\"toggleMenu()\" class=\"icon\" name=\"menu-outline\"></ion-icon>\r\n    </div>\r\n      <div class=\"container\">\r\n        <ion-item routerLink=\"/main\" (click)=\"toggleMenu()\" lines=\"none\">\r\n          <img src=\"../assets/img/inicio.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Inicio</ion-label>\r\n        </ion-item>\r\n        <ion-item   lines=\"none\"   onclick=\"window.open('http://demo.tecnologicoitca.edu.ec/sis/', '_system')\">\r\n          <img src=\"../assets/img/ofertaacademica.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Oferta Académica</ion-label>\r\n        </ion-item>\r\n        <ion-item routerLink=\"/contactus\" (click)=\"toggleMenu()\" lines=\"none\" >\r\n          <img src=\"../assets/img/finger.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Contáctanos</ion-label>\r\n        </ion-item>\r\n        <ion-item   lines=\"none\"   onclick=\"window.open('http://demo.tecnologicoitca.edu.ec/u_event_cat/carreras/', '_system')\">\r\n          <img src=\"../assets/img/cross.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Sistema Integral de Salud</ion-label>\r\n        </ion-item>\r\n        <ion-item *ngIf=\"login==true\" routerLink=\"/panel-admin\" (click)=\"toggleMenu()\" lines=\"none\" >\r\n          <img src=\"../assets/img/wrench.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Menú</ion-label>\r\n        </ion-item>\r\n        <ion-item   lines=\"none\"   onclick=\"window.open('https://www.google.com/maps?ll=0.372352,-78.121676&z=15&t=m&hl=es-US&gl=US&mapclient=embed&cid=11735464735168140925', '_system')\">\r\n          <img src=\"../assets/img/point.png\" alt=\"\" slot=\"start\">\r\n          <ion-label >Ubicación</ion-label>\r\n        </ion-item>\r\n        <ion-item   *ngIf=\"login==false\" routerLink=\"/login\" (click)=\"toggleMenu()\" lines=\"none\" >\r\n          <img src=\"../assets/img/access.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Ingresar</ion-label>\r\n        </ion-item>\r\n        <ion-item *ngIf=\"login==false\" routerLink=\"/register\" (click)=\"toggleMenu()\" lines=\"none\" >\r\n          <img src=\"../assets/img/wrench.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Regístrate Aquí!</ion-label>\r\n        </ion-item>\r\n        <ion-item *ngIf=\"login==true\" routerLink=\"/main\" (click)=\"logoutUser()\" lines=\"none\" >\r\n          <img src=\"../assets/img/out.png\" alt=\"\" slot=\"start\">\r\n          <ion-label>Cerrar sesión</ion-label>\r\n        </ion-item>\r\n        <ion-item   lines=\"none\"   onclick=\"window.open('https://nousclic.com/', '_system')\">\r\n          <img src=\"../assets/img/logo_nous.png\" alt=\"\" slot=\"end\">\r\n          <ion-label slot=\"end\">Nousclic</ion-label>\r\n        </ion-item>\r\n      </div>\r\n\r\n  </ion-menu>\r\n  <ion-router-outlet id=\"main\"></ion-router-outlet>\r\n</ion-app>\r\n");

/***/ }),

/***/ "Xb3o":
/*!***********************************************************************!*\
  !*** ./src/app/components/update-career/update-career.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilo del icono para salir del modal */\nion-toolbar ion-icon {\n  font-size: 25px;\n}\n/* Contenedor del boton */\n.container-btn {\n  margin-top: 10%;\n}\n/* Estilo del boton de registrarse */\n.btn {\n  --background: #4ac96a;\n  --box-shadow: 0px 3px 3px rgba($color:#4ac96a, $alpha: 0.3);\n  --border-radiuss: 10px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx1cGRhdGUtY2FyZWVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBDQUFBO0FBRUk7RUFDSSxlQUFBO0FBQVI7QUFLQSx5QkFBQTtBQUNBO0VBQ0ksZUFBQTtBQUZKO0FBSUEsb0NBQUE7QUFDQTtFQUNJLHFCQUFBO0VBQ0EsMkRBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7QUFESiIsImZpbGUiOiJ1cGRhdGUtY2FyZWVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogRXN0aWxvIGRlbCBpY29ubyBwYXJhIHNhbGlyIGRlbCBtb2RhbCAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qIENvbnRlbmVkb3IgZGVsIGJvdG9uICovXHJcbi5jb250YWluZXItYnRue1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG59XHJcbi8qIEVzdGlsbyBkZWwgYm90b24gZGUgcmVnaXN0cmFyc2UgKi9cclxuLmJ0biB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICM0YWM5NmE7XHJcbiAgICAtLWJveC1zaGFkb3c6IDBweCAzcHggM3B4IHJnYmEoJGNvbG9yOiM0YWM5NmEsICRhbHBoYTogMC4zKTtcclxuICAgIC0tYm9yZGVyLXJhZGl1c3M6IDEwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufSJdfQ== */");

/***/ }),

/***/ "Y+r7":
/*!*****************************************!*\
  !*** ./src/app/services/guard.guard.ts ***!
  \*****************************************/
/*! exports provided: GuardGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardGuard", function() { return GuardGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _push_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./push.service */ "H+l1");





let GuardGuard = class GuardGuard {
    constructor(router, PushService) {
        this.router = router;
        this.PushService = PushService;
    }
    canActivate(next, state) {
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].auth().onAuthStateChanged((user) => {
                if (user) {
                    if (user.emailVerified) {
                        resolve(true);
                    }
                    else {
                        this.router.navigate(['/login']);
                    }
                }
                else {
                    this.router.navigate(['/login']);
                    resolve(false);
                }
            });
        });
    }
};
GuardGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _push_service__WEBPACK_IMPORTED_MODULE_4__["PushService"] }
];
GuardGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], GuardGuard);



/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire */ "spgP");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase */ "JZFu");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "6NWb");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "wHSu");
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ "twK/");
/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ "8tEE");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "wljF");
/* harmony import */ var _components_update_career_update_career_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/update-career/update-career.component */ "2xQN");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/service-worker */ "Jho9");
















//import { ComponentsModule } from './components/components.module';




firebase__WEBPACK_IMPORTED_MODULE_9__["default"].initializeApp(src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebaseConfig);
let AppModule = class AppModule {
    constructor(library) {
        library.addIconPacks(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["fas"], _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_15__["fab"], _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_14__["far"]);
    }
};
AppModule.ctorParameters = () => [
    { type: _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FaIconLibrary"] }
];
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"], _components_update_career_update_career_component__WEBPACK_IMPORTED_MODULE_17__["UpdateCareerComponent"]],
        entryComponents: [],
        imports: [_angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_7__["AngularFireModule"].initializeApp(src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebaseConfig),
            _angular_fire__WEBPACK_IMPORTED_MODULE_7__["AngularFireModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuthModule"],
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeModule"],
            _angular_service_worker__WEBPACK_IMPORTED_MODULE_19__["ServiceWorkerModule"].register('ngsw-worker.js', {
                enabled: src_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].production,
                // Register the ServiceWorker as soon as the app is stable
                // or after 30 seconds (whichever comes first).
                registrationStrategy: 'registerWhenStable:30000'
            }),
        ],
        providers: [_ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_16__["OneSignal"], { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
    })
], AppModule);



/***/ }),

/***/ "ejrE":
/*!****************************************************!*\
  !*** ./src/app/shared/utilities/formValidation.ts ***!
  \****************************************************/
/*! exports provided: validateForm, validateDocument */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateForm", function() { return validateForm; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateDocument", function() { return validateDocument; });
/* harmony import */ var validator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! validator */ "+QwO");
/* harmony import */ var validator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(validator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ccValidator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ccValidator */ "V/Mn");


const validateForm = (form, validationRules) => {
    const errors = {};
    for (const rule in validationRules) {
        if (validationRules.hasOwnProperty(rule)) {
            switch (rule) {
                case 'required':
                    validationRules[rule].forEach((item) => {
                        if (!form[item] || validator__WEBPACK_IMPORTED_MODULE_0___default.a.isEmpty(form[item].toString())) {
                            errors[item] = 'Este campo es requerido';
                        }
                    });
                    break;
                case 'email':
                    validationRules[rule].forEach((item) => {
                        if (!validator__WEBPACK_IMPORTED_MODULE_0___default.a.isEmpty(form[item]) && !validator__WEBPACK_IMPORTED_MODULE_0___default.a.isEmail(form[item])) {
                            errors[rule] = 'Ingresa un correo electrónico válido';
                        }
                    });
                    break;
                case 'emailitca':
                    validationRules[rule].forEach((item) => {
                        if (!(/^\w+([\.-]?\w+)*@(?:|tecnologicoitca)\.(?:|edu)\.(?:|ec)+$/.test(form[item]))) {
                            errors[item] = 'Ingresa un email válido por ejemplo: jintriago21@tecnologicoitca.edu.ec';
                        }
                    });
                    break;
                case 'assertive':
                    validationRules[rule].forEach((item) => {
                        if (form[item] !== true) {
                            errors[item] = 'Este campo es requerido';
                        }
                    });
                    break;
                case 'length':
                    validationRules[rule].forEach((item) => {
                        if (typeof item === 'object') {
                            if (!validator__WEBPACK_IMPORTED_MODULE_0___default.a.isLength(form[item.field], { min: item.min }) && item.min) {
                                errors[item.field] = `El tamaño de este campo debe ser mínimo ${item.min}`;
                            }
                            if (!validator__WEBPACK_IMPORTED_MODULE_0___default.a.isLength(form[item.field], { max: item.max }) && item.max) {
                                errors[item.field] = `El tamaño de este campo debe ser máximo ${item.max}`;
                            }
                        }
                    });
                    break;
                case 'number':
                    const numberRegex = /[^0-9]+/;
                    validationRules[rule].forEach((item) => {
                        if (numberRegex.exec(form[item]) !== null) {
                            errors[rule] = 'Este campo debe ser numérico';
                        }
                    });
                    break;
                case 'noNumber':
                    const noNumberRegex = /[0-9]+/;
                    validationRules[rule].forEach((item) => {
                        if (noNumberRegex.exec(form[item]) !== null) {
                            errors[item] = 'Este campo no puede contener números';
                        }
                    });
                    break;
                case 'noSpecialCharacters':
                    const noSpecialCharactersRegex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                    validationRules[rule].forEach((item) => {
                        if (noSpecialCharactersRegex.exec(form[item]) !== null) {
                            errors[item] = 'Este campo no puede contener caracteres especiales';
                        }
                    });
                    break;
                case 'passwordConfirm':
                    validationRules[rule].forEach((item) => {
                        const password = Object.keys(item)[0];
                        const passwordConfirm = item[password];
                        const currentPassword = Object.keys(item)[1];
                        if (form[currentPassword] || form[password] || form[passwordConfirm]) {
                            if (!form[currentPassword]) {
                                errors[currentPassword] = 'Este campo es requerido';
                            }
                            if (!form[password]) {
                                errors[password] = 'Este campo es requerido';
                            }
                            if (!form[passwordConfirm]) {
                                errors[passwordConfirm] = 'Este campo es requerido';
                            }
                            if (form[password] && !form[passwordConfirm]) {
                                errors[passwordConfirm] = 'Confirma la contraseña';
                            }
                            else if (form[password] &&
                                form[passwordConfirm] &&
                                form[password] !== form[passwordConfirm]) {
                                errors[passwordConfirm] = 'Las contraseñas no coinciden';
                            }
                        }
                    });
                    break;
                case 'creditCard':
                    validationRules[rule].forEach((item) => {
                        if (!Object(_ccValidator__WEBPACK_IMPORTED_MODULE_1__["ccValidator"])(form[item])) {
                            errors[item] = 'Número de tarjeta inválido';
                        }
                    });
                    break;
                case 'document':
                    validationRules[rule].forEach(item => {
                        if (!validateDocument(form[item.field], item.type)) {
                            if (item.type === "CI") {
                                errors[item.field] = "Cédula inválida";
                            }
                            else if (item.type === "RUC") {
                                errors[item.field] = "RUC inválido";
                            }
                            else if (item.type === "PASSPORT") {
                                errors[item.field] = "Pasaporte inválido";
                            }
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    }
    return errors;
};
const UtilIdetification = {
    esCedulaPersonaNatural: (identificacion) => {
        if (identificacion.length === 10) {
            return true;
        }
        return false;
    },
    esRucPersonaNatural: (identificacion) => {
        if (identificacion.length === 13 &&
            identificacion.charAt(2) !== '6' &&
            identificacion.charAt(2) !== '9' &&
            identificacion.substring(10, 13) === '001') {
            return true;
        }
        return false;
    },
    ultimosDigitosRuc: (identificacion) => {
        if (identificacion.length === 13 && identificacion.substring(10, 13) === '001') {
            return true;
        }
        return false;
    },
    esRucPersonaJuridica: (identificacion) => {
        if (UtilIdetification.ultimosDigitosRuc(identificacion) && identificacion.charAt(2) === '9') {
            return true;
        }
        return false;
    },
    esRucEmpresaPublica: (identificacion) => {
        if (UtilIdetification.ultimosDigitosRuc(identificacion) && identificacion.charAt(2) === '6') {
            return true;
        }
        return false;
    },
    validarCedula: (identificacion, coeficientes) => {
        const id = identificacion;
        let sumaDigitosPorCoeficiente = 0;
        let valor = 0;
        for (let i = 0; i < coeficientes.length; i++) {
            const digito = id.charAt(i) * 1;
            valor = coeficientes[i] * digito;
            if (valor > 9) {
                valor = valor - 9;
            }
            sumaDigitosPorCoeficiente = sumaDigitosPorCoeficiente + valor;
        }
        let modulo = sumaDigitosPorCoeficiente % 10;
        modulo = modulo === 0 ? 10 : modulo;
        const resultado = 10 - modulo;
        const ultimoDigito = id.charAt(9) * 1;
        if (resultado === ultimoDigito) {
            return true;
        }
        return false;
    },
    validarRUC: (identificacion, coeficientes, digitoVerificador) => {
        const id = identificacion;
        const verificador = digitoVerificador * 1;
        let sumaTotalDigitosPorCoeficiente = 0;
        let digito = 0;
        let valor = 0;
        for (let i = 0; i < coeficientes.length; i++) {
            digito = id.charAt(i) * 1;
            valor = coeficientes[i] * digito;
            sumaTotalDigitosPorCoeficiente = sumaTotalDigitosPorCoeficiente + valor;
        }
        const modulo = sumaTotalDigitosPorCoeficiente % 11;
        let resultado = 0;
        if (modulo !== 0) {
            resultado = 11 - modulo;
        }
        if (resultado === verificador) {
            return true;
        }
        return false;
    },
    esClienteFinal: (identificacion) => {
        if (identificacion === '9999999999999') {
            return true;
        }
        return false;
    },
    validarProvincia: (identificacion) => {
        if (parseInt(identificacion, 10) <= 0 || parseInt(identificacion, 10) > 24) {
            return false;
        }
        return true;
    },
};
function validateCI(document) {
    const coeficientesCedula = [2, 1, 2, 1, 2, 1, 2, 1, 2];
    return (UtilIdetification.validarProvincia(document.substring(0, 2)) &&
        UtilIdetification.esCedulaPersonaNatural(document) &&
        UtilIdetification.validarCedula(document, coeficientesCedula));
}
function validateRUC(document) {
    const coeficientesCedula = [2, 1, 2, 1, 2, 1, 2, 1, 2];
    const coeficientesRucPersonaJuridica = [4, 3, 2, 7, 6, 5, 4, 3, 2];
    const coeficientesRucEmpresaPublica = [3, 2, 7, 6, 5, 4, 3, 2];
    return ((UtilIdetification.esRucPersonaNatural(document) &&
        UtilIdetification.validarCedula(document, coeficientesCedula)) ||
        (UtilIdetification.esRucPersonaJuridica(document) &&
            UtilIdetification.validarRUC(document, coeficientesRucPersonaJuridica, document.charAt(9))) ||
        (UtilIdetification.esRucEmpresaPublica(document) &&
            UtilIdetification.validarRUC(document, coeficientesRucEmpresaPublica, document.charAt(8))));
}
const validateDocument = (document, type) => {
    if (type === 'CI') {
        return validateCI(document);
    }
    if (type === 'RUC') {
        return validateRUC(document);
    }
    if (type === 'PASSPORT') {
        return true;
    }
    return false;
};


/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth-guard */ "HTFn");
/* harmony import */ var _app_services_guard_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app/services/guard.guard */ "Y+r7");





// Enviar usuarios no autorizados para iniciar sesión
const redirectUnauthorizedToLogin = () => Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_3__["redirectUnauthorizedTo"])(['/login']);
//Iniciar sesión automáticamente en usuarios
const redirectLoggedInToHome = () => Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_3__["redirectLoggedInTo"])(['/main']);
const routes = [
    {
        path: 'home',
        loadChildren: () => __webpack_require__.e(/*! import() | home-home-module */ "home-home-module").then(__webpack_require__.bind(null, /*! ./home/home.module */ "ct+p")).then(m => m.HomePageModule)
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    Object.assign({ path: 'login', loadChildren: () => Promise.all(/*! import() | login-login-module */[__webpack_require__.e("common"), __webpack_require__.e("login-login-module")]).then(__webpack_require__.bind(null, /*! ./login/login.module */ "X3zk")).then(m => m.LoginPageModule) }, Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_3__["canActivate"])(redirectLoggedInToHome)),
    Object.assign({ path: 'panel-admin', loadChildren: () => Promise.all(/*! import() | panel-admin-panel-admin-module */[__webpack_require__.e("common"), __webpack_require__.e("panel-admin-panel-admin-module")]).then(__webpack_require__.bind(null, /*! ./panel-admin/panel-admin.module */ "LRnq")).then(m => m.PanelAdminPageModule) }, Object(_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_3__["canActivate"])(redirectUnauthorizedToLogin)),
    {
        path: 'categories',
        loadChildren: () => Promise.all(/*! import() | categories-categories-module */[__webpack_require__.e("common"), __webpack_require__.e("categories-categories-module")]).then(__webpack_require__.bind(null, /*! ./categories/categories.module */ "kMJQ")).then(m => m.CategoriesPageModule)
    },
    {
        path: 'main',
        loadChildren: () => __webpack_require__.e(/*! import() | main-main-module */ "main-main-module").then(__webpack_require__.bind(null, /*! ./main/main.module */ "XpXM")).then(m => m.MainPageModule)
    },
    {
        path: 'notice/:id',
        loadChildren: () => __webpack_require__.e(/*! import() | notice-notice-module */ "notice-notice-module").then(__webpack_require__.bind(null, /*! ./notice/notice.module */ "IUlz")).then(m => m.NoticePageModule)
    },
    {
        path: 'notification',
        loadChildren: () => __webpack_require__.e(/*! import() | notification-notification-module */ "notification-notification-module").then(__webpack_require__.bind(null, /*! ./notification/notification.module */ "TLzw")).then(m => m.NotificationPageModule)
    },
    {
        path: 'contactus',
        loadChildren: () => __webpack_require__.e(/*! import() | contactus-contactus-module */ "contactus-contactus-module").then(__webpack_require__.bind(null, /*! ./contactus/contactus.module */ "7U2a")).then(m => m.ContactusPageModule)
    },
    {
        path: 'admin-notifications',
        loadChildren: () => Promise.all(/*! import() | admin-notifications-admin-notifications-module */[__webpack_require__.e("common"), __webpack_require__.e("admin-notifications-admin-notifications-module")]).then(__webpack_require__.bind(null, /*! ./admin-notifications/admin-notifications.module */ "CwMn")).then(m => m.AdminNotificationsPageModule)
    },
    {
        path: 'contacts',
        loadChildren: () => __webpack_require__.e(/*! import() | contacts-contacts-module */ "contacts-contacts-module").then(__webpack_require__.bind(null, /*! ./contacts/contacts.module */ "1s7i")).then(m => m.ContactsPageModule)
    },
    {
        path: 'register',
        loadChildren: () => Promise.all(/*! import() | register-register-module */[__webpack_require__.e("common"), __webpack_require__.e("register-register-module")]).then(__webpack_require__.bind(null, /*! ./register/register.module */ "x5bZ")).then(m => m.RegisterPageModule)
    },
    {
        path: 'panel-student',
        loadChildren: () => __webpack_require__.e(/*! import() | panel-student-panel-student-module */ "panel-student-panel-student-module").then(__webpack_require__.bind(null, /*! ./panel-student/panel-student.module */ "hv3e")).then(m => m.PanelStudentPageModule)
    },
    {
        path: 'successreg',
        loadChildren: () => __webpack_require__.e(/*! import() | successreg-successreg-module */ "successreg-successreg-module").then(__webpack_require__.bind(null, /*! ./successreg/successreg.module */ "31Bx")).then(m => m.SuccessregPageModule)
    },
    {
        path: 'upinvoice',
        loadChildren: () => Promise.all(/*! import() | upinvoice-upinvoice-module */[__webpack_require__.e("common"), __webpack_require__.e("upinvoice-upinvoice-module")]).then(__webpack_require__.bind(null, /*! ./upinvoice/upinvoice.module */ "5CEN")).then(m => m.UpinvoicePageModule),
        canActivate: [_app_services_guard_guard__WEBPACK_IMPORTED_MODULE_4__["GuardGuard"]],
    },
    {
        path: 'invoicelist',
        loadChildren: () => Promise.all(/*! import() | invoicelist-invoicelist-module */[__webpack_require__.e("common"), __webpack_require__.e("invoicelist-invoicelist-module")]).then(__webpack_require__.bind(null, /*! ./invoicelist/invoicelist.module */ "mNla")).then(m => m.InvoicelistPageModule),
        canActivate: [_app_services_guard_guard__WEBPACK_IMPORTED_MODULE_4__["GuardGuard"]],
    },
    {
        path: 'profile',
        loadChildren: () => Promise.all(/*! import() | profile-profile-module */[__webpack_require__.e("common"), __webpack_require__.e("profile-profile-module")]).then(__webpack_require__.bind(null, /*! ./profile/profile.module */ "cRhG")).then(m => m.ProfilePageModule)
    },
    {
        path: 'list-invoice-admin',
        loadChildren: () => Promise.all(/*! import() | list-invoice-admin-list-invoice-admin-module */[__webpack_require__.e("default~list-invoice-admin-list-invoice-admin-module~list-invoice-all-list-invoice-all-module"), __webpack_require__.e("list-invoice-admin-list-invoice-admin-module")]).then(__webpack_require__.bind(null, /*! ./list-invoice-admin/list-invoice-admin.module */ "BmDs")).then(m => m.ListInvoiceAdminPageModule)
    },
    {
        path: 'see-detail-invoice/:id',
        loadChildren: () => Promise.all(/*! import() | see-detail-invoice-see-detail-invoice-module */[__webpack_require__.e("common"), __webpack_require__.e("see-detail-invoice-see-detail-invoice-module")]).then(__webpack_require__.bind(null, /*! ./see-detail-invoice/see-detail-invoice.module */ "hK95")).then(m => m.SeeDetailInvoicePageModule)
    },
    {
        path: 'career',
        loadChildren: () => __webpack_require__.e(/*! import() | career-career-module */ "career-career-module").then(__webpack_require__.bind(null, /*! ./career/career.module */ "PgHP")).then(m => m.CareerPageModule)
    },
    {
        path: 'iconnselector',
        loadChildren: () => __webpack_require__.e(/*! import() | iconnselector-iconnselector-module */ "iconnselector-iconnselector-module").then(__webpack_require__.bind(null, /*! ./iconnselector/iconnselector.module */ "BdKf")).then(m => m.IconnselectorPageModule)
    },
    {
        path: 'databank',
        loadChildren: () => Promise.all(/*! import() | databank-databank-module */[__webpack_require__.e("common"), __webpack_require__.e("databank-databank-module")]).then(__webpack_require__.bind(null, /*! ./databank/databank.module */ "2hda")).then(m => m.DatabankPageModule)
    },
    {
        path: 'list-invoice-rejected',
        loadChildren: () => __webpack_require__.e(/*! import() | list-invoice-rejected-list-invoice-rejected-module */ "list-invoice-rejected-list-invoice-rejected-module").then(__webpack_require__.bind(null, /*! ./list-invoice-rejected/list-invoice-rejected.module */ "a+ut")).then(m => m.ListInvoiceRejectedPageModule)
    },
    {
        path: 'list-invoice-accepted',
        loadChildren: () => __webpack_require__.e(/*! import() | list-invoice-accepted-list-invoice-accepted-module */ "list-invoice-accepted-list-invoice-accepted-module").then(__webpack_require__.bind(null, /*! ./list-invoice-accepted/list-invoice-accepted.module */ "MZm8")).then(m => m.ListInvoiceAcceptedPageModule)
    },
    {
        path: 'list-invoice-incomplete',
        loadChildren: () => __webpack_require__.e(/*! import() | list-invoice-incomplete-list-invoice-incomplete-module */ "list-invoice-incomplete-list-invoice-incomplete-module").then(__webpack_require__.bind(null, /*! ./list-invoice-incomplete/list-invoice-incomplete.module */ "EtLs")).then(m => m.ListInvoiceIncompletePageModule)
    },
    {
        path: 'list-invoice-all',
        loadChildren: () => Promise.all(/*! import() | list-invoice-all-list-invoice-all-module */[__webpack_require__.e("default~list-invoice-admin-list-invoice-admin-module~list-invoice-all-list-invoice-all-module"), __webpack_require__.e("list-invoice-all-list-invoice-all-module")]).then(__webpack_require__.bind(null, /*! ./list-invoice-all/list-invoice-all.module */ "GGNd")).then(m => m.ListInvoiceAllPageModule)
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos para darle color de fondo a los elementos del menu */\n.container {\n  width: 100%;\n  height: 100%;\n}\n.container-menu {\n  padding: 9% 0px 0px 9%;\n  color: white;\n}\n.container-menu ion-icon {\n  font-size: 25px;\n}\n/* Estilos para darle color de fondo al header del menu */\nion-toolbar {\n  border-radius: 3%;\n  --background:rgba(1, 76, 154, 0);\n  -webkit-backdrop-filter: blur(30px);\n          backdrop-filter: blur(30px);\n  --padding-start:6%;\n  --padding-end:6%;\n}\nion-toolbar ion-icon {\n  font-size: 28px;\n  color: white;\n}\n/* Estilos para todos los elementos del menu */\nion-item {\n  --background: rgba(0,0,0,0);\n  padding: 4%;\n  --color:white;\n}\nimg {\n  width: 20px;\n}\nion-menu {\n  --background:rgba(1, 76, 154, 0.8);\n  -webkit-backdrop-filter: blur(8px);\n          backdrop-filter: blur(8px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLCtEQUFBO0FBQ0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQURKO0FBSUE7RUFDRyxzQkFBQTtFQUNBLFlBQUE7QUFESDtBQUVHO0VBQ0ksZUFBQTtBQUFQO0FBS0EseURBQUE7QUFDQTtFQUNJLGlCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUZKO0FBR0k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQURSO0FBS0EsOENBQUE7QUFDQTtFQUNJLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7QUFGSjtBQUtBO0VBQ0ksV0FBQTtBQUZKO0FBS0E7RUFDSSxrQ0FBQTtFQUNBLGtDQUFBO1VBQUEsMEJBQUE7QUFGSiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbi8qIEVzdGlsb3MgcGFyYSBkYXJsZSBjb2xvciBkZSBmb25kbyBhIGxvcyBlbGVtZW50b3MgZGVsIG1lbnUgKi9cclxuLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uY29udGFpbmVyLW1lbnV7XHJcbiAgIHBhZGRpbmc6IDklIDBweCAwcHggOSU7XHJcbiAgIGNvbG9yOndoaXRlO1xyXG4gICBpb24taWNvbntcclxuICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgfVxyXG59XHJcblxyXG5cclxuLyogRXN0aWxvcyBwYXJhIGRhcmxlIGNvbG9yIGRlIGZvbmRvIGFsIGhlYWRlciBkZWwgbWVudSAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIGJvcmRlci1yYWRpdXM6IDMlO1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYmEoMSwgNzYsIDE1NCwgMCk7XHJcbiAgICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMzBweCk7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgaW9uLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAyOHB4O1xyXG4gICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIHBhcmEgdG9kb3MgbG9zIGVsZW1lbnRvcyBkZWwgbWVudSAqL1xyXG5pb24taXRlbXtcclxuICAgIC0tYmFja2dyb3VuZDogcmdiYSgwLDAsMCwwKTtcclxuICAgIHBhZGRpbmc6IDQlO1xyXG4gICAgLS1jb2xvcjp3aGl0ZTtcclxufVxyXG5cclxuaW1ne1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbn1cclxuXHJcbmlvbi1tZW51e1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJnYmEoMSwgNzYsIDE1NCwgMC44KTtcclxuICAgIGJhY2tkcm9wLWZpbHRlcjogYmx1cig4cHgpO1xyXG59Il19 */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map