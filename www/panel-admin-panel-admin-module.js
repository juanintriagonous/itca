(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-admin-panel-admin-module"],{

/***/ "LRnq":
/*!***************************************************!*\
  !*** ./src/app/panel-admin/panel-admin.module.ts ***!
  \***************************************************/
/*! exports provided: PanelAdminPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelAdminPageModule", function() { return PanelAdminPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _panel_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-admin-routing.module */ "W8QP");
/* harmony import */ var _panel_admin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./panel-admin.page */ "j8mh");







let PanelAdminPageModule = class PanelAdminPageModule {
};
PanelAdminPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _panel_admin_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelAdminPageRoutingModule"]
        ],
        declarations: [_panel_admin_page__WEBPACK_IMPORTED_MODULE_6__["PanelAdminPage"]]
    })
], PanelAdminPageModule);



/***/ }),

/***/ "On/C":
/*!***************************************************!*\
  !*** ./src/app/panel-admin/panel-admin.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\nion-icon {\n  font-size: 40px;\n}\n\n.img-user {\n  width: 20%;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n.container-welcome h2 {\n  font-weight: 700;\n  font-size: 23px;\n}\n\n/* Estilos generales de cada card */\n\nion-col {\n  display: flex;\n  justify-content: center;\n}\n\nion-col ion-card {\n  border-radius: 15px;\n  box-shadow: none;\n  width: 90%;\n  height: 90%;\n}\n\nion-col ion-card ion-card-header {\n  display: flex;\n  justify-content: center;\n}\n\nion-col ion-card ion-card-header .circle {\n  width: 51px;\n  height: 51px;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nion-col ion-card ion-card-content {\n  text-align: center;\n  font-weight: bold;\n  color: black;\n  padding: 0 15px 15px;\n}\n\n/* Estilo de contenedor para separar las tarjetas del bienvenido */\n\n.container {\n  margin-top: 20%;\n}\n\n/* Colores de las diferentes tarjetas */\n\n.gray {\n  background: rgba(15, 101, 122, 0.2);\n}\n\n.green {\n  background: #EAFCEE;\n}\n\n.blue {\n  background: #B9EAFF;\n}\n\n.purple {\n  background: #CCE5FF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHBhbmVsLWFkbWluLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLFlBQUE7RUFDQSx3Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFBSjs7QUFFRTtFQUNFLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFDQTtFQUNJLFVBQUE7QUFFSjs7QUFBQSx3QkFBQTs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBR0o7O0FBREk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUdSOztBQURJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUdSOztBQUVBLHNDQUFBOztBQUNBO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBQUk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBRVI7O0FBQUk7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUFFUjs7QUFBSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUVBLG1DQUFBOztBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBQ0k7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFDUjs7QUFDUTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUNaOztBQUFZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBRWhCOztBQUVRO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQUFaOztBQU9BLGtFQUFBOztBQUNBO0VBQ0ksZUFBQTtBQUpKOztBQU9BLHVDQUFBOztBQUNBO0VBQ0ksbUNBQUE7QUFKSjs7QUFNQTtFQUNJLG1CQUFBO0FBSEo7O0FBS0E7RUFDSSxtQkFBQTtBQUZKOztBQUlBO0VBQ0ksbUJBQUE7QUFESiIsImZpbGUiOiJwYW5lbC1hZG1pbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2ZvbmRvIGRlIGxhIGltYWdlbiBkZWwgY29udGFpbmVyXHJcbi5jb250YWluZXJ7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8vLi4vYXNzZXRzL2ltZy9iYWNrZ3JvdW5kMi5wbmdcIik7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICB9XHJcbiAgLmgzYXp1bHtcclxuICAgIGNvbG9yOiMwMTQ4OTg7XHJcbn1cclxuLy90YWxsYSBkZWwgaWNvbm8gZW4gaW9uIGljb25cclxuaW9uLWljb257XHJcbiAgICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuLmltZy11c2Vye1xyXG4gICAgd2lkdGg6MjAlO1xyXG59XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDo1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICAgIC5jb250YWluZXItaW1nLWhlYWRlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjg5JTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qIEVzdGlsb3MgZGVsIG1lbnNhamUgZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzM3B4O1xyXG4gICAgfVxyXG4gICAgaDJ7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZ2VuZXJhbGVzIGRlIGNhZGEgY2FyZCAqL1xyXG5pb24tY29se1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgIGlvbi1jYXJke1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGhlaWdodDogOTAlO1xyXG4gICAgXHJcbiAgICAgICAgaW9uLWNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgLmNpcmNsZXtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICBpb24tY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBjb2xvcjpibGFjaztcclxuICAgICAgICAgICAgcGFkZGluZzogMCAxNXB4IDE1cHg7XHJcbiAgICBcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG4vKiBFc3RpbG8gZGUgY29udGVuZWRvciBwYXJhIHNlcGFyYXIgbGFzIHRhcmpldGFzIGRlbCBiaWVudmVuaWRvICovXHJcbi5jb250YWluZXJ7XHJcbiAgICBtYXJnaW4tdG9wOjIwJTtcclxufVxyXG5cclxuLyogQ29sb3JlcyBkZSBsYXMgZGlmZXJlbnRlcyB0YXJqZXRhcyAqL1xyXG4uZ3JheXtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMTUsIDEwMSwgMTIyLCAwLjIpO1xyXG59XHJcbi5ncmVlbntcclxuICAgIGJhY2tncm91bmQ6ICNFQUZDRUU7XHJcbn1cclxuLmJsdWV7XHJcbiAgICBiYWNrZ3JvdW5kOiAjQjlFQUZGO1xyXG59XHJcbi5wdXJwbGV7XHJcbiAgICBiYWNrZ3JvdW5kOiAjQ0NFNUZGOztcclxufSJdfQ== */");

/***/ }),

/***/ "W8QP":
/*!***********************************************************!*\
  !*** ./src/app/panel-admin/panel-admin-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: PanelAdminPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelAdminPageRoutingModule", function() { return PanelAdminPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _panel_admin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-admin.page */ "j8mh");




const routes = [
    {
        path: '',
        component: _panel_admin_page__WEBPACK_IMPORTED_MODULE_3__["PanelAdminPage"]
    }
];
let PanelAdminPageRoutingModule = class PanelAdminPageRoutingModule {
};
PanelAdminPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelAdminPageRoutingModule);



/***/ }),

/***/ "j8mh":
/*!*************************************************!*\
  !*** ./src/app/panel-admin/panel-admin.page.ts ***!
  \*************************************************/
/*! exports provided: PanelAdminPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelAdminPage", function() { return PanelAdminPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_panel_admin_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./panel-admin.page.html */ "lwOy");
/* harmony import */ var _panel_admin_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./panel-admin.page.scss */ "On/C");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/alert.service */ "3LUQ");
/* harmony import */ var _services_career_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/career.service */ "Uvke");
/* harmony import */ var _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/firebase-auth-service.service */ "1x2Z");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/user.service */ "qfBg");










let PanelAdminPage = class PanelAdminPage {
    constructor(router, toastController, alertService, userService, authService, menu, careerService) {
        this.router = router;
        this.toastController = toastController;
        this.alertService = alertService;
        this.userService = userService;
        this.authService = authService;
        this.menu = menu;
        this.careerService = careerService;
        this.user = {
            uid: '',
            email: '',
            role: '',
            fullname: '',
            isActive: true,
        };
        this.isToggled = false;
    }
    ngOnInit() {
        this.menu.enable(true);
    }
    ionViewWillEnter() {
        this.loadData();
        this.user = {
            uid: '',
            email: '',
            role: '',
            fullname: '',
            isActive: true,
        };
        this.getSessionUser();
    }
    loadData() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.user = (yield this.getSessionUser());
            this.validateProfileStudent(this.user);
            this.loadCareer(this.user.career);
        });
    }
    loadCareer(careerUid) {
        this.careerService.getCareerByUid(careerUid).subscribe((data) => {
            this.career = data;
            this.level = this.career.levels.find((e) => (e.uid === this.user.level));
        });
    }
    getSessionUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getCurrentUser();
            return new Promise((resolve, reject) => {
                this.userService.getUserById(userSession.uid).subscribe((res) => {
                    resolve(res);
                });
            });
        });
    }
    presentToast(valor) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: valor,
                duration: 2000,
            });
            toast.present();
        });
    }
    validateProfileStudent(user) {
        if (user.role === 'student' &&
            (user.phoneNumber === '' ||
                user.address === '' ||
                user.image === '')) {
            this.alertService
                .presentAlertConfirm('Perfil incompleto!!!🤔', 'Para la app funcione correctamente por favor, llene todos los datos del perfil.😎')
                .then((res) => {
                if (res) {
                    this.router.navigate(['/profile']);
                }
            });
        }
    }
    openFacebook() {
        window.open('https://www.facebook.com/Ruah-Env%C3%ADos-Express-Servicio-de-Encomiendas-y-Entregas-a-domicilio-102467271649209');
    }
    openInstagram() {
        window.open('https://www.instagram.com/ruahenviosexpress/');
    }
    openYoutube() {
        this.alertService.toastShow('Aún no contamos con youtube c:');
    }
};
PanelAdminPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_9__["UserService"] },
    { type: _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_8__["FirebaseAuthServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
    { type: _services_career_service__WEBPACK_IMPORTED_MODULE_7__["CareerService"] }
];
PanelAdminPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-panel-admin',
        template: _raw_loader_panel_admin_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_panel_admin_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PanelAdminPage);



/***/ }),

/***/ "lwOy":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/panel-admin/panel-admin.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <h3 class=\"h3azul ion-text-center\">ITCA-App</h3>\r\n    <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    <ion-menu-button slot=\"start\" menu=\"administrador\"></ion-menu-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <ion-avatar>\r\n      <img *ngIf=\"user.image==''\" src=\"../../../../assets/img/profiledefault.png\" />\r\n      <img *ngIf=\"user.image!=''\" src=\"{{user.image}}\" />\r\n    </ion-avatar>\r\n    <h1>Hola, {{user.fullname}}</h1>\r\n    <div *ngIf=\"user.role=='student'\">\r\n      <h2>Carrera:</h2>\r\n      <p >{{career?.name}} </p>\r\n      <h2>Nro de Nivel:</h2>\r\n      <p> {{level?.name}}</p>\r\n    </div>\r\n  </ion-row>\r\n  <div class=\"container\" *ngIf=\"user.role === 'student'\">\r\n    <ion-row *ngIf=\"user.role === 'student'\">\r\n      <ion-col size=\"6\">\r\n        <ion-card class=\"blue\">\r\n          <ion-card-header routerLink=\"/upinvoice\">\r\n            <div class=\"circle\">\r\n              <ion-icon name=\"document-text-outline\"></ion-icon>\r\n            </div>\r\n          </ion-card-header>\r\n          <ion-card-content routerLink=\"/upinvoice\">\r\n            Subir Pagos\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <ion-card class=\"purple\">\r\n          <ion-card-header routerLink=\"/invoicelist\">\r\n            <div class=\"circle\">\r\n              <ion-icon name=\"documents-outline\"></ion-icon>\r\n            </div>\r\n          </ion-card-header>\r\n          <ion-card-content routerLink=\"/invoicelist\">\r\n            Historial de Pagos\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row *ngIf=\"user.role === 'student'\">\r\n      <ion-col size=\"6\">\r\n        <ion-card class=\"gray\">\r\n          <ion-card-header routerLink=\"/main\">\r\n            <div class=\"circle\">\r\n              <ion-icon name=\"arrow-undo-outline\"></ion-icon>\r\n            </div>\r\n          </ion-card-header>\r\n          <ion-card-content routerLink=\"/main\">\r\n            Seccion de Noticias\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n        <ion-card class=\"blue\">\r\n          <ion-card-header routerLink=\"/profile\">\r\n            <div class=\"circle\">\r\n              <ion-icon name=\"create-outline\"></ion-icon>\r\n            </div>\r\n          </ion-card-header>\r\n          <ion-card-content routerLink=\"/profile\">\r\n            Editar Mi Perfil\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </div>\r\n  <ion-row *ngIf=\"user.role === 'admin'\">\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"gray\">\r\n        <ion-card-header routerLink=\"/notification\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"add-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/notification\">\r\n          Nueva Noticia\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"green\">\r\n        <ion-card-header routerLink=\"/admin-notifications\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"notifications-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/admin-notifications\">\r\n          Editar Noticias\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row *ngIf=\"user.role === 'admin'\">\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"blue\">\r\n        <ion-card-header routerLink=\"/contacts\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"chatbubble-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/contacts\">\r\n          Formas de contácto\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"purple\">\r\n        <ion-card-header routerLink=\"/categories\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"copy-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/categories\">\r\n          Categorías de Noticias\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>   \r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"gray\">\r\n        <ion-card-header routerLink=\"/career\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"school-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/career\">\r\n          Gestor de Carreras\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"green\">\r\n        <ion-card-header routerLink=\"/profile\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"create-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/profile\">\r\n          Editar Mi Perfil\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"blue\">\r\n        <ion-card-header routerLink=\"/list-invoice-all\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"cash-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/list-invoice-all\">\r\n          Pagos recibidos\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n      <ion-card class=\"purple\">\r\n        <ion-card-header routerLink=\"/databank\">\r\n          <div class=\"circle\">\r\n            <ion-icon name=\"wallet-outline\"></ion-icon>\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content routerLink=\"/databank\">\r\n          Datos Bancarios ITCA\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=panel-admin-panel-admin-module.js.map