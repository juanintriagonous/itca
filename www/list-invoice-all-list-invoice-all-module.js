(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["list-invoice-all-list-invoice-all-module"],{

/***/ "5PdZ":
/*!*************************************************************!*\
  !*** ./src/app/list-invoice-all/list-invoice-all.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGxpc3QtaW52b2ljZS1hbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBQTtFQUNBLHdDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQUFKOztBQUVBO0VBQ00sY0FBQTtBQUNOOztBQUNBLHdCQUFBOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFFSjs7QUFBSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBRVI7O0FBQUk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBRVIiLCJmaWxlIjoibGlzdC1pbnZvaWNlLWFsbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2ZvbmRvIGRlIGxhIGltYWdlbiBkZWwgY29udGFpbmVyXHJcbi5jb250YWluZXJ7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8vLi4vYXNzZXRzL2ltZy9iYWNrZ3JvdW5kMi5wbmdcIik7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICB9XHJcbi5oM2F6dWx7XHJcbiAgICAgIGNvbG9yOiMwMTQ4OTg7XHJcbiAgfVxyXG4vKiBFc3RpbG9zIGRlbCBoZWFkZXIgICovXHJcbmlvbi10b29sYmFye1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OjYlO1xyXG4gICAgLS1wYWRkaW5nLWVuZDo2JTtcclxuICAgIC0tcGFkZGluZy10b3A6NiU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcclxuXHJcbiAgICAuaW1nLWxvZ297XHJcbiAgICAgICAgd2lkdGg6NTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVyLWltZy1oZWFkZXJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo4OSU7XHJcbiAgICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "GGNd":
/*!*************************************************************!*\
  !*** ./src/app/list-invoice-all/list-invoice-all.module.ts ***!
  \*************************************************************/
/*! exports provided: ListInvoiceAllPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceAllPageModule", function() { return ListInvoiceAllPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _list_invoice_all_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-invoice-all-routing.module */ "Ph66");
/* harmony import */ var _list_invoice_all_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-invoice-all.page */ "p5Kr");







let ListInvoiceAllPageModule = class ListInvoiceAllPageModule {
};
ListInvoiceAllPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_invoice_all_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListInvoiceAllPageRoutingModule"]
        ],
        declarations: [_list_invoice_all_page__WEBPACK_IMPORTED_MODULE_6__["ListInvoiceAllPage"]]
    })
], ListInvoiceAllPageModule);



/***/ }),

/***/ "Ph66":
/*!*********************************************************************!*\
  !*** ./src/app/list-invoice-all/list-invoice-all-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ListInvoiceAllPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceAllPageRoutingModule", function() { return ListInvoiceAllPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _list_invoice_all_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-invoice-all.page */ "p5Kr");




const routes = [
    {
        path: '',
        component: _list_invoice_all_page__WEBPACK_IMPORTED_MODULE_3__["ListInvoiceAllPage"]
    }
];
let ListInvoiceAllPageRoutingModule = class ListInvoiceAllPageRoutingModule {
};
ListInvoiceAllPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListInvoiceAllPageRoutingModule);



/***/ }),

/***/ "k612":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/list-invoice-all/list-invoice-all.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img\r\n        slot=\"end\"\r\n        class=\"img-logo ion-text-center\"\r\n        src=\"../../assets/img/logo.png\"\r\n        alt=\"\"\r\n      />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"container\">\r\n  <ion-card class=\"card-invoice\" *ngFor=\"let invoice of invoices\">\r\n    <ion-card-header (click)=\"goToSeeVoucher(invoice)\">\r\n      <ion-card-title>{{invoice.identifynumber}}</ion-card-title>\r\n      <ion-card-subtitle>{{invoice.updateAt.toDate() | date:'dd/MM/yyyy' }}</ion-card-subtitle>\r\n    </ion-card-header>\r\n    <ion-card-content (click)=\"goToSeeVoucher(invoice)\">\r\n     <p><strong>Estudiante: </strong>{{invoice?.studentName}}</p>\r\n     <p><strong>Banco: </strong>{{invoice.bank}}</p>\r\n     <p><strong>Carrera: </strong>{{invoice?.career}}</p>\r\n     <p><strong>Nivel: </strong>{{invoice?.level}}</p>\r\n     <p><strong>Paralelo: </strong>{{invoice?.level}}</p>\r\n     <p><strong>Valor:</strong> ${{invoice.value}} </p>\r\n     <p><strong>Razón del pago:  </strong>{{invoice.reason}}</p>\r\n     <p><strong>Detalle del pago:  </strong>{{invoice.detail}}</p>\r\n    </ion-card-content>\r\n  </ion-card>\r\n  <ion-button color=\"success\" (click)=\"exportXLS()\">Exportar a Excel</ion-button>\r\n</ion-content>\r\n");

/***/ }),

/***/ "p5Kr":
/*!***********************************************************!*\
  !*** ./src/app/list-invoice-all/list-invoice-all.page.ts ***!
  \***********************************************************/
/*! exports provided: ListInvoiceAllPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceAllPage", function() { return ListInvoiceAllPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_list_invoice_all_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./list-invoice-all.page.html */ "k612");
/* harmony import */ var _list_invoice_all_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list-invoice-all.page.scss */ "5PdZ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_invoicedetails_invoicedetails_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/invoicedetails/invoicedetails.component */ "IPoj");
/* harmony import */ var _services_banks_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/banks.service */ "3mc2");
/* harmony import */ var _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/upinvoice.service */ "b8cs");
/* harmony import */ var _services_xls_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/xls.service */ "lzbr");









//xls export

let ListInvoiceAllPage = class ListInvoiceAllPage {
    constructor(upinvoiceService, router, modalController, bankService, xlsService) {
        this.upinvoiceService = upinvoiceService;
        this.router = router;
        this.modalController = modalController;
        this.bankService = bankService;
        this.xlsService = xlsService;
    }
    ngOnInit() {
        var _a;
        this.getInvoices();
        this.getBankbyId((_a = this.invoice) === null || _a === void 0 ? void 0 : _a.bank);
    }
    getInvoices() {
        this.upinvoiceService.getAllInvoices().subscribe(res => {
            this.invoices = res;
            // if (!this.invoices) {
            //   const message = 'No exiten Comprobantes registrados 😣';
            // }
        });
    }
    getBankbyId(bankUid) {
        return this.bankService.getBankByUid(bankUid).subscribe(res => {
            this.bank = res;
            this.banco = this.bank.name;
            console.log(this.banco);
        });
    }
    exportXLS() {
        this.xlsService.exportToExcel(this.invoices, 'pagos recibidos');
    }
    //update
    UpdateInvoice(id, name, description, levels, tittleType, isActivate) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_invoicedetails_invoicedetails_component__WEBPACK_IMPORTED_MODULE_6__["InvoicedetailsComponent"],
                cssClass: 'my-custom-class',
                componentProps: {
                    'id': id,
                    'name': name,
                    'description': description,
                    'levels': levels,
                    'tittleType': tittleType,
                    'isActivate': isActivate,
                }
            });
            return yield modal.present();
        });
    }
    goToSeeVoucher(invoice) {
        this.router.navigate(['see-detail-invoice/', invoice.uid]);
    }
};
ListInvoiceAllPage.ctorParameters = () => [
    { type: _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_8__["UpinvoiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _services_banks_service__WEBPACK_IMPORTED_MODULE_7__["BanksService"] },
    { type: _services_xls_service__WEBPACK_IMPORTED_MODULE_9__["XlsService"] }
];
ListInvoiceAllPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-list-invoice-all',
        template: _raw_loader_list_invoice_all_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_list_invoice_all_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ListInvoiceAllPage);



/***/ })

}]);
//# sourceMappingURL=list-invoice-all-list-invoice-all-module.js.map