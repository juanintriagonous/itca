(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["categories-categories-module"],{

/***/ "1n1Y":
/*!***********************************************!*\
  !*** ./src/app/categories/categories.page.ts ***!
  \***********************************************/
/*! exports provided: CategoriesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesPage", function() { return CategoriesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_categories_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./categories.page.html */ "zDdw");
/* harmony import */ var _categories_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./categories.page.scss */ "ArCr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/firebase-auth-service.service */ "1x2Z");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_updatecategory_updatecategory_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/updatecategory/updatecategory.component */ "Ki8D");





//Import AngularFirestore to make Queries.



let CategoriesPage = class CategoriesPage {
    constructor(toastController, authService, firebase, modalController) {
        this.toastController = toastController;
        this.authService = authService;
        this.firebase = firebase;
        this.modalController = modalController;
        this.Category = { name: "", description: "", order: "", state: "" };
    }
    ngOnInit() {
        this.infoOrder();
        this.firebase
            .collection("/category").snapshotChanges().subscribe(res => {
            if (res) {
                this.Categories = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()["name"],
                        description: e.payload.doc.data()["description"],
                        order: e.payload.doc.data()["order"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    AddCategory() {
        if (this.Category.name != '' && this.Category.description != '' && this.Category.state != '') {
            let addCategory = {};
            addCategory['name'] = this.Category.name;
            addCategory['description'] = this.Category.description;
            addCategory['order'] = this.Category.order;
            addCategory['state'] = this.Category.state;
            this.firebase.collection('/category/').add(addCategory).then(() => {
                this.Category = { name: '', description: '', order: '', state: '' };
                this.mensaje("Categoría Generada");
            });
        }
        else {
            this.mensaje("Ingrese todos los datos");
        }
    }
    infoOrder() {
        this.firebase.collection("/order", (ref) => ref.where("state", "==", "public").orderBy('value', 'asc')).snapshotChanges().subscribe(res => {
            if (res) {
                this.Orders = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        value: e.payload.doc.data()["value"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    update(id, name, order, description, state) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_updatecategory_updatecategory_component__WEBPACK_IMPORTED_MODULE_7__["UpdatecategoryComponent"],
                cssClass: 'my-custom-class',
                componentProps: {
                    'id': id,
                    'name': name,
                    'description': description,
                    'order': order,
                    'state': state
                }
            });
            return yield modal.present();
        });
    }
    delete(valor) {
        this.firebase.doc('/category/' + valor).delete();
        this.mensaje("Categoría eliminada");
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000
            });
            toast.present();
        });
    }
};
CategoriesPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseAuthServiceService"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] }
];
CategoriesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-categories',
        template: _raw_loader_categories_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_categories_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CategoriesPage);



/***/ }),

/***/ "1qrh":
/*!*********************************************************!*\
  !*** ./src/app/categories/categories-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: CategoriesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesPageRoutingModule", function() { return CategoriesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _categories_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./categories.page */ "1n1Y");




const routes = [
    {
        path: '',
        component: _categories_page__WEBPACK_IMPORTED_MODULE_3__["CategoriesPage"]
    }
];
let CategoriesPageRoutingModule = class CategoriesPageRoutingModule {
};
CategoriesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CategoriesPageRoutingModule);



/***/ }),

/***/ "ArCr":
/*!*************************************************!*\
  !*** ./src/app/categories/categories.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos del icono de subir categoria */\n\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n/* Estilos de la tarjeta de los inputs */\n\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n\n.container-card ion-card {\n  width: 90%;\n  background-color: #DDF2FB;\n  box-shadow: none;\n  border-radius: 15px;\n}\n\n.container-card ion-card ion-card-content {\n  padding: 20px;\n  padding-top: 10px;\n}\n\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n\n.icon {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGNhdGVnb3JpZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBQTtFQUNBLHdDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQUFKOztBQUVBO0VBQ00sY0FBQTtBQUNOOztBQUNBLHdCQUFBOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFFSjs7QUFBSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBRVI7O0FBQUk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBRVI7O0FBQ0Esc0NBQUE7O0FBQ0E7RUFDSSxzQkFBQTtFQUNBLGVBQUE7QUFFSjs7QUFESTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFHUjs7QUFESTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUdSOztBQUNBLG9DQUFBOztBQUNBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUFFSjs7QUFESTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUdSOztBQUNBLHlDQUFBOztBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQUVKOztBQURJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFHUjs7QUFDQSx3Q0FBQTs7QUFDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUVKOztBQURJO0VBQ0ksVUFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUdSOztBQUZRO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0FBSVo7O0FBSFk7RUFDSSxXQUFBO0VBQ0EsdUJBQUE7QUFLaEI7O0FBSGM7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUFLbEI7O0FBSGM7RUFDRSxvQkFBQTtBQUtoQjs7QUFJQTtFQUNJLGtCQUFBO0FBREoiLCJmaWxlIjoiY2F0ZWdvcmllcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2ZvbmRvIGRlIGxhIGltYWdlbiBkZWwgY29udGFpbmVyXHJcbi5jb250YWluZXJ7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8vLi4vYXNzZXRzL2ltZy9iYWNrZ3JvdW5kMi5wbmdcIik7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICB9XHJcbi5oM2F6dWx7XHJcbiAgICAgIGNvbG9yOiMwMTQ4OTg7XHJcbiAgfVxyXG4vKiBFc3RpbG9zIGRlbCBoZWFkZXIgICovXHJcbmlvbi10b29sYmFye1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OjYlO1xyXG4gICAgLS1wYWRkaW5nLWVuZDo2JTtcclxuICAgIC0tcGFkZGluZy10b3A6NiU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2JTtcclxuXHJcbiAgICAuaW1nLWxvZ297XHJcbiAgICAgICAgd2lkdGg6NTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVyLWltZy1oZWFkZXJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo4OSU7XHJcbiAgICB9XHJcbn1cclxuLyogRXN0aWxvcyBkZWwgbWVuc2FqZSBkZSBiaWVudmVuaWRvICovXHJcbi5jb250YWluZXItd2VsY29tZXtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBwe1xyXG4gICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgICBmb250LXNpemU6IDMzcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIHRpdHVsbyBkZSBsYSBwYWdpbmEgKi9cclxuLmNvbnRhaW5lci10aXRsZXtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgIHB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBpY29ubyBkZSBzdWJpciBjYXRlZ29yaWEgKi9cclxuLmNvbnRhaW5lci1pY29ue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAuY2lyY2xle1xyXG4gICAgICAgIHdpZHRoOiA1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjQ0JEOERDO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlIGxhIHRhcmpldGEgZGUgbG9zIGlucHV0cyAqL1xyXG4uY29udGFpbmVyLWNhcmR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6I0RERjJGQjsgXHJcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgIGlvbi1jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBpb24tc2VsZWN0IHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgICBjb2xvcjogYmxhY2s7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlvbi1pdGVte1xyXG4gICAgICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOjEwcHg7XHJcbiAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuLmljb24ge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4iXX0= */");

/***/ }),

/***/ "Ki8D":
/*!***********************************************************************!*\
  !*** ./src/app/components/updatecategory/updatecategory.component.ts ***!
  \***********************************************************************/
/*! exports provided: UpdatecategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatecategoryComponent", function() { return UpdatecategoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_updatecategory_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./updatecategory.component.html */ "dV0w");
/* harmony import */ var _updatecategory_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./updatecategory.component.scss */ "bLHA");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");






let UpdatecategoryComponent = class UpdatecategoryComponent {
    constructor(firebase, toastController, modalController) {
        this.firebase = firebase;
        this.toastController = toastController;
        this.modalController = modalController;
    }
    ngOnInit() { }
    update(name, description, state) {
        if (this.name != '' && this.description != '' && this.state != '') {
            let category = {};
            category['name'] = name;
            category['description'] = description;
            category['state'] = state;
            this.firebase.doc('/category/' + this.id).update(category).then(() => {
                this.quit();
                this.mensaje("Categoria Actualizada");
            });
        }
        else {
            this.mensaje("Todos los datos debe estar llenos");
        }
    }
    quit() {
        this.modalController.dismiss();
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000
            });
            toast.present();
        });
    }
};
UpdatecategoryComponent.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] }
];
UpdatecategoryComponent.propDecorators = {
    id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    name: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    description: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    state: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
UpdatecategoryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-updatecategory',
        template: _raw_loader_updatecategory_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_updatecategory_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpdatecategoryComponent);



/***/ }),

/***/ "bLHA":
/*!*************************************************************************!*\
  !*** ./src/app/components/updatecategory/updatecategory.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Estilos del header  */\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\nion-toolbar ion-icon {\n  font-size: 25px;\n}\n/* Estilos del mensaje de bienvenido */\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n/* Estilos del titulo de la pagina */\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n/* Estilos del icono de subir categoria */\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n}\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n/* Estilos de la tarjeta de los inputs */\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n.container-card ion-card {\n  width: 90%;\n  background-color: #DDF2FB;\n  box-shadow: none;\n  border-radius: 15px;\n}\n.container-card ion-card ion-card-content {\n  padding: 20px;\n}\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n/* Estilos del contenedor del boton para agregar imagen */\n.icon {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx1cGRhdGVjYXRlZ29yeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSx3QkFBQTtBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFBSjtBQUVJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFBUjtBQUVJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUFSO0FBRUk7RUFDSSxlQUFBO0FBQVI7QUFJQSxzQ0FBQTtBQUNBO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0FBREo7QUFFSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFBUjtBQUVJO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBQVI7QUFJQSxvQ0FBQTtBQUNBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUFESjtBQUVJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBQVI7QUFNQSx5Q0FBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQUhKO0FBSUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUZSO0FBTUEsd0NBQUE7QUFDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUhKO0FBSUk7RUFDSSxVQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBRlI7QUFHUTtFQUNJLGFBQUE7QUFEWjtBQUVZO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0FBQWhCO0FBRWM7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUFBbEI7QUFFYztFQUNFLG9CQUFBO0FBQWhCO0FBT0EseURBQUE7QUFHQTtFQUNJLGtCQUFBO0FBTkoiLCJmaWxlIjoidXBkYXRlY2F0ZWdvcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLyogRXN0aWxvcyBkZWwgaGVhZGVyICAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIC0tcGFkZGluZy1zdGFydDo2JTtcclxuICAgIC0tcGFkZGluZy1lbmQ6NiU7XHJcbiAgICAtLXBhZGRpbmctdG9wOjYlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNiU7XHJcblxyXG4gICAgLmltZy1sb2dve1xyXG4gICAgICAgIHdpZHRoOjUxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lci1pbWctaGVhZGVye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6ODklO1xyXG4gICAgfVxyXG4gICAgaW9uLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBtZW5zYWplIGRlIGJpZW52ZW5pZG8gKi9cclxuLmNvbnRhaW5lci13ZWxjb21le1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzNweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGl0dWxvIGRlIGxhIHBhZ2luYSAqL1xyXG4uY29udGFpbmVyLXRpdGxle1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICAgcHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuLyogRXN0aWxvcyBkZWwgaWNvbm8gZGUgc3ViaXIgY2F0ZWdvcmlhICovXHJcbi5jb250YWluZXItaWNvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0NCRDhEQztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZSBsYSB0YXJqZXRhIGRlIGxvcyBpbnB1dHMgKi9cclxuLmNvbnRhaW5lci1jYXJke1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaW9uLWNhcmR7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNEREYyRkI7IFxyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBpb24tY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgICAgICBpb24tc2VsZWN0IHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgICBjb2xvcjogYmxhY2s7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlvbi1pdGVte1xyXG4gICAgICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOjEwcHg7XHJcbiAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIGNvbnRlbmVkb3IgZGVsIGJvdG9uIHBhcmEgYWdyZWdhciBpbWFnZW4gKi9cclxuXHJcblxyXG4uaWNvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "dV0w":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/updatecategory/updatecategory.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-icon (click)=\"quit()\"  name=\"arrow-back-outline\" slot=start></ion-icon>\r\n    <ion-row class=\"container-img-header\">\r\n      <img class=\"img-logo\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n\r\n<ion-content>  \r\n<ion-row class=\"container-title\">\r\n  <ion-col size=\"12\">\r\n    <p>Actualizar Categoría</p>\r\n  </ion-col>\r\n  <!--     <ion-col size=\"12\">\r\n    <div class=\"container-icon\">\r\n      <div class=\"circle\">\r\n        <img src=\"../../assets/img/add-notification.png\" alt=\"\">\r\n      </div>\r\n      <p>Agregar icono</p>\r\n    </div>\r\n  </ion-col> -->\r\n</ion-row>\r\n\r\n<ion-row class=\"container-card\">\r\n  <ion-card>\r\n    <ion-card-content class=\"form\">\r\n      <p>Nombre</p>\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"text\" [(ngModel)]=\"name\"></ion-input>\r\n      </ion-item>\r\n      <p>Descripción</p>\r\n      <ion-item lines=\"none\">\r\n        <ion-textarea [(ngModel)]=\"description\"></ion-textarea>\r\n      </ion-item>\r\n      <p>Estado</p>\r\n      <ion-item lines=\"none\">\r\n        <ion-select [(ngModel)]=\"state\">\r\n          <ion-select-option value=\"public\">Público</ion-select-option>\r\n          <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n    </ion-card-content>\r\n\r\n  </ion-card>\r\n</ion-row>\r\n<ion-row class=\"container-btn\">\r\n  <ion-button (click)=\"update( name,description,state)\">\r\n    Actualizar\r\n  </ion-button>\r\n</ion-row>\r\n\r\n</ion-content>\r\n\r\n\r\n");

/***/ }),

/***/ "kMJQ":
/*!*************************************************!*\
  !*** ./src/app/categories/categories.module.ts ***!
  \*************************************************/
/*! exports provided: CategoriesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesPageModule", function() { return CategoriesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _categories_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./categories-routing.module */ "1qrh");
/* harmony import */ var _categories_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./categories.page */ "1n1Y");
/* harmony import */ var _components_updatecategory_updatecategory_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/updatecategory/updatecategory.component */ "Ki8D");








let CategoriesPageModule = class CategoriesPageModule {
};
CategoriesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _categories_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoriesPageRoutingModule"]
        ],
        entryComponents: [_components_updatecategory_updatecategory_component__WEBPACK_IMPORTED_MODULE_7__["UpdatecategoryComponent"]],
        declarations: [_components_updatecategory_updatecategory_component__WEBPACK_IMPORTED_MODULE_7__["UpdatecategoryComponent"], _categories_page__WEBPACK_IMPORTED_MODULE_6__["CategoriesPage"]]
    })
], CategoriesPageModule);



/***/ }),

/***/ "zDdw":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categories/categories.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">TCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content >\r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Bienvenido</h1>\r\n    <p>Administrador</p>\r\n  </ion-row>\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Crear Categorías</p>\r\n    </ion-col>\r\n    <!--     <ion-col size=\"12\">\r\n      <div class=\"container-icon\">\r\n        <div class=\"circle\">\r\n          <img src=\"../../assets/img/add-notification.png\" alt=\"\">\r\n        </div>\r\n        <p>Agregar icono</p>\r\n      </div>\r\n    </ion-col> -->\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-card\">\r\n    <ion-card>\r\n      <ion-card-content class=\"form\">\r\n        <p>Nombre</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"Category.name\"></ion-input>\r\n        </ion-item>\r\n        <p>Descripción</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-textarea [(ngModel)]=\"Category.description\"></ion-textarea>\r\n        </ion-item>\r\n        <p>Orden</p>\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Orden\" [(ngModel)]=\"Category.order\">\r\n            <div *ngFor=\"let order of Orders; let i = index\">\r\n              <ion-select-option value=\"{{order.value}}\">{{order.value}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n        <p>Estado</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-select [(ngModel)]=\"Category.state\">\r\n            <ion-select-option value=\"public\">Público</ion-select-option>\r\n            <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-card-content>\r\n\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn \">\r\n    <ion-button (click)=\"AddCategory()\" >\r\n      Registrar\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Administrar Categorías</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-card margindown\" *ngFor=\"let category of Categories; let i = index\" style=\"margin-top: 20px;\">\r\n    <ion-card>\r\n      <ion-card-content class=\"form\">\r\n        <p>Nombre</p>\r\n        <ion-item lines=\"none\">\r\n          {{category.name}}\r\n        </ion-item>\r\n        <p>Descripción</p>\r\n        <ion-item lines=\"none\">\r\n          {{category.description}}\r\n        </ion-item>\r\n        <p>Estado</p>\r\n        <ion-item lines=\"none\">\r\n          {{category.state}}\r\n        </ion-item>\r\n        <p>Acción</p>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <ion-item lines=\"none\" (click)=\"delete( category.id)\">\r\n              <ion-label class=\"icon\">\r\n                <ion-icon name=\"trash-outline\" ></ion-icon>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-col>\r\n\r\n          <ion-col size=\"6\">\r\n            <ion-item lines=\"none\" (click)=\"update( category.id,category.name,category.description,category.state)\">\r\n              <ion-label class=\"icon\">\r\n                <ion-icon name=\"pencil-outline\" ></ion-icon>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-card-content>\r\n\r\n    </ion-card>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=categories-categories-module.js.map