(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["successreg-successreg-module"],{

/***/ "31Bx":
/*!*************************************************!*\
  !*** ./src/app/successreg/successreg.module.ts ***!
  \*************************************************/
/*! exports provided: SuccessregPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessregPageModule", function() { return SuccessregPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _successreg_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./successreg-routing.module */ "DK3o");
/* harmony import */ var _successreg_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./successreg.page */ "M8ly");







let SuccessregPageModule = class SuccessregPageModule {
};
SuccessregPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _successreg_routing_module__WEBPACK_IMPORTED_MODULE_5__["SuccessregPageRoutingModule"]
        ],
        declarations: [_successreg_page__WEBPACK_IMPORTED_MODULE_6__["SuccessregPage"]]
    })
], SuccessregPageModule);



/***/ }),

/***/ "DK3o":
/*!*********************************************************!*\
  !*** ./src/app/successreg/successreg-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: SuccessregPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessregPageRoutingModule", function() { return SuccessregPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _successreg_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./successreg.page */ "M8ly");




const routes = [
    {
        path: '',
        component: _successreg_page__WEBPACK_IMPORTED_MODULE_3__["SuccessregPage"]
    }
];
let SuccessregPageRoutingModule = class SuccessregPageRoutingModule {
};
SuccessregPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SuccessregPageRoutingModule);



/***/ }),

/***/ "M8ly":
/*!***********************************************!*\
  !*** ./src/app/successreg/successreg.page.ts ***!
  \***********************************************/
/*! exports provided: SuccessregPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessregPage", function() { return SuccessregPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_successreg_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./successreg.page.html */ "lpe8");
/* harmony import */ var _successreg_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./successreg.page.scss */ "SZeL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let SuccessregPage = class SuccessregPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    back() {
        this.router.navigate(['/main']);
    }
};
SuccessregPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
SuccessregPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-successreg',
        template: _raw_loader_successreg_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_successreg_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SuccessregPage);



/***/ }),

/***/ "SZeL":
/*!*************************************************!*\
  !*** ./src/app/successreg/successreg.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n  text-align: center;\n}\n\nion-toolbar .img-logo {\n  width: 25%;\n  height: 25%;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n.check {\n  width: 40%;\n  padding-bottom: 20%;\n}\n\n.container {\n  text-align: center;\n}\n\nh1 {\n  color: #0f0f0f;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 20px;\n  line-height: 28px;\n  padding-bottom: 10%;\n}\n\nh4 {\n  color: #0f0f0f;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 18px;\n}\n\n.btn {\n  /* Green */\n  margin-top: 5%;\n  margin-left: 10%;\n  margin-right: 10%;\n  background: #2b2b22;\n  height: 60px;\n  width: 80%;\n  box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  border-radius: 15px;\n  color: #ffffff;\n  font-size: 18px;\n  font-weight: bold;\n  margin-bottom: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHN1Y2Nlc3NyZWcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBQTtFQUNBLHdDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQUFKOztBQUVBO0VBQ00sY0FBQTtBQUNOOztBQUVBLHdCQUFBOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUNJO0VBQ0ksVUFBQTtFQUNBLFdBQUE7QUFDUjs7QUFFSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFBUjs7QUFJQTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtBQURKOztBQUlBO0VBQ0ksa0JBQUE7QUFESjs7QUFJQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFESjs7QUFJRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBREo7O0FBSUU7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0RBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQURKIiwiZmlsZSI6InN1Y2Nlc3NyZWcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy9mb25kbyBkZSBsYSBpbWFnZW4gZGVsIGNvbnRhaW5lclxyXG4uY29udGFpbmVye1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIucG5nXCIpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgfVxyXG4uaDNhenVse1xyXG4gICAgICBjb2xvcjojMDE0ODk4O1xyXG4gIH1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDoyNSU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNSU7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVyLWltZy1oZWFkZXJ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo4OSU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jaGVja3tcclxuICAgIHdpZHRoOjQwJTsgICAgXHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjAlO1xyXG59XHJcblxyXG4uY29udGFpbmVye1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBjb2xvcjogIzBmMGYwZjtcclxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDI4cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTAlO1xyXG4gIH1cclxuXHJcbiAgaDQge1xyXG4gICAgY29sb3I6ICMwZjBmMGY7XHJcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIH1cclxuXHJcbiAgLmJ0bntcclxuICAgIC8qIEdyZWVuICovXHJcbiAgICBtYXJnaW4tdG9wOiA1JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICAgIGJhY2tncm91bmQ6ICMyYjJiMjI7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDRweCAyMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gIH0iXX0= */");

/***/ }),

/***/ "lpe8":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/successreg/successreg.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <img class=\"img-logo\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    <ion-back-button slot=\"start\" defaultHref=\"main\"></ion-back-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"container\">\r\n    <h1>Registro Exitoso</h1>\r\n    <img class=\"check\" src=\"../../assets/img/check.png\" alt=\"\" />\r\n    <h4>Revisa tu bandeja de </h4>\r\n  <h4>entrada para activar tu cuenta</h4>\r\n  <button class=\"btn\" (click)=\"back()\">Volver al Inicio</button>\r\n  </div>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=successreg-successreg-module.js.map