(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contactus-contactus-module"],{

/***/ "7U2a":
/*!***********************************************!*\
  !*** ./src/app/contactus/contactus.module.ts ***!
  \***********************************************/
/*! exports provided: ContactusPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactusPageModule", function() { return ContactusPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _contactus_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contactus-routing.module */ "vRFT");
/* harmony import */ var _contactus_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contactus.page */ "d/vQ");







let ContactusPageModule = class ContactusPageModule {
};
ContactusPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contactus_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactusPageRoutingModule"]
        ],
        declarations: [_contactus_page__WEBPACK_IMPORTED_MODULE_6__["ContactusPage"]]
    })
], ContactusPageModule);



/***/ }),

/***/ "BY/O":
/*!***********************************************!*\
  !*** ./src/app/contactus/contactus.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del banner  */\n\n.banner {\n  padding: 0px 5%;\n  margin-top: 2%;\n  position: relative;\n  z-index: 1;\n}\n\n.banner ion-col .card-banner {\n  width: 100%;\n  height: 210px;\n  border-radius: 21px;\n  background-repeat: no-repeat;\n  background-size: cover;\n  color: white;\n  position: relative;\n  z-index: 1;\n}\n\n.banner ion-col .card-banner ion-card-header {\n  color: white;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.banner ion-col .card-banner ion-card-header .container-name {\n  color: #737373;\n  display: flex;\n  align-items: center;\n}\n\n.banner ion-col .card-banner ion-card-header .container-name p {\n  margin: 0px;\n}\n\n.banner ion-col .card-banner ion-card-header .container-name ion-icon {\n  color: black;\n  font-size: 30px;\n}\n\n.banner ion-col .card-banner ion-card-content {\n  margin-top: 5%;\n  text-align: left;\n  padding-left: 5%;\n  color: white;\n}\n\n.banner ion-col .card-banner::before {\n  content: \"\";\n  position: absolute;\n  background: rgba(0, 0, 0, 0.6);\n  right: 0px;\n  width: 100%;\n  height: 100%;\n  z-index: -1;\n}\n\n/* Estilos del tiempo de la noticia */\n\n.time {\n  display: flex;\n  align-items: center;\n}\n\n.time p {\n  margin: 0px;\n  color: #989AA6;\n  font-weight: normal;\n}\n\n.time ion-icon {\n  color: #989AA6;\n  font-size: 25px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 8%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos de las tarjetas de los temas */\n\nion-slide ion-col {\n  padding: 0px;\n  margin-right: 20px;\n}\n\nion-slide ion-col ion-card {\n  width: 100%;\n  margin: 0px;\n  height: 160px;\n  --background: rgba(15, 101, 122, 0.2);\n  border-radius: 15px;\n  box-shadow: none;\n}\n\nion-slide ion-col ion-card ion-card-header {\n  display: flex;\n  justify-content: center;\n}\n\nion-slide ion-col ion-card ion-card-header .circle {\n  width: 51px;\n  height: 51px;\n  background: white;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nion-slide ion-col ion-card ion-card-header .circle ion-icon {\n  font-size: 30px;\n}\n\nion-slide ion-col ion-card ion-card-content {\n  padding: 0px;\n  color: black;\n}\n\nion-slide ion-col ion-card .amount {\n  font-style: normal;\n  font-size: 14px;\n}\n\n.green {\n  --background:#EAFCEE;\n}\n\n.blue {\n  --background:#B9EAFF;\n}\n\n.gray {\n  --background:rgba(15, 101, 122, 0.2);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGNvbnRhY3R1cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBQUo7O0FBRUU7RUFDRSxjQUFBO0FBQ0o7O0FBQ0Esd0JBQUE7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUVKOztBQUFJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFFUjs7QUFBSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFFUjs7QUFHQSxzQ0FBQTs7QUFDQTtFQUNJLHNCQUFBO0VBQ0EsZUFBQTtBQUFKOztBQUNJO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUNSOztBQUNJO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBQ1I7O0FBS0Esd0JBQUE7O0FBQ0E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQUZKOztBQUtRO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBSFo7O0FBSVk7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFGaEI7O0FBR2dCO0VBQ0ksY0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQURwQjs7QUFFb0I7RUFDSSxXQUFBO0FBQXhCOztBQUVvQjtFQUNJLFlBQUE7RUFDQSxlQUFBO0FBQXhCOztBQUtZO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDRSxZQUFBO0FBSGxCOztBQU9RO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsOEJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBTFo7O0FBYUEscUNBQUE7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUFWSjs7QUFXSTtFQUNJLFdBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFUUjs7QUFXSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBVFI7O0FBYUEsb0NBQUE7O0FBQ0E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQVZKOztBQVdJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBVFI7O0FBYUEseUNBQUE7O0FBRUk7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7QUFYTjs7QUFZTTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHFDQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQVZSOztBQVdRO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBVFo7O0FBV1k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQVRoQjs7QUFVZ0I7RUFDSSxlQUFBO0FBUnBCOztBQWVRO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFiWjs7QUFnQlE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUFkWjs7QUFzQkE7RUFDSSxvQkFBQTtBQW5CSjs7QUFzQkE7RUFDSSxvQkFBQTtBQW5CSjs7QUFzQkE7RUFDSSxvQ0FBQTtBQW5CSiIsImZpbGUiOiJjb250YWN0dXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy9mb25kbyBkZSBsYSBpbWFnZW4gZGVsIGNvbnRhaW5lclxyXG4uY29udGFpbmVye1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIucG5nXCIpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgfVxyXG4gIC5oM2F6dWx7XHJcbiAgICBjb2xvcjojMDE0ODk4O1xyXG59XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDo1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICAgIC5jb250YWluZXItaW1nLWhlYWRlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjg5JTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qIEVzdGlsb3MgZGVsIG1lbnNhamUgZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzM3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcbi8qIEVzdGlsb3MgZGVsIGJhbm5lciAgKi9cclxuLmJhbm5lcntcclxuICAgIHBhZGRpbmc6IDBweCA1JTtcclxuICAgIG1hcmdpbi10b3A6IDIlO1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OjE7XHJcbiAgICBpb24tY29se1xyXG5cclxuICAgICAgICAuY2FyZC1iYW5uZXJ7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDIxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICAgICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICAgICAgICAgIHotaW5kZXg6MTtcclxuICAgICAgICAgICAgaW9uLWNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIC5jb250YWluZXItbmFtZXtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzczNzM3MztcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogcmdiYSgwLDAsMCwxKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgICAgICBpb24tY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1JTtcclxuICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1iYW5uZXI6OmJlZm9yZXtcclxuICAgICAgICAgICAgY29udGVudDonJztcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOnJnYmEoMCwwLDAsMC42KTtcclxuICAgICAgICAgICAgcmlnaHQ6IDBweDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OjEwMCU7XHJcbiAgICAgICAgICAgIHotaW5kZXg6LTE7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBcclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGllbXBvIGRlIGxhIG5vdGljaWEgKi9cclxuLnRpbWV7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgY29sb3I6Izk4OUFBNiA7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIH1cclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGNvbG9yOiM5ODlBQTYgO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGl0dWxvIGRlIGxhIHBhZ2luYSAqL1xyXG4uY29udGFpbmVyLXRpdGxle1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgbWFyZ2luLXRvcDogOCU7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZSBsYXMgdGFyamV0YXMgZGUgbG9zIHRlbWFzICovXHJcbmlvbi1zbGlkZSB7XHJcbiAgICBpb24tY29sIHtcclxuICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICAgIGlvbi1jYXJkIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDE2MHB4O1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogcmdiYSgxNSwgMTAxLCAxMjIsIDAuMik7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGlvbi1jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblxyXG4gICAgICAgICAgICAuY2lyY2xle1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDUxcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgaW9uLWljb257XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpb24tY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgIGNvbG9yOmJsYWNrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmFtb3VudHtcclxuICAgICAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcblxyXG4uZ3JlZW57XHJcbiAgICAtLWJhY2tncm91bmQ6I0VBRkNFRTtcclxufVxyXG5cclxuLmJsdWV7XHJcbiAgICAtLWJhY2tncm91bmQ6I0I5RUFGRjtcclxufVxyXG5cclxuLmdyYXl7XHJcbiAgICAtLWJhY2tncm91bmQ6cmdiYSgxNSwgMTAxLCAxMjIsIDAuMik7XHJcbn0iXX0= */");

/***/ }),

/***/ "KOp0":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contactus/contactus.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <h3 class=\"h3azul\">ITCA-App</h3>\r\n    <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    <ion-menu-button slot=\"start\" menu=\"administrador\"></ion-menu-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"container\">\r\n    \r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Contáctanos</h1>\r\n    <p>Respondemos tus dudas</p>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"banner\">\r\n    <ion-col size=\"12\" >\r\n      <ion-slides [options]=\"bannerConfig\" >\r\n        <ion-slide  *ngFor=\"let item of lastFivenotification\" routerLink=\"/notice/{{item.id}}\">\r\n          <ion-card class=\"card-banner\" style=\"background-image:url('{{item.img}}');\"  >\r\n            <ion-card-header>\r\n              <div class=\"container-name\">\r\n                <ion-icon name=\"person-circle\"></ion-icon>\r\n                <p>ITCA-App</p>\r\n              </div>\r\n              <div class=\"time\">\r\n                <ion-icon name=\"time-outline\"></ion-icon>\r\n                <p>{{item.updateDate}}</p>\r\n              </div> \r\n            </ion-card-header>\r\n            <ion-card-content>\r\n              <h1>{{item.name}}</h1>\r\n              <p>{{substr(item.description)}}...</p>\r\n            </ion-card-content>\r\n          </ion-card>\r\n  \r\n        </ion-slide>\r\n      </ion-slides>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Departamentos</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <ion-slides [options]=\"categoryConfig\" >\r\n        <ion-slide class=\"slide\" *ngFor=\"let data of contacts; let i = index\"> \r\n          <ion-col>\r\n            <ion-card class=\"gray\"  (click)=\"ventana(data.hiper)\"> \r\n              <ion-card-header>\r\n                <div class=\"circle\">\r\n                  <ion-icon slot=\"start\" name=\"{{data.icon}}\"></ion-icon>\r\n                </div>\r\n          \r\n              </ion-card-header>\r\n              <ion-card-content>\r\n                <p>{{data.categoria}}</p>\r\n              </ion-card-content>\r\n            </ion-card>\r\n          </ion-col>\r\n        </ion-slide>  \r\n      </ion-slides>\r\n    </ion-col>\r\n\r\n\r\n  </ion-row>\r\n  </div>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "d/vQ":
/*!*********************************************!*\
  !*** ./src/app/contactus/contactus.page.ts ***!
  \*********************************************/
/*! exports provided: ContactusPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactusPage", function() { return ContactusPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_contactus_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./contactus.page.html */ "KOp0");
/* harmony import */ var _contactus_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contactus.page.scss */ "BY/O");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");





let ContactusPage = class ContactusPage {
    constructor(firestore) {
        this.firestore = firestore;
        /* Obejeto para configurar el slide */
        this.categoryConfig = {
            spaceBetween: 1,
            slidesPerView: 2.5,
        };
        /* Obejeto para configurar el slide de los banners   */
        this.bannerConfig = {
            spaceBetween: 10,
            slidesPerView: 1,
            autoplay: true,
        };
    }
    ngOnInit() {
        this.contactsQuery();
        this.lastFiveNewsQuery();
    }
    ventana(valor) {
        window.open(valor, '_system');
    }
    substr(text) {
        let textResult;
        textResult = text.substring(0, 100);
        return textResult;
    }
    /* Metodo para consultar los contactos */
    contactsQuery() {
        this.firestore
            .collection("/contacts", (ref) => ref.where("state", "==", "public")).snapshotChanges().subscribe(res => {
            if (res) {
                this.contacts = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        categoria: e.payload.doc.data()["categoria"],
                        hiper: e.payload.doc.data()["hiper"],
                        icon: e.payload.doc.data()["icon"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    /* Metodo para consultar las ultimas 5 noticias publicadas */
    lastFiveNewsQuery() {
        const notificationRef = this.firestore.collection('/notification/', (ref) => ref.orderBy("updateDate", "desc").limit(5));
        notificationRef
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.lastFivenotification = res.map((e) => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        description: e.payload.doc.data()['description'],
                        creationDate: e.payload.doc.data()['creationDate'],
                        updateDate: this.tiempo(e.payload.doc.data()["updateDate"]),
                        img: e.payload.doc.data()['img'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    tiempo(time) {
        //  console.log(Date.now() + "");
        let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
        this.publicado = '';
        this.trasformar = 0;
        //Segundos
        if (tiempo > 0 && tiempo < 60) {
            this.trasformar = tiempo;
            this.publicado = this.trasformar + " Seg";
        }
        //minustos 
        else if (tiempo > 60 && tiempo < 3600) {
            this.trasformar = (tiempo / 60).toFixed(0);
            this.publicado = this.trasformar + " Min";
        }
        //horas 
        else if (tiempo > 3600 && tiempo < 86400) {
            this.trasformar = (tiempo / 3600).toFixed(0);
            this.publicado = this.trasformar + " Horas";
        }
        //Dias 
        else if (tiempo > 86400 && tiempo < 604800) {
            this.trasformar = (tiempo / 86400).toFixed(0);
            this.publicado = this.trasformar + " Dias";
        }
        else if (tiempo > 604800 && tiempo < 2629743) {
            this.trasformar = (tiempo / 604800).toFixed(0);
            this.publicado = this.trasformar + " Semanas";
        }
        return this.publicado;
    }
};
ContactusPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] }
];
ContactusPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contactus',
        template: _raw_loader_contactus_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contactus_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ContactusPage);



/***/ }),

/***/ "vRFT":
/*!*******************************************************!*\
  !*** ./src/app/contactus/contactus-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: ContactusPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactusPageRoutingModule", function() { return ContactusPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _contactus_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contactus.page */ "d/vQ");




const routes = [
    {
        path: '',
        component: _contactus_page__WEBPACK_IMPORTED_MODULE_3__["ContactusPage"]
    }
];
let ContactusPageRoutingModule = class ContactusPageRoutingModule {
};
ContactusPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactusPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=contactus-contactus-module.js.map