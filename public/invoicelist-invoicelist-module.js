(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["invoicelist-invoicelist-module"],{

/***/ "0XQT":
/*!***************************************************!*\
  !*** ./src/app/invoicelist/invoicelist.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGludm9pY2VsaXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLFlBQUE7RUFDQSx3Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFBSjs7QUFFQTtFQUNNLGNBQUE7QUFDTjs7QUFDQSx3QkFBQTs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBRUo7O0FBQUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUVSOztBQUFJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUVSIiwiZmlsZSI6Imludm9pY2VsaXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vZm9uZG8gZGUgbGEgaW1hZ2VuIGRlbCBjb250YWluZXJcclxuLmNvbnRhaW5lcntcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy8uLi9hc3NldHMvaW1nL2JhY2tncm91bmQyLnBuZ1wiKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIH1cclxuLmgzYXp1bHtcclxuICAgICAgY29sb3I6IzAxNDg5ODtcclxuICB9XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDo1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICAgIC5jb250YWluZXItaW1nLWhlYWRlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjg5JTtcclxuICAgIH1cclxufVxyXG5cclxuIl19 */");

/***/ }),

/***/ "27gS":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/invoicelist/invoicelist.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img\r\n        slot=\"end\"\r\n        class=\"img-logo ion-text-center\"\r\n        src=\"../../assets/img/logo.png\"\r\n        alt=\"\"\r\n      />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-title color=\"tertiary\"><strong>Mis Pagos</strong></ion-title>\r\n  <ion-card class=\"card-invoice\" *ngFor=\"let invoice of invoices\">\r\n    <ion-card-header (click)=\"goToSeeVoucher(invoice)\" >\r\n      <ion-card-title>{{invoice.identifynumber}}</ion-card-title>\r\n      <ion-card-subtitle>{{invoice.updateAt.toDate() | date:'dd/MM/yyyy' }}</ion-card-subtitle>\r\n    </ion-card-header>\r\n    <ion-card-content (click)=\"goToSeeVoucher(invoice)\">\r\n     <p><strong>Banco: </strong>{{invoice.bank}}</p>\r\n     <p><strong>Precio:</strong> ${{invoice.value}} </p>\r\n     <p><strong>Razón del pago:  </strong>{{invoice.reason}}</p>\r\n     <p><strong>Detalle del pago:  </strong>{{invoice.detail}}</p>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "3mc2":
/*!*******************************************!*\
  !*** ./src/app/services/banks.service.ts ***!
  \*******************************************/
/*! exports provided: BanksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksService", function() { return BanksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let BanksService = class BanksService {
    constructor(firebase) {
        this.firebase = firebase;
    }
    addBank(bank) {
        bank.createdAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').add(bank);
    }
    updateBank(bank) {
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').doc(bank.uid).update(bank);
    }
    deleteBank(uid) {
        this.firebase.doc(`/bank/${uid}`).delete();
    }
    getBankByUid(uid) {
        const ref = this.firebase
            .collection('/bank/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getBanks() {
        const ref = this.firebase
            .collection('bank')
            .valueChanges({ idField: 'uid' });
        return ref;
    }
};
BanksService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
BanksService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BanksService);



/***/ }),

/***/ "b8cs":
/*!***********************************************!*\
  !*** ./src/app/services/upinvoice.service.ts ***!
  \***********************************************/
/*! exports provided: UpinvoiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpinvoiceService", function() { return UpinvoiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let UpinvoiceService = class UpinvoiceService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    addInvoice(invoice) {
        invoice.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        invoice.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        invoice.date = invoice.createAt.toDateString();
        this.firestore.collection('/invoice').add(invoice);
    }
    updateInvoice(invoice) {
        invoice.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('/invoice').doc(invoice.uid).update(invoice);
    }
    deleteInvoice(uid) {
        this.firestore.doc(`/invoice/${uid}`).delete();
    }
    getInvoiceByUser(uid) {
        const ref = this.firestore
            .collection('/invoice/', (ref) => ref.where('useruid', '==', uid))
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getInvoiceByUid(uid) {
        const ref = this.firestore
            .collection('/invoice/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getInvoicesPendding() {
        return this.firestore
            .collection('/invoice', (ref) => ref.where('resolution', '==', 'registered'))
            .valueChanges({ idField: 'uid' });
    }
    getAllInvoices() {
        return this.firestore
            .collection('invoice')
            .valueChanges({ idField: 'uid' });
    }
};
UpinvoiceService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
UpinvoiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UpinvoiceService);



/***/ }),

/***/ "gyCI":
/*!***********************************************************!*\
  !*** ./src/app/invoicelist/invoicelist-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: InvoicelistPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicelistPageRoutingModule", function() { return InvoicelistPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _invoicelist_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./invoicelist.page */ "nrWC");




const routes = [
    {
        path: '',
        component: _invoicelist_page__WEBPACK_IMPORTED_MODULE_3__["InvoicelistPage"]
    }
];
let InvoicelistPageRoutingModule = class InvoicelistPageRoutingModule {
};
InvoicelistPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InvoicelistPageRoutingModule);



/***/ }),

/***/ "mNla":
/*!***************************************************!*\
  !*** ./src/app/invoicelist/invoicelist.module.ts ***!
  \***************************************************/
/*! exports provided: InvoicelistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicelistPageModule", function() { return InvoicelistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _invoicelist_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./invoicelist-routing.module */ "gyCI");
/* harmony import */ var _invoicelist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./invoicelist.page */ "nrWC");







let InvoicelistPageModule = class InvoicelistPageModule {
};
InvoicelistPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _invoicelist_routing_module__WEBPACK_IMPORTED_MODULE_5__["InvoicelistPageRoutingModule"]
        ],
        declarations: [_invoicelist_page__WEBPACK_IMPORTED_MODULE_6__["InvoicelistPage"]]
    })
], InvoicelistPageModule);



/***/ }),

/***/ "nrWC":
/*!*************************************************!*\
  !*** ./src/app/invoicelist/invoicelist.page.ts ***!
  \*************************************************/
/*! exports provided: InvoicelistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicelistPage", function() { return InvoicelistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_invoicelist_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./invoicelist.page.html */ "27gS");
/* harmony import */ var _invoicelist_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./invoicelist.page.scss */ "0XQT");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/upinvoice.service */ "b8cs");
/* harmony import */ var _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/firebase-auth-service.service */ "1x2Z");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_banks_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/banks.service */ "3mc2");








let InvoicelistPage = class InvoicelistPage {
    constructor(upinvoiceService, authService, router, bankService) {
        this.upinvoiceService = upinvoiceService;
        this.authService = authService;
        this.router = router;
        this.bankService = bankService;
    }
    ngOnInit() {
        this.getUpInvoiceService();
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getCurrentUser();
            return userSession.uid;
        });
    }
    getUpInvoiceService() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userUid = yield this.getUserId();
            console.log(userUid);
            this.upinvoiceService.getInvoiceByUser(userUid).subscribe(res => {
                this.invoices = res;
                this.invoices = this.invoices.map(c => (this.getbankById(c)));
            });
        });
    }
    goToSeeVoucher(invoice) {
        this.router.navigate(['see-detail-invoice/', invoice.uid]);
    }
    getbankById(invoice) {
        this.bankService.getBankByUid(invoice.bank).subscribe(res => {
            invoice.bankData = res;
        });
        return invoice;
    }
};
InvoicelistPage.ctorParameters = () => [
    { type: _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_4__["UpinvoiceService"] },
    { type: _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseAuthServiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _services_banks_service__WEBPACK_IMPORTED_MODULE_7__["BanksService"] }
];
InvoicelistPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-invoicelist',
        template: _raw_loader_invoicelist_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_invoicelist_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], InvoicelistPage);



/***/ })

}]);
//# sourceMappingURL=invoicelist-invoicelist-module.js.map