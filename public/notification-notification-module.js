(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notification-notification-module"],{

/***/ "LcRc":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notification/notification.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
<<<<<<< HEAD
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Bienvenido</h1>\r\n    <p>Administrador</p>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Nueva notificación</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-icon\" style=\"background-image: url({{notification.img}});\">\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"file\" accept=\"image/*\" id=\"file-input\" #fileInp (change)=\"fileChange($event)\">\r\n          </ion-input>\r\n          <button class=\"btn-subir\">\r\n            <ion-icon name=\"arrow-up-outline\"></ion-icon>\r\n          </button>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-card\">\r\n    <ion-card class=\"blue\">\r\n      <ion-card-content class=\"form\">\r\n        <p>Nombre</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"notification.name\"></ion-input>\r\n        </ion-item>\r\n        <p>Descripción</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-textarea [(ngModel)]=\"notification.description\"></ion-textarea>\r\n        </ion-item>\r\n\r\n        <p>Enlace</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"notification.hyper\"></ion-input>\r\n        </ion-item>\r\n        \r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Orden\" [(ngModel)]=\"notification.order\">\r\n            <div *ngFor=\"let order of Orders; let i = index\">\r\n              <ion-select-option value=\"{{order.value}}\">{{order.value}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Categoría\" [(ngModel)]=\"notification.category\">\r\n            <div *ngFor=\"let category  of Categories; let i = index\">\r\n              <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Prioridad en Panel\" [(ngModel)]=\"notification.priority\">\r\n            <div *ngFor=\"let priority of Priorities; let i = index\">\r\n              <ion-select-option value=\"{{priority.name}}\">{{priority.name}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Estado\" [(ngModel)]=\"notification.state\">\r\n            <ion-select-option value=\"public\">Público</ion-select-option>\r\n            <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-card-content>\r\n\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn margindown\">\r\n    <ion-button (click)=\"addNotification()\">\r\n      Registrar\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n\r\n</ion-content>");
=======
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Bienvenido</h1>\r\n    <p>Administrador</p>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Nueva Noticia</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-icon\" style=\"background-image: url({{notification.img}});\">\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"file\" accept=\"image/*\" id=\"file-input\" #fileInp (change)=\"fileChange($event)\">\r\n          </ion-input>\r\n          <button class=\"btn-subir\">\r\n            <ion-icon name=\"arrow-up-outline\"></ion-icon>\r\n          </button>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-card\">\r\n    <ion-card class=\"blue\">\r\n      <ion-card-content class=\"form\">\r\n        <p>Nombre</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"notification.name\"></ion-input>\r\n        </ion-item>\r\n        <p>Descripción</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-textarea [(ngModel)]=\"notification.description\"></ion-textarea>\r\n        </ion-item>\r\n\r\n        <p>Enlace</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"notification.hyper\"></ion-input>\r\n        </ion-item>\r\n        \r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Orden\" [(ngModel)]=\"notification.order\">\r\n            <div *ngFor=\"let order of Orders; let i = index\">\r\n              <ion-select-option value=\"{{order.value}}\">{{order.value}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Categoría\" [(ngModel)]=\"notification.category\">\r\n            <div *ngFor=\"let category  of Categories; let i = index\">\r\n              <ion-select-option value=\"{{category.name}}\">{{category.name}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Prioridad en Panel\" [(ngModel)]=\"notification.priority\">\r\n            <div *ngFor=\"let priority of Priorities; let i = index\">\r\n              <ion-select-option value=\"{{priority.name}}\">{{priority.name}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Estado\" [(ngModel)]=\"notification.state\">\r\n            <ion-select-option value=\"public\">Publico</ion-select-option>\r\n            <ion-select-option value=\"hide\">Oculto</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-card-content>\r\n\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn margindown\">\r\n    <ion-button (click)=\"addNotification()\">\r\n      Registrar\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n\r\n</ion-content>");
>>>>>>> d211c7cc55355ea0c6e3442d1daad6edb732a978

/***/ }),

/***/ "TLzw":
/*!*****************************************************!*\
  !*** ./src/app/notification/notification.module.ts ***!
  \*****************************************************/
/*! exports provided: NotificationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function() { return NotificationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notification-routing.module */ "mhdW");
/* harmony import */ var _notification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notification.page */ "dOoZ");







let NotificationPageModule = class NotificationPageModule {
};
NotificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationPageRoutingModule"]
        ],
        declarations: [_notification_page__WEBPACK_IMPORTED_MODULE_6__["NotificationPage"]]
    })
], NotificationPageModule);



/***/ }),

/***/ "Z9r7":
/*!*****************************************************!*\
  !*** ./src/app/notification/notification.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n.btn-subir {\n  background-color: #94c9d4;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos del icono de subir categoria */\n\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n  border: 2px solid #979797;\n  width: 100%;\n  height: 174px;\n  border-radius: 21px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.container-icon ion-item {\n  --background:rgba(0,0,0,0);\n}\n\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n#file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n\n/* Estilos de la tarjeta de los inputs */\n\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n\n.container-card ion-card {\n  width: 90%;\n  box-shadow: none;\n  border-radius: 15px;\n}\n\n.container-card ion-card ion-card-content {\n  padding: 20px;\n  padding-top: 10px;\n}\n\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n\n.container-card ion-card ion-card-content .categories {\n  margin-top: 5%;\n}\n\n.container-card ion-card ion-card-content .categories ion-select {\n  --placeholder-opacity:1;\n}\n\n.select-black {\n  --background: #161616;\n  color: white;\n}\n\n.container-link {\n  padding: 0px 6%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.container-link p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n.container-link .circle {\n  width: 40px;\n  height: 40px;\n  background: #333333;\n  color: white;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.blue {\n  background-color: #DDF2FB;\n}\n\n.green {\n  background-color: #E8F8E5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXG5vdGlmaWNhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBQUo7O0FBRUE7RUFDTSxjQUFBO0FBQ047O0FBQ0Esd0JBQUE7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUVKOztBQUFJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFFUjs7QUFBSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFFUjs7QUFFQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUEsc0NBQUE7O0FBQ0E7RUFDSSxzQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFBSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFFUjs7QUFBSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUVBLG9DQUFBOztBQUNBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFBSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUdBLHlDQUFBOztBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFBSjs7QUFDSTtFQUNJLDBCQUFBO0FBQ1I7O0FBQ0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUNSOztBQUlBO0VBQ0ksVUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsT0FBQTtFQUNBLFlBQUE7QUFESjs7QUFNQSx3Q0FBQTs7QUFDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQUhKOztBQUlJO0VBQ0ksVUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFGUjs7QUFHUTtFQUNJLGFBQUE7RUFDQSxpQkFBQTtBQURaOztBQUVZO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0FBQWhCOztBQUVjO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FBQWxCOztBQUVjO0VBQ0Usb0JBQUE7QUFBaEI7O0FBSWM7RUFDSSxjQUFBO0FBRmxCOztBQUdrQjtFQUNFLHVCQUFBO0FBRHBCOztBQVNBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBTko7O0FBU0E7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBTko7O0FBT0k7RUFDSSxpQkFBQTtFQUNBLGVBQUE7QUFMUjs7QUFPSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUxSOztBQVdBO0VBQ0kseUJBQUE7QUFSSjs7QUFXQTtFQUNJLHlCQUFBO0FBUkoiLCJmaWxlIjoibm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vZm9uZG8gZGUgbGEgaW1hZ2VuIGRlbCBjb250YWluZXJcclxuLmNvbnRhaW5lcntcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy8uLi9hc3NldHMvaW1nL2JhY2tncm91bmQyLnBuZ1wiKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIH1cclxuLmgzYXp1bHtcclxuICAgICAgY29sb3I6IzAxNDg5ODtcclxuICB9XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDo1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICAgIC5jb250YWluZXItaW1nLWhlYWRlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjg5JTtcclxuICAgIH1cclxufVxyXG4vL0VzdGlsb3MgZGUgYm90b24gc3ViaXIgaW1nXHJcbi5idG4tc3ViaXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiM5NGM5ZDQ7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogNDVweDtcclxuICAgIGhlaWdodDogNDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiA1JTtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIH1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIG1lbnNhamUgZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzM3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCB0aXR1bG8gZGUgbGEgcGFnaW5hICovXHJcbi5jb250YWluZXItdGl0bGV7XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qIEVzdGlsb3MgZGVsIGljb25vIGRlIHN1YmlyIGNhdGVnb3JpYSAqL1xyXG4uY29udGFpbmVyLWljb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkICM5Nzk3OTc7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTc0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBpb24taXRlbXtcclxuICAgICAgICAtLWJhY2tncm91bmQ6cmdiYSgwLDAsMCwwKTtcclxuICAgIH1cclxuICAgIC5jaXJjbGV7XHJcbiAgICAgICAgd2lkdGg6IDUxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNDQkQ4REM7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgIFxyXG59XHJcblxyXG4jZmlsZS1pbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgei1pbmRleDogOTk5O1xyXG4gIH1cclxuXHJcblxyXG5cclxuLyogRXN0aWxvcyBkZSBsYSB0YXJqZXRhIGRlIGxvcyBpbnB1dHMgKi9cclxuLmNvbnRhaW5lci1jYXJke1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaW9uLWNhcmR7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGlvbi1zZWxlY3Qge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaW9uLWl0ZW17XHJcbiAgICAgICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6MTBweDtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAuY2F0ZWdvcmllc3tcclxuICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgICAgICAgICAgIGlvbi1zZWxlY3R7XHJcbiAgICAgICAgICAgICAgICAgICAgLS1wbGFjZWhvbGRlci1vcGFjaXR5OjE7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnNlbGVjdC1ibGFja3tcclxuICAgIC0tYmFja2dyb3VuZDogIzE2MTYxNjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmNvbnRhaW5lci1saW5re1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgfVxyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzMzMzMzMztcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4uYmx1ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6I0RERjJGQjsgXHJcbn1cclxuXHJcbi5ncmVlbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6I0U4RjhFNTsgXHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG4iXX0= */");

/***/ }),

/***/ "dOoZ":
/*!***************************************************!*\
  !*** ./src/app/notification/notification.page.ts ***!
  \***************************************************/
/*! exports provided: NotificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPage", function() { return NotificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_notification_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./notification.page.html */ "LcRc");
/* harmony import */ var _notification_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notification.page.scss */ "Z9r7");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/app */ "Jgta");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _services_push_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/push.service */ "H+l1");








let NotificationPage = class NotificationPage {
    constructor(pushService, toastController, loadingController, firestore) {
        this.pushService = pushService;
        this.toastController = toastController;
        this.loadingController = loadingController;
        this.firestore = firestore;
        this.validationRules = {
            required: [
                'name',
                'description',
                'order',
                'img',
                'priority',
                'category'
            ],
        };
        this.any = {
            name: null,
            description: null,
            order: null,
            img: null,
            priority: null,
            category: null
        };
        this.file = null;
        this.notification = {
            uid: '',
            name: '',
            description: '',
            state: '',
            creationDate: null,
            hyper: '',
            priority: '',
            updateDate: null,
            category: '',
            img: '',
            order: ''
        };
    }
    ngOnInit() {
        this.infocategory();
        this.infoOrder();
        this.infoPriority();
    }
    /* Método para cambiar la imagen de la notificación temporalemnte, recibe un evento como parametro */
    fileChange($event) {
        this.file = $event.target.files[0];
        if ($event.target.files && $event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event) => {
                this.notification.img = event.target.result;
            };
            reader.readAsDataURL($event.target.files[0]);
        }
    }
    infocategory() {
        this.firestore.collection("/category", (ref) => ref.where("state", "==", "public")).snapshotChanges().subscribe(res => {
            if (res) {
                this.Categories = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()["name"],
                        description: e.payload.doc.data()["description"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    infoOrder() {
        this.firestore.collection("/order", (ref) => ref.where("state", "==", "public").orderBy('value', 'asc')).snapshotChanges().subscribe(res => {
            if (res) {
                this.Orders = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        value: e.payload.doc.data()["value"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    infoPriority() {
        this.firestore.collection("/priority", (ref) => ref.where("state", "==", "public").orderBy("name", "asc")).snapshotChanges().subscribe(res => {
            if (res) {
                this.Priorities = res.map(e => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()["name"],
                        state: e.payload.doc.data()["state"]
                    };
                });
            }
        });
    }
    addNotification() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create();
            yield loading.present();
            if (this.file == null) {
                this.mensaje('Por favor ingrese la imgen');
                console.log("no hay" + this.file);
                loading.dismiss();
            }
            else {
                console.log(this.file);
                console.log("uploadProfileImage");
                const uploadTask = yield firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].storage().ref('notification/' + this.file.name);
                uploadTask.put(this.file).then(snapshot => {
                    return uploadTask.getDownloadURL().then(url => {
                        loading.dismiss();
                        console.log(url);
                        this.updatein(url);
                    });
                }).catch(error => {
                    loading.dismiss();
                    console.log(error);
                });
            }
        });
    }
    updatein(valor) {
        if (this.notification.name !== ''
            && this.notification.description !== ''
            && this.notification.category !== ''
            && this.notification.order !== ''
            && this.notification.state !== '') {
            let addNotification = {};
            addNotification['name'] = this.notification.name;
            addNotification['description'] = this.notification.description;
            addNotification['state'] = this.notification.state;
            addNotification['order'] = this.notification.order;
            addNotification['hyper'] = this.notification.hyper;
            addNotification['priority'] = this.notification.priority;
            addNotification['creationDate'] = firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].firestore.Timestamp.now();
            addNotification['updateDate'] = firebase_app__WEBPACK_IMPORTED_MODULE_5__["default"].firestore.Timestamp.now();
            addNotification['category'] = this.notification.category;
            addNotification['img'] = valor;
            this.firestore.collection('/notification/').add(addNotification).then((ids) => {
                this.pushService.onesignal(this.notification.name, this.notification.name, this.notification.description, ids.id + "");
                this.notification = {
                    name: '',
                    description: '',
                    state: '',
                    creationDate: null,
                    updateDate: null,
                    category: '',
                    hyper: '',
                    priority: '',
                    img: '',
                    order: ''
                };
                this.mensaje('Notificación enviada');
            });
        }
        else {
            this.mensaje('Por favor ingrese todos los datos');
        }
    }
    mensaje(parametro) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: parametro,
                duration: 2000
            });
            toast.present();
        });
    }
};
NotificationPage.ctorParameters = () => [
    { type: _services_push_service__WEBPACK_IMPORTED_MODULE_7__["PushService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"] }
];
NotificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-notification',
        template: _raw_loader_notification_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_notification_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NotificationPage);



/***/ }),

/***/ "mhdW":
/*!*************************************************************!*\
  !*** ./src/app/notification/notification-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: NotificationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageRoutingModule", function() { return NotificationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _notification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification.page */ "dOoZ");




const routes = [
    {
        path: '',
        component: _notification_page__WEBPACK_IMPORTED_MODULE_3__["NotificationPage"]
    }
];
let NotificationPageRoutingModule = class NotificationPageRoutingModule {
};
NotificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NotificationPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=notification-notification-module.js.map