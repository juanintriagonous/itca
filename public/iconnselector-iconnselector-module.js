(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["iconnselector-iconnselector-module"],{

/***/ "0CSV":
/*!***************************************************************!*\
  !*** ./src/app/iconnselector/iconnselector-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: IconnselectorPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconnselectorPageRoutingModule", function() { return IconnselectorPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _iconnselector_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./iconnselector.page */ "hJu9");




const routes = [
    {
        path: '',
        component: _iconnselector_page__WEBPACK_IMPORTED_MODULE_3__["IconnselectorPage"]
    }
];
let IconnselectorPageRoutingModule = class IconnselectorPageRoutingModule {
};
IconnselectorPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], IconnselectorPageRoutingModule);



/***/ }),

/***/ "BdKf":
/*!*******************************************************!*\
  !*** ./src/app/iconnselector/iconnselector.module.ts ***!
  \*******************************************************/
/*! exports provided: IconnselectorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconnselectorPageModule", function() { return IconnselectorPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _iconnselector_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./iconnselector-routing.module */ "0CSV");
/* harmony import */ var _iconnselector_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./iconnselector.page */ "hJu9");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "6NWb");








let IconnselectorPageModule = class IconnselectorPageModule {
};
IconnselectorPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _iconnselector_routing_module__WEBPACK_IMPORTED_MODULE_5__["IconnselectorPageRoutingModule"],
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeModule"]
        ],
        declarations: [_iconnselector_page__WEBPACK_IMPORTED_MODULE_6__["IconnselectorPage"]]
    })
], IconnselectorPageModule);



/***/ }),

/***/ "hJu9":
/*!*****************************************************!*\
  !*** ./src/app/iconnselector/iconnselector.page.ts ***!
  \*****************************************************/
/*! exports provided: IconnselectorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconnselectorPage", function() { return IconnselectorPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_iconnselector_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./iconnselector.page.html */ "wyQ2");
/* harmony import */ var _iconnselector_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./iconnselector.page.scss */ "qgQB");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "wHSu");






let IconnselectorPage = class IconnselectorPage {
    constructor(modalController) {
        this.modalController = modalController;
        this.selectedIcon = '';
        /**
        * Lista completa de íconos.
        */
        this.awesome = [];
        this.auxAwesomeIcons = [];
        this.loadingIcons = true;
        this.skeletonNumber = [0, 1, 2, 3, 4];
        this.icon = "";
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.loadIcons();
        this.selectedIcon = this.icon;
    }
    loadIcons() {
        for (const iconName in _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__) {
            if (iconName.substring(0, 2) === 'fa' && iconName !== 'fas') {
                const icon = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__[iconName];
                icon.iconName && this.awesome.push(icon);
            }
        }
        this.auxAwesomeIcons = [...this.awesome];
        this.loadingIcons = false;
    }
    /* Metodo para cerrar el modal */
    closeModal() {
        this.modalController.dismiss({
            option: ""
        });
    }
    /* Metodo que envia el dato a la pagina principal */
    sendData() {
        this.modalController.dismiss({
            'option': this.icon || "",
        });
    }
    searchIcons({ detail }) {
        const searchText = detail.value.toLowerCase();
        this.awesome = this.auxAwesomeIcons.filter((s) => s.iconName.toLowerCase().includes(searchText));
    }
    handleSelectedIcon({ detail }) {
        this.icon = detail.value;
    }
};
IconnselectorPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
IconnselectorPage.propDecorators = {
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
IconnselectorPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-iconnselector',
        template: _raw_loader_iconnselector_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_iconnselector_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], IconnselectorPage);



/***/ }),

/***/ "qgQB":
/*!*******************************************************!*\
  !*** ./src/app/iconnselector/iconnselector.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --padding-start:25px;\n  --padding-end:25px;\n}\nion-toolbar ion-icon {\n  font-size: 25px;\n}\n/* Estilos del contenedor del boton para agregar imagen */\n.container-btn {\n  display: flex;\n  justify-content: center;\n}\n.container-btn ion-button {\n  height: 60px;\n  width: 80%;\n  --background:#111111;\n  --background-activated:#111111;\n  --border-radius: 15px;\n  --box-shadow:0px 4px 20px rgba(255, 255, 255, 0.25);\n  font-size: 18px;\n  font-weight: bold;\n  color: white;\n}\n.search-button {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n.icon-number {\n  font-size: 11px;\n  display: block;\n  text-align: center;\n  margin-bottom: 10px;\n  color: #b1bbb3;\n}\n.icon-color-awesome {\n  color: rgba(var(--ion-text-color-rgb, 0, 0, 0), 0.54);\n  stroke: rgba(var(--ion-text-color-rgb, 0, 0, 0), 0.54);\n}\n/* Estilo del boton de registrarse */\n.btn {\n  --background: #111111;\n  --box-shadow: 0px 3px 3px rgba($color:#000000, $alpha: 0.3);\n  --border-radiuss: 10px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGljb25uc2VsZWN0b3IucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0JBQUE7RUFDQSxrQkFBQTtBQUNKO0FBQUk7RUFDSSxlQUFBO0FBRVI7QUFFQSx5REFBQTtBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBQ0o7QUFBSTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSw4QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbURBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FBRVI7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUFDSjtBQUVBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQUNKO0FBRUE7RUFDSSxxREFBQTtFQUNBLHNEQUFBO0FBQ0o7QUFFQSxvQ0FBQTtBQUNBO0VBQ0kscUJBQUE7RUFDQSwyREFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtBQUNKIiwiZmlsZSI6Imljb25uc2VsZWN0b3IucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6MjVweDtcclxuICAgIC0tcGFkZGluZy1lbmQ6MjVweDtcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweFxyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBjb250ZW5lZG9yIGRlbCBib3RvbiBwYXJhIGFncmVnYXIgaW1hZ2VuICovXHJcbi5jb250YWluZXItYnRue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaW9uLWJ1dHRvbntcclxuICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgd2lkdGg6ODAlO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDojMTExMTExO1xyXG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IzExMTExMTtcclxuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgLS1ib3gtc2hhZG93OjBweCA0cHggMjBweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMjUpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgIH1cclxufVxyXG5cclxuLnNlYXJjaC1idXR0b257XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG5cclxuLmljb24tbnVtYmVye1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgY29sb3I6ICNiMWJiYjM7XHJcbn1cclxuXHJcbi5pY29uLWNvbG9yLWF3ZXNvbWV7XHJcbiAgICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IsIDAsIDAsIDApLCAwLjU0KTtcclxuICAgIHN0cm9rZTogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IsIDAsIDAsIDApLCAwLjU0KTtcclxufVxyXG5cclxuLyogRXN0aWxvIGRlbCBib3RvbiBkZSByZWdpc3RyYXJzZSAqL1xyXG4uYnRuIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzExMTExMTtcclxuICAgIC0tYm94LXNoYWRvdzogMHB4IDNweCAzcHggcmdiYSgkY29sb3I6IzAwMDAwMCwgJGFscGhhOiAwLjMpO1xyXG4gICAgLS1ib3JkZXItcmFkaXVzczogMTBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59Il19 */");

/***/ }),

/***/ "wyQ2":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/iconnselector/iconnselector.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-icon name=\"close-circle-outline\"\r\n      (click)=\"closeModal()\"\r\n      slot=\"end\"></ion-icon>\r\n    <ion-searchbar (ionChange)=\"searchIcons($event)\"\r\n      placeholder=\"Buscar ícono\"\r\n      mode=\"ios\"\r\n      color=\"light\"\r\n      class=\"search-button\"\r\n      debounce=\"500\"></ion-searchbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-list *ngIf=\"loadingIcons\">\r\n    <ion-item *ngFor=\"let i of skeletonNumber\">\r\n      <ion-avatar slot=\"start\">\r\n        <ion-skeleton-text animated></ion-skeleton-text>\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <p>\r\n          <ion-skeleton-text animated\r\n            style=\"width: 100%; height: 32px;\"></ion-skeleton-text>\r\n        </p>\r\n      </ion-label>\r\n    </ion-item>\r\n  </ion-list>\r\n  <ion-fab vertical=\"bottom\"\r\n    horizontal=\"end\"\r\n    slot=\"fixed\">\r\n    <ion-button (click)=\"sendData()\"\r\n      shape=\"round\"\r\n      class=\"btn\">\r\n      Siguiente\r\n      <ion-icon slot=\"end\"\r\n        name=\"chevron-forward-outline\"></ion-icon>\r\n    </ion-button>\r\n  </ion-fab>\r\n  <ion-radio-group [(ngModel)]=\"selectedIcon\"\r\n    *ngIf=\"!loadingIcons\"\r\n    (ionChange)=\"handleSelectedIcon($event)\">\r\n    <ion-item *ngFor=\"let item of awesome\">\r\n      <fa-icon icon=\"{{item.iconName}}\"\r\n        size=\"lg\"\r\n        [fixedWidth]=\"true\"\r\n        pull=\"right\"\r\n        class=\"icon-color-awesome\"></fa-icon>\r\n      <ion-radio slot=\"start\"\r\n        value=\"{{item.iconName}}\"></ion-radio>\r\n    </ion-item>\r\n  </ion-radio-group>\r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=iconnselector-iconnselector-module.js.map