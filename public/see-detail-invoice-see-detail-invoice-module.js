(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["see-detail-invoice-see-detail-invoice-module"],{

/***/ "3mc2":
/*!*******************************************!*\
  !*** ./src/app/services/banks.service.ts ***!
  \*******************************************/
/*! exports provided: BanksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksService", function() { return BanksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let BanksService = class BanksService {
    constructor(firebase) {
        this.firebase = firebase;
    }
    addBank(bank) {
        bank.createdAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').add(bank);
    }
    updateBank(bank) {
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').doc(bank.uid).update(bank);
    }
    deleteBank(uid) {
        this.firebase.doc(`/bank/${uid}`).delete();
    }
    getBankByUid(uid) {
        const ref = this.firebase
            .collection('/bank/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getBanks() {
        const ref = this.firebase
            .collection('bank')
            .valueChanges({ idField: 'uid' });
        return ref;
    }
};
BanksService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
BanksService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BanksService);



/***/ }),

/***/ "JXvu":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/see-detail-invoice/see-detail-invoice.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
<<<<<<< HEAD
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section>\r\n    <ion-title color=\"success\"><strong>Detalle de Pago</strong></ion-title>\r\n    <ion-card class=\"image-card\">\r\n      <ion-slides [options]=\"sliderOpts\" zoom>\r\n        <ion-slide >\r\n          <div class=\"swiper-zoom-container\">\r\n            <img src=\"{{invoice?.img}}\" width=\"90%\" height=\"90%\">\r\n          </div>      \r\n        </ion-slide>\r\n      </ion-slides>  \r\n    </ion-card>\r\n  </section>\r\n\r\n  <ion-card class=\"card-invoice\">\r\n    \r\n    <ion-card-header>\r\n      <ion-card-title >Detalles del Comprobante</ion-card-title>\r\n      <ion-card-subtitle color=\"success\">{{invoice?.createAt.toDate() | date:'dd/MM/yyyy' }}</ion-card-subtitle>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <p><strong><ion-label color=\"tertiary\">Estudiante: </ion-label></strong>{{invoice?.studentName}}</p>\r\n      <p><strong><ion-label color=\"tertiary\">Banco: </ion-label></strong>{{invoice?.bank}}</p>\r\n      <p><strong><ion-label color=\"tertiary\">Valor depositado : </ion-label></strong> ${{invoice?.value}} </p>\r\n      <p><strong><ion-label color=\"tertiary\">Motivo de Pago: </ion-label></strong>{{invoice?.reason}}</p>\r\n      <p><strong><ion-label color=\"tertiary\">Detalle del pago: </ion-label></strong>{{invoice?.detail}}</p>\r\n        <ion-list *ngIf=\"invoice?.resolution == 'registered'\">\r\n          <ion-radio-group [(ngModel)]=\"resolutionAux\">\r\n            <ion-list-header>\r\n              <ion-label>Verificar estado del pago</ion-label>\r\n            </ion-list-header>        \r\n            <ion-item>\r\n              <ion-label>Aceptar Pago</ion-label>\r\n              <ion-radio slot=\"start\" value=\"accepted\"></ion-radio>\r\n            </ion-item>\r\n        \r\n            <ion-item>\r\n              <ion-label>Rechazar Pago</ion-label>\r\n              <ion-radio slot=\"start\" value=\"rejected\"></ion-radio>\r\n            </ion-item>\r\n        \r\n            <ion-item>\r\n              <ion-label>Pago Incompleto</ion-label>\r\n              <ion-radio slot=\"start\" value=\"incompleted\"></ion-radio>\r\n            </ion-item>\r\n          </ion-radio-group>\r\n        </ion-list>\r\n      <ion-item class=\"ion-text-center\">\r\n        <ion-button size=\"small\" color=\"success\" (click)=\"updateBank()\">Guardar Cambios</ion-button>\r\n      </ion-item>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");
=======
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section>\r\n    <ion-title color=\"success\"><strong>Detalle de Pago</strong></ion-title>\r\n    <ion-card class=\"image-card\">\r\n      <ion-slides [options]=\"sliderOpts\" zoom>\r\n        <ion-slide >\r\n          <div class=\"swiper-zoom-container\">\r\n            <img src=\"{{invoice?.img}}\" width=\"90%\" height=\"90%\">\r\n          </div>      \r\n        </ion-slide>\r\n      </ion-slides>  \r\n    </ion-card>\r\n  </section>\r\n\r\n  <ion-card class=\"card-invoice\">\r\n    \r\n    <ion-card-header>\r\n      <ion-card-title >Detalles del Comprobante</ion-card-title>\r\n      <ion-card-subtitle color=\"success\">{{invoice?.createAt.toDate() | date:'dd/MM/yyyy' }}</ion-card-subtitle>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <p><strong><ion-label color=\"tertiary\">Estudiante: </ion-label></strong>{{invoice?.studentName}}</p>\r\n      <p><strong><ion-label color=\"tertiary\">Banco: </ion-label></strong>{{invoice?.bank}}</p>\r\n      <p><strong><ion-label color=\"tertiary\">Valor depositado : </ion-label></strong> ${{invoice?.value}} </p>\r\n      <p><strong><ion-label color=\"tertiary\">Motivo de Pago: </ion-label></strong>{{invoice?.reason}}</p>\r\n      <p><strong><ion-label color=\"tertiary\">Detalle del pago: </ion-label></strong>{{invoice?.detail}}</p>\r\n        <ion-list *ngIf=\"invoice?.resolution == 'registered'\">\r\n          <ion-radio-group [(ngModel)]=\"resolutionAux\">\r\n            <ion-list-header>\r\n              <ion-label>Verificar estado del pago</ion-label>\r\n            </ion-list-header>        \r\n            <ion-item>\r\n              <ion-label>Aceptar Pago</ion-label>\r\n              <ion-radio slot=\"start\" value=\"accepted\"></ion-radio>\r\n            </ion-item>\r\n        \r\n            <ion-item>\r\n              <ion-label>Rechazar Pago</ion-label>\r\n              <ion-radio slot=\"start\" value=\"rejected\"></ion-radio>\r\n            </ion-item>\r\n        \r\n            <ion-item>\r\n              <ion-label>Pago Incompleto</ion-label>\r\n              <ion-radio slot=\"start\" value=\"incompleted\"></ion-radio>\r\n            </ion-item>\r\n          </ion-radio-group>\r\n        </ion-list>\r\n        <div *ngIf=\"user?.role=='admin'\">\r\n          <ion-item class=\"ion-text-center\" >\r\n            <ion-button size=\"small\" color=\"success\" (click)=\"updateBank()\">Guardar Cambios</ion-button>\r\n          </ion-item>\r\n        </div>      \r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");
>>>>>>> d211c7cc55355ea0c6e3442d1daad6edb732a978

/***/ }),

/***/ "b8cs":
/*!***********************************************!*\
  !*** ./src/app/services/upinvoice.service.ts ***!
  \***********************************************/
/*! exports provided: UpinvoiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpinvoiceService", function() { return UpinvoiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let UpinvoiceService = class UpinvoiceService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    addInvoice(invoice) {
        invoice.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        invoice.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        invoice.date = invoice.createAt.toDateString();
        this.firestore.collection('/invoice').add(invoice);
    }
    updateInvoice(invoice) {
        invoice.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('/invoice').doc(invoice.uid).update(invoice);
    }
    deleteInvoice(uid) {
        this.firestore.doc(`/invoice/${uid}`).delete();
    }
    getInvoiceByUser(uid) {
        const ref = this.firestore
            .collection('/invoice/', (ref) => ref.where('useruid', '==', uid))
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getInvoiceByUid(uid) {
        const ref = this.firestore
            .collection('/invoice/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getInvoicesPendding() {
        return this.firestore
            .collection('/invoice', (ref) => ref.where('resolution', '==', 'registered'))
            .valueChanges({ idField: 'uid' });
    }
    getAllInvoices() {
        return this.firestore
            .collection('invoice')
            .valueChanges({ idField: 'uid' });
    }
};
UpinvoiceService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
UpinvoiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UpinvoiceService);



/***/ }),

/***/ "dIrW":
/*!*************************************************************************!*\
  !*** ./src/app/see-detail-invoice/see-detail-invoice-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: SeeDetailInvoicePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeDetailInvoicePageRoutingModule", function() { return SeeDetailInvoicePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _see_detail_invoice_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./see-detail-invoice.page */ "p1PS");




const routes = [
    {
        path: '',
        component: _see_detail_invoice_page__WEBPACK_IMPORTED_MODULE_3__["SeeDetailInvoicePage"]
    }
];
let SeeDetailInvoicePageRoutingModule = class SeeDetailInvoicePageRoutingModule {
};
SeeDetailInvoicePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SeeDetailInvoicePageRoutingModule);



/***/ }),

/***/ "hK95":
/*!*****************************************************************!*\
  !*** ./src/app/see-detail-invoice/see-detail-invoice.module.ts ***!
  \*****************************************************************/
/*! exports provided: SeeDetailInvoicePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeDetailInvoicePageModule", function() { return SeeDetailInvoicePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _see_detail_invoice_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./see-detail-invoice-routing.module */ "dIrW");
/* harmony import */ var _see_detail_invoice_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./see-detail-invoice.page */ "p1PS");







let SeeDetailInvoicePageModule = class SeeDetailInvoicePageModule {
};
SeeDetailInvoicePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _see_detail_invoice_routing_module__WEBPACK_IMPORTED_MODULE_5__["SeeDetailInvoicePageRoutingModule"]
        ],
        declarations: [_see_detail_invoice_page__WEBPACK_IMPORTED_MODULE_6__["SeeDetailInvoicePage"]]
    })
], SeeDetailInvoicePageModule);



/***/ }),

/***/ "iBEw":
/*!*****************************************************************!*\
  !*** ./src/app/see-detail-invoice/see-detail-invoice.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start: 6%;\n  --padding-end: 6%;\n  --padding-top: 6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* img {\n  padding-left: 10%;\n  padding-right: 10%;\n  height: 50%;\n} */\n\n.preview-slides {\n  margin-top: 20%;\n  background: #e6e6e6;\n}\n\n.preview-slides img {\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n\n.image-slide,\n.image-card {\n  overflow: visible;\n}\n\n.image-card {\n  z-index: 9;\n}\n\n.backdrop {\n  height: 200%;\n  width: 100%;\n  background: black;\n  position: absolute;\n  z-index: 10;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHNlZS1kZXRhaWwtaW52b2ljZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBQUY7O0FBR0E7RUFDRSxjQUFBO0FBQUY7O0FBR0Esd0JBQUE7O0FBQ0E7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQUFGOztBQUVFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFBSjs7QUFHRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFESjs7QUFLQTs7OztHQUFBOztBQU1BO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0FBSEY7O0FBS0U7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0FBSEo7O0FBT0E7O0VBRUUsaUJBQUE7QUFKRjs7QUFPQTtFQUNFLFVBQUE7QUFKRjs7QUFPQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFKRiIsImZpbGUiOiJzZWUtZGV0YWlsLWludm9pY2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy9mb25kbyBkZSBsYSBpbWFnZW4gZGVsIGNvbnRhaW5lclxyXG4uY29udGFpbmVyIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxufVxyXG5cclxuLmgzYXp1bCB7XHJcbiAgY29sb3I6ICMwMTQ4OTg7XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXIge1xyXG4gIC0tcGFkZGluZy1zdGFydDogNiU7XHJcbiAgLS1wYWRkaW5nLWVuZDogNiU7XHJcbiAgLS1wYWRkaW5nLXRvcDogNiU7XHJcbiAgbWFyZ2luLWJvdHRvbTogNiU7XHJcblxyXG4gIC5pbWctbG9nbyB7XHJcbiAgICB3aWR0aDogNTFweDtcclxuICAgIGhlaWdodDogNTFweDtcclxuICB9XHJcblxyXG4gIC5jb250YWluZXItaW1nLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogODklO1xyXG4gIH1cclxufVxyXG5cclxuLyogaW1nIHtcclxuICBwYWRkaW5nLWxlZnQ6IDEwJTtcclxuICBwYWRkaW5nLXJpZ2h0OiAxMCU7XHJcbiAgaGVpZ2h0OiA1MCU7XHJcbn0gKi9cclxuXHJcbi5wcmV2aWV3LXNsaWRlcyB7XHJcbiAgbWFyZ2luLXRvcDogMjAlO1xyXG4gIGJhY2tncm91bmQ6ICNlNmU2ZTY7XHJcblxyXG4gIGltZyB7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmltYWdlLXNsaWRlLFxyXG4uaW1hZ2UtY2FyZCB7XHJcbiAgb3ZlcmZsb3c6IHZpc2libGU7XHJcbn1cclxuXHJcbi5pbWFnZS1jYXJkIHtcclxuICB6LWluZGV4OiA5O1xyXG59XHJcblxyXG4uYmFja2Ryb3Age1xyXG4gIGhlaWdodDogMjAwJTtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgei1pbmRleDogMTA7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "p1PS":
/*!***************************************************************!*\
  !*** ./src/app/see-detail-invoice/see-detail-invoice.page.ts ***!
  \***************************************************************/
/*! exports provided: SeeDetailInvoicePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeeDetailInvoicePage", function() { return SeeDetailInvoicePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_see_detail_invoice_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./see-detail-invoice.page.html */ "JXvu");
/* harmony import */ var _see_detail_invoice_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./see-detail-invoice.page.scss */ "iBEw");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_banks_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/banks.service */ "3mc2");
/* harmony import */ var _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/upinvoice.service */ "b8cs");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user.service */ "qfBg");








let SeeDetailInvoicePage = class SeeDetailInvoicePage {
    constructor(activateRoute, upinvoiceService, userService, bankService) {
        this.activateRoute = activateRoute;
        this.upinvoiceService = upinvoiceService;
        this.userService = userService;
        this.bankService = bankService;
        this.sliderOpts = {
            zoom: {
                maxRatio: 2
            }
        };
        this.invoiceUid = this.activateRoute.snapshot.paramMap.get('id');
    }
    ngOnInit() {
        this.getInvoiceByUid();
        this.getCurrentUser;
    }
    getInvoiceByUid() {
        this.upinvoiceService.getInvoiceByUid(this.invoiceUid).subscribe(res => {
            this.invoice = res;
            this.getbankByUid(this.invoice.bank);
            this.resolutionAux = this.invoice.resolution;
        });
    }
    getCurrentUser() {
        this.userService.getUserById(this.invoice.useruid).subscribe(res1 => {
            this.user = res1;
        });
    }
    getbankByUid(bankUid) {
        this.bankService.getBankByUid(bankUid).subscribe(res => {
            this.bank = res;
        });
    }
    updateBank() {
        this.invoice.resolution = this.resolutionAux;
        this.upinvoiceService.updateInvoice(this.invoice);
    }
};
SeeDetailInvoicePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_6__["UpinvoiceService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
    { type: _services_banks_service__WEBPACK_IMPORTED_MODULE_5__["BanksService"] }
];
SeeDetailInvoicePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-see-detail-invoice',
        template: _raw_loader_see_detail_invoice_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_see_detail_invoice_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SeeDetailInvoicePage);



/***/ })

}]);
//# sourceMappingURL=see-detail-invoice-see-detail-invoice-module.js.map