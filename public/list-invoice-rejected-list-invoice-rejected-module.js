(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["list-invoice-rejected-list-invoice-rejected-module"],{

/***/ "AqX3":
/*!*******************************************************************************!*\
  !*** ./src/app/list-invoice-rejected/list-invoice-rejected-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: ListInvoiceRejectedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceRejectedPageRoutingModule", function() { return ListInvoiceRejectedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _list_invoice_rejected_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list-invoice-rejected.page */ "ytkb");




const routes = [
    {
        path: '',
        component: _list_invoice_rejected_page__WEBPACK_IMPORTED_MODULE_3__["ListInvoiceRejectedPage"]
    }
];
let ListInvoiceRejectedPageRoutingModule = class ListInvoiceRejectedPageRoutingModule {
};
ListInvoiceRejectedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ListInvoiceRejectedPageRoutingModule);



/***/ }),

/***/ "a+ut":
/*!***********************************************************************!*\
  !*** ./src/app/list-invoice-rejected/list-invoice-rejected.module.ts ***!
  \***********************************************************************/
/*! exports provided: ListInvoiceRejectedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceRejectedPageModule", function() { return ListInvoiceRejectedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _list_invoice_rejected_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-invoice-rejected-routing.module */ "AqX3");
/* harmony import */ var _list_invoice_rejected_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-invoice-rejected.page */ "ytkb");







let ListInvoiceRejectedPageModule = class ListInvoiceRejectedPageModule {
};
ListInvoiceRejectedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _list_invoice_rejected_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListInvoiceRejectedPageRoutingModule"]
        ],
        declarations: [_list_invoice_rejected_page__WEBPACK_IMPORTED_MODULE_6__["ListInvoiceRejectedPage"]]
    })
], ListInvoiceRejectedPageModule);



/***/ }),

/***/ "bKqE":
/*!***********************************************************************!*\
  !*** ./src/app/list-invoice-rejected/list-invoice-rejected.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0LWludm9pY2UtcmVqZWN0ZWQucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "pv/+":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/list-invoice-rejected/list-invoice-rejected.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>list-invoice-rejected</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "ytkb":
/*!*********************************************************************!*\
  !*** ./src/app/list-invoice-rejected/list-invoice-rejected.page.ts ***!
  \*********************************************************************/
/*! exports provided: ListInvoiceRejectedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListInvoiceRejectedPage", function() { return ListInvoiceRejectedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_list_invoice_rejected_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./list-invoice-rejected.page.html */ "pv/+");
/* harmony import */ var _list_invoice_rejected_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list-invoice-rejected.page.scss */ "bKqE");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let ListInvoiceRejectedPage = class ListInvoiceRejectedPage {
    constructor() { }
    ngOnInit() {
    }
};
ListInvoiceRejectedPage.ctorParameters = () => [];
ListInvoiceRejectedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-list-invoice-rejected',
        template: _raw_loader_list_invoice_rejected_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_list_invoice_rejected_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ListInvoiceRejectedPage);



/***/ })

}]);
//# sourceMappingURL=list-invoice-rejected-list-invoice-rejected-module.js.map