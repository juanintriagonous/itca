(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notice-notice-module"],{

/***/ "IUlz":
/*!*****************************************!*\
  !*** ./src/app/notice/notice.module.ts ***!
  \*****************************************/
/*! exports provided: NoticePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticePageModule", function() { return NoticePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _notice_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notice-routing.module */ "hOG8");
/* harmony import */ var _notice_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notice.page */ "lzF2");







let NoticePageModule = class NoticePageModule {
};
NoticePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _notice_routing_module__WEBPACK_IMPORTED_MODULE_5__["NoticePageRoutingModule"]
        ],
        declarations: [_notice_page__WEBPACK_IMPORTED_MODULE_6__["NoticePage"]]
    })
], NoticePageModule);



/***/ }),

/***/ "UxFy":
/*!*****************************************!*\
  !*** ./src/app/notice/notice.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n.container-user {\n  display: flex;\n  justify-content: space-between;\n  padding: 6%;\n}\n\n.container-user div {\n  display: flex;\n  align-items: center;\n}\n\n.container-user div ion-icon {\n  font-size: 25px;\n}\n\n.container-info {\n  padding: 0px 6%;\n}\n\n.container-info h1 {\n  font-weight: bold;\n  font-size: 24px;\n}\n\n.container-img {\n  display: flex;\n  justify-content: center;\n}\n\n.container-img .img {\n  width: 100%;\n  height: 250px;\n  border-radius: 30px;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\nion-toolbar ion-row {\n  justify-content: space-around;\n}\n\nion-toolbar ion-row .circle {\n  width: 51px;\n  height: 51px;\n  background: #E8F8E5;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nion-toolbar ion-row ion-button {\n  --background:#014C9A;\n  --background-activated:#014C9A;\n  --border-radius:30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXG5vdGljZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBREo7O0FBR0E7RUFDTSxjQUFBO0FBQU47O0FBRUEsd0JBQUE7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUNJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFDUjs7QUFDSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFDUjs7QUFFQSxzQ0FBQTs7QUFDQTtFQUNJLHNCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUFJO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUVSOztBQUFJO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBRVI7O0FBR0E7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0FBQUo7O0FBQ0k7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUFDUjs7QUFBUTtFQUNJLGVBQUE7QUFFWjs7QUFHQTtFQUNJLGVBQUE7QUFBSjs7QUFDSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUNSOztBQUdBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBQUo7O0FBQ0k7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQUNSOztBQUlJO0VBQ0ksNkJBQUE7QUFEUjs7QUFFUTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQVo7O0FBR1E7RUFDSSxvQkFBQTtFQUNBLDhCQUFBO0VBQ0Esb0JBQUE7QUFEWiIsImZpbGUiOiJub3RpY2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi8vZm9uZG8gZGUgbGEgaW1hZ2VuIGRlbCBjb250YWluZXJcclxuLmNvbnRhaW5lcntcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy8uLi9hc3NldHMvaW1nL2JhY2tncm91bmQyLnBuZ1wiKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIH1cclxuLmgzYXp1bHtcclxuICAgICAgY29sb3I6IzAxNDg5ODtcclxuICB9XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDo1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICAgIC5jb250YWluZXItaW1nLWhlYWRlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjg5JTtcclxuICAgIH1cclxufVxyXG4vKiBFc3RpbG9zIGRlbCBtZW5zYWplIGRlIGJpZW52ZW5pZG8gKi9cclxuLmNvbnRhaW5lci13ZWxjb21le1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzNweDtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi5jb250YWluZXItdXNlcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBwYWRkaW5nOiA2JTtcclxuICAgIGRpdntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgaW9uLWljb257XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb250YWluZXItaW5mb3tcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgIH1cclxufVxyXG5cclxuLmNvbnRhaW5lci1pbWd7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAuaW1ne1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbmlvbi10b29sYmFye1xyXG4gICAgaW9uLXJvd3tcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICAuY2lyY2xle1xyXG4gICAgICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjRThGOEU1O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlvbi1idXR0b257XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDojMDE0QzlBO1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiMwMTRDOUE7XHJcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czozMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuIl19 */");

/***/ }),

/***/ "VTdB":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notice/notice.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-user\">\r\n\r\n      <div>\r\n        <ion-icon name=\"person-circle\" class=\"mr\"></ion-icon>\r\n        <p>ITCA-App</p>\r\n      </div>\r\n      <div>\r\n        <ion-icon name=\"time-outline\" class=\"mr\"></ion-icon>\r\n        <p>{{notification.updateDate}}</p>\r\n\r\n\r\n      </div>\r\n         \r\n <!--    <p>Hace 3 minutos</p> -->\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-info\">\r\n    <ion-col size=\"12\" class=\"container-img\">\r\n      <div class=\"img\" style=\"background-image: url({{notification.img}});\">\r\n\r\n      </div>\r\n     \r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <h1>{{notification.name }}</h1>\r\n      <p class=\"parrafo\">\r\n     {{notification.description}}\r\n      </p>\r\n      <ion-label color=\"primary\" (click)=\"ventana(notification.hyper)\">Ir a {{notification.name}}</ion-label>\r\n    </ion-col>\r\n    \r\n  </ion-row>\r\n</ion-content>\r\n<!-- \r\n<ion-footer class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n   <ion-row>\r\n     <div class=\"circle\">\r\n        <img src=\"../../assets/img/wpp.png\" alt=\"\">\r\n     </div>\r\n     <ion-button >\r\n       Vinculo\r\n     </ion-button>\r\n     <ion-button >\r\n      Hiperenlace\r\n    </ion-button>\r\n   </ion-row>\r\n  </ion-toolbar>\r\n</ion-footer> -->\r\n");

/***/ }),

/***/ "hOG8":
/*!*************************************************!*\
  !*** ./src/app/notice/notice-routing.module.ts ***!
  \*************************************************/
/*! exports provided: NoticePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticePageRoutingModule", function() { return NoticePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _notice_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notice.page */ "lzF2");




const routes = [
    {
        path: '',
        component: _notice_page__WEBPACK_IMPORTED_MODULE_3__["NoticePage"]
    }
];
let NoticePageRoutingModule = class NoticePageRoutingModule {
};
NoticePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NoticePageRoutingModule);



/***/ }),

/***/ "lzF2":
/*!***************************************!*\
  !*** ./src/app/notice/notice.page.ts ***!
  \***************************************/
/*! exports provided: NoticePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticePage", function() { return NoticePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_notice_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./notice.page.html */ "VTdB");
/* harmony import */ var _notice_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notice.page.scss */ "UxFy");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");






let NoticePage = class NoticePage {
    constructor(rutaActiva, firestore) {
        this.rutaActiva = rutaActiva;
        this.firestore = firestore;
        this.notification = {
            uid: '',
            name: '',
            description: '',
            hyper: '',
            state: '',
            order: '',
            updateDate: '',
            category: '',
            img: '',
        };
        this.id = this.rutaActiva.snapshot.paramMap.get('id');
    }
    ngOnInit() {
        const ref = this.firestore.collection('/notification/').doc(this.id);
        ref.snapshotChanges().subscribe((res) => {
            if (res) {
                this.notification.uid = res.payload.id;
                this.notification.name = res.payload.data()['name'];
                this.notification.description = res.payload.data()['description'];
                this.notification.state = res.payload.data()['state'];
                this.notification.hyper = res.payload.data()['hyper'];
                this.notification.order = res.payload.data()['order'];
                this.notification.updateDate = this.tiempo(res.payload.data()["updateDate"]),
                    this.notification.category = res.payload.data()['category'];
                this.notification.img = res.payload.data()['img'];
            }
            console.log(this.notification.hyper);
        });
    }
    /* Metodo que me sirve para calcular el tiempo de publicación de cada noticia */
    tiempo(time) {
        //  console.log(Date.now() + "");
        let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
        this.publicado = '';
        this.trasformar = 0;
        //Segundos
        if (tiempo > 0 && tiempo < 60) {
            this.trasformar = tiempo;
            this.publicado = this.trasformar + ' Seg';
        }
        //minustos
        else if (tiempo > 60 && tiempo < 3600) {
            this.trasformar = (tiempo / 60).toFixed(0);
            this.publicado = this.trasformar + ' Min';
        }
        //horas
        else if (tiempo > 3600 && tiempo < 86400) {
            this.trasformar = (tiempo / 3600).toFixed(0);
            this.publicado = this.trasformar + ' Horas';
        }
        //Dias
        else if (tiempo > 86400 && tiempo < 604800) {
            this.trasformar = (tiempo / 86400).toFixed(0);
            this.publicado = this.trasformar + ' Dias';
        }
        else if (tiempo > 604800 && tiempo < 2629743) {
            this.trasformar = (tiempo / 604800).toFixed(0);
            this.publicado = this.trasformar + ' Semanas';
        }
        return this.publicado;
    }
    ventana(valor) {
        window.open(valor, '_system');
    }
};
NoticePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"] }
];
NoticePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-notice',
        template: _raw_loader_notice_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_notice_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NoticePage);



/***/ })

}]);
//# sourceMappingURL=notice-notice-module.js.map