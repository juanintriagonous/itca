(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main-main-module"],{

/***/ "/UQe":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main/main.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <h3 class=\"h3azul ion-text-center\">ITCA-App</h3>\r\n    <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    <ion-menu-button slot=\"start\" menu=\"administrador\"></ion-menu-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"container\">\r\n    <ion-row class=\"container-welcome\">\r\n      <h1>Bienvenido</h1>\r\n      <p>Noticias acerca del ITCA</p>\r\n    </ion-row>\r\n    <ion-row class=\"banner\">\r\n      <ion-col size=\"12\">\r\n        <ion-slides [options]=\"bannerConfig\">\r\n          <ion-slide *ngFor=\"let item of lastFivenotification\" routerLink=\"/notice/{{item.id}}\">\r\n\r\n            <ion-card class=\"card-banner\" style=\"background-image:url('{{item.img}}');\">\r\n              <ion-card-header routerLink=\"/notice/{{item.id}}\">\r\n                <div class=\"container-name\">\r\n                  <ion-icon name=\"person-circle\" class=\"mr\"></ion-icon>\r\n                  <p>ITCA-App</p>\r\n                </div>\r\n                <div class=\"time\">\r\n                  <ion-icon name=\"time-outline\" class=\"mr\"></ion-icon>\r\n                  <p>{{item.updateDate}}</p>\r\n                </div>\r\n              </ion-card-header>\r\n              <ion-card-content>\r\n                <h1>{{item.name}}</h1>\r\n                <p>{{substr(item.description)}}...</p>\r\n              </ion-card-content>\r\n            </ion-card>\r\n\r\n          </ion-slide>\r\n        </ion-slides>\r\n      </ion-col>\r\n\r\n    </ion-row>\r\n\r\n    <ion-row class=\"container-title\">\r\n      <ion-col size=\"12\">\r\n        <p>Servicios en Línea</p>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"12\">\r\n        <ion-slides [options]=\"categoryConfig\">\r\n          <ion-slide class=\"slide-themes\" *ngFor=\"let category of Categories; let i = index\">\r\n            <ion-col>\r\n              <ion-card (click)=\"newsQuery(category.name)\">\r\n                <ion-card-header (click)=\"newsQuery(category.name)\">\r\n                  <div class=\"circle\">\r\n                    <img src=\"../../assets/img/notice.png\" alt=\"\" />\r\n                  </div>\r\n\r\n                </ion-card-header>\r\n                <ion-card-content (click)=\"newsQuery(category.name)\">\r\n                  <p>{{ category.name }}</p>\r\n                </ion-card-content>\r\n              </ion-card>\r\n            </ion-col>\r\n          </ion-slide>\r\n        </ion-slides>\r\n      </ion-col>\r\n\r\n\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <p>Más información sobre {{title}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n \r\n      <ion-col size=\"12\" [ngSwitch]=\"type\" class=\"notice\">\r\n\r\n        <ion-item *ngFor=\"let notifica of notification;\">\r\n          <ion-col size=\"5\" class=\"container-img-card\">\r\n            <div routerLink=\"/notice/{{notifica.id}}\" class=\"img-card\" style=\"background-image: url({{notifica.img}});\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"7\" routerLink=\"/notice/{{notifica.id}}\">\r\n            <div class=\"user\">\r\n              <div>\r\n                <ion-icon name=\"person-circle\" class=\"mr\"></ion-icon>\r\n                <p>ITCA-App</p>\r\n              </div>\r\n              <!-- <ion-icon name=\"bookmark-outline\"></ion-icon> -->\r\n            </div>\r\n\r\n            <div class=\"title\">\r\n              <p>{{notifica.name}}</p>\r\n            </div>\r\n            <div class=\"time\">\r\n              <ion-icon name=\"time-outline\" class=\"mr\"></ion-icon>\r\n              <p>{{notifica.updateDate}}</p>\r\n            </div>\r\n          </ion-col>\r\n        </ion-item>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "3hl/":
/*!*********************************************!*\
  !*** ./src/app/main/main-routing.module.ts ***!
  \*********************************************/
/*! exports provided: MainPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPageRoutingModule", function() { return MainPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _main_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main.page */ "Rmbw");




const routes = [
    {
        path: '',
        component: _main_page__WEBPACK_IMPORTED_MODULE_3__["MainPage"]
    }
];
let MainPageRoutingModule = class MainPageRoutingModule {
};
MainPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MainPageRoutingModule);



/***/ }),

/***/ "8QZa":
/*!*************************************!*\
  !*** ./src/app/main/main.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 40px;\n  height: 40px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del banner  */\n\n.banner {\n  padding: 0px 1%;\n  margin-top: 2%;\n  position: relative;\n  z-index: 1;\n}\n\n.banner ion-col .card-banner {\n  width: 100%;\n  height: 210px;\n  border-radius: 21px;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n  z-index: 1;\n}\n\n.banner ion-col .card-banner ion-card-header {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.banner ion-col .card-banner ion-card-header .container-name {\n  display: flex;\n  align-items: center;\n}\n\n.banner ion-col .card-banner ion-card-header .container-name p {\n  margin: 0px;\n}\n\n.banner ion-col .card-banner ion-card-header .container-name ion-icon {\n  color: black;\n  font-size: 30px;\n}\n\n.banner ion-col .card-banner ion-card-content {\n  margin-top: 5%;\n  text-align: left;\n  padding-left: 5%;\n}\n\n.banner ion-col .card-banner ion-card-content h1 {\n  color: #FCFCFC;\n}\n\n.banner ion-col .card-banner ion-card-content p {\n  color: #FCFCFC;\n}\n\n.banner ion-col .card-banner::before {\n  content: \"\";\n  position: absolute;\n  background: rgba(0, 0, 0, 0.6);\n  right: 0px;\n  top: 0px;\n  left: 0px;\n  width: 100%;\n  height: 100%;\n  z-index: -1;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 2%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos del usuario de la noticia */\n\n.user {\n  display: flex;\n  justify-content: space-between;\n}\n\n.user ion-icon {\n  font-size: 25px;\n}\n\n.user div {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.user div p {\n  margin: 0px;\n  font-weight: normal;\n}\n\n/* Estilos del tiempo de la noticia */\n\n.time {\n  display: flex;\n  align-items: center;\n}\n\n.time p {\n  margin: 0px;\n  color: #989AA6;\n  font-weight: normal;\n}\n\n.time ion-icon {\n  color: #989AA6;\n  font-size: 25px;\n}\n\n/* Estilos del ion-item para que cada noticia quede separada */\n\nion-item {\n  margin-bottom: 8%;\n  width: 100%;\n}\n\n/* Estilos de las tarjetas de los temas */\n\n.slide-themes {\n  padding: 0px;\n}\n\n.slide-themes ion-card {\n  width: 95%;\n  margin: 0px;\n  height: 160px;\n  --background: rgba(15, 101, 122, 0.2);\n  border-radius: 15px;\n  box-shadow: none;\n}\n\n.slide-themes ion-card ion-card-header {\n  display: flex;\n  justify-content: center;\n}\n\n.slide-themes ion-card ion-card-header .circle {\n  width: 51px;\n  height: 51px;\n  background: white;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.slide-themes ion-card ion-card-content {\n  padding: 0px;\n  color: black;\n}\n\n.slide-themes ion-card .amount {\n  font-style: normal;\n  font-size: 14px;\n}\n\n/* Estilo para separar las noticas del switch */\n\n.notice {\n  margin-top: 2%;\n}\n\n/* Estilos para separar el componenet segment de los temas */\n\n.segment {\n  margin-top: 4%;\n  display: flex;\n  justify-content: center;\n  border: 3px solid rgba(0, 0, 0, 0.2);\n  color: rgba(0, 0, 0, 0.9);\n  border-radius: 10px;\n  width: 100%;\n}\n\n.segment p {\n  margin: 3px;\n  font-weight: 500;\n  font-size: 18px;\n}\n\n/* Estilos de la imagen de cada noticia */\n\n.container-img-card {\n  display: flex;\n  justify-content: center;\n}\n\n.container-img-card .img-card {\n  width: 100px;\n  height: 100px;\n  border-radius: 15px;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n/* Estilos del icono de de cada tema */\n\n.img-notice {\n  width: 20px;\n  height: 20px;\n  border-radius: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXG1haW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBQTtFQUNBLHdDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQUFKOztBQUVBO0VBQ00sY0FBQTtBQUNOOztBQUVBLHdCQUFBOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFDSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBQ1I7O0FBQ0k7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0FBQ1I7O0FBR0Esc0NBQUE7O0FBQ0E7RUFDSSxzQkFBQTtFQUNBLGVBQUE7QUFBSjs7QUFDSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFDUjs7QUFDSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUNSOztBQUdBLHdCQUFBOztBQUNBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFBSjs7QUFHUTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBRFo7O0FBRVk7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQUFoQjs7QUFDZ0I7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUFDcEI7O0FBQW9CO0VBQ0ksV0FBQTtBQUV4Qjs7QUFBb0I7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQUV4Qjs7QUFHWTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBRGhCOztBQUVnQjtFQUNJLGNBQUE7QUFBcEI7O0FBRWdCO0VBQ0ksY0FBQTtBQUFwQjs7QUFLUTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBSFo7O0FBWUEsb0NBQUE7O0FBQ0E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQVRKOztBQVVJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBUlI7O0FBWUEsc0NBQUE7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7QUFUSjs7QUFVSTtFQUNJLGVBQUE7QUFSUjs7QUFVSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBUlI7O0FBU1E7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7QUFQWjs7QUFZQSxxQ0FBQTs7QUFDQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQVRKOztBQVVJO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQVJSOztBQVVJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFSUjs7QUFZQSw4REFBQTs7QUFDQTtFQUNJLGlCQUFBO0VBQ0EsV0FBQTtBQVRKOztBQVlBLHlDQUFBOztBQUNBO0VBRU0sWUFBQTtBQVZOOztBQVlNO0VBQ0UsVUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EscUNBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBVlI7O0FBV1E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFUWjs7QUFXWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBVGhCOztBQWVRO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFiWjs7QUFnQlE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUFkWjs7QUFvQkUsK0NBQUE7O0FBQ0E7RUFDSSxjQUFBO0FBakJOOztBQW9CRSw0REFBQTs7QUFDQTtFQUNFLGNBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxvQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FBakJKOztBQWtCSTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFoQlI7O0FBb0JFLHlDQUFBOztBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBakJOOztBQWtCSTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQyw0QkFBQTtFQUNELHNCQUFBO0FBaEJSOztBQXVCRSxzQ0FBQTs7QUFDQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFwQkoiLCJmaWxlIjoibWFpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL2ZvbmRvIGRlIGxhIGltYWdlbiBkZWwgY29udGFpbmVyXHJcbi5jb250YWluZXJ7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8vLi4vYXNzZXRzL2ltZy9iYWNrZ3JvdW5kMi5wbmdcIik7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICB9XHJcbi5oM2F6dWx7XHJcbiAgICAgIGNvbG9yOiMwMTQ4OTg7XHJcbiAgfVxyXG5cclxuLyogRXN0aWxvcyBkZWwgaGVhZGVyICAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIC0tcGFkZGluZy1zdGFydDo2JTtcclxuICAgIC0tcGFkZGluZy1lbmQ6NiU7XHJcbiAgICAtLXBhZGRpbmctdG9wOjYlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNiU7XHJcblxyXG4gICAgLmltZy1sb2dve1xyXG4gICAgICAgIHdpZHRoOjQwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lci1pbWctaGVhZGVye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6ODklO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCBtZW5zYWplIGRlIGJpZW52ZW5pZG8gKi9cclxuLmNvbnRhaW5lci13ZWxjb21le1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzNweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgYmFubmVyICAqL1xyXG4uYmFubmVye1xyXG4gICAgcGFkZGluZzogMHB4IDElO1xyXG4gICAgbWFyZ2luLXRvcDogMiU7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6MTtcclxuICAgIGlvbi1jb2x7XHJcblxyXG4gICAgICAgIC5jYXJkLWJhbm5lcntcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMjEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgICAgICAgICB6LWluZGV4OjE7XHJcbiAgICAgICAgICAgIGlvbi1jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgLmNvbnRhaW5lci1uYW1le1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaW9uLWljb257XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsMCwwLDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIGlvbi1jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1JTtcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xyXG4gICAgICAgICAgICAgICAgaDF7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6I0ZDRkNGQztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6I0ZDRkNGQztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtYmFubmVyOjpiZWZvcmV7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6Jyc7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDpyZ2JhKDAsMCwwLDAuNik7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICAgIHRvcDowcHg7XHJcbiAgICAgICAgICAgIGxlZnQ6MHB4O1xyXG4gICAgICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6MTAwJTtcclxuICAgICAgICAgICAgei1pbmRleDotMTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIFxyXG59XHJcblxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGl0dWxvIGRlIGxhIHBhZ2luYSAqL1xyXG4uY29udGFpbmVyLXRpdGxle1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgbWFyZ2luLXRvcDogMiU7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdXN1YXJpbyBkZSBsYSBub3RpY2lhICovXHJcbi51c2Vye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweCA7XHJcbiAgICB9XHJcbiAgICBkaXZ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHB7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgdGllbXBvIGRlIGxhIG5vdGljaWEgKi9cclxuLnRpbWV7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgY29sb3I6Izk4OUFBNiA7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIH1cclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGNvbG9yOiM5ODlBQTYgO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgaW9uLWl0ZW0gcGFyYSBxdWUgY2FkYSBub3RpY2lhIHF1ZWRlIHNlcGFyYWRhICovXHJcbmlvbi1pdGVte1xyXG4gICAgbWFyZ2luLWJvdHRvbTogOCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG4gIFxyXG4vKiBFc3RpbG9zIGRlIGxhcyB0YXJqZXRhcyBkZSBsb3MgdGVtYXMgKi9cclxuLnNsaWRlLXRoZW1lcyB7XHJcbiAgICBcclxuICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICBcclxuICAgICAgaW9uLWNhcmQge1xyXG4gICAgICAgIHdpZHRoOiA5NSU7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxNjBweDtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHJnYmEoMTUsIDEwMSwgMTIyLCAwLjIpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICBpb24tY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgICAgICAgICAgLmNpcmNsZXtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICBjb2xvcjpibGFjaztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5hbW91bnR7XHJcbiAgICAgICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgXHJcbiAgfVxyXG5cclxuICAvKiBFc3RpbG8gcGFyYSBzZXBhcmFyIGxhcyBub3RpY2FzIGRlbCBzd2l0Y2ggKi9cclxuICAubm90aWNle1xyXG4gICAgICBtYXJnaW4tdG9wOiAyJTtcclxuICB9XHJcblxyXG4gIC8qIEVzdGlsb3MgcGFyYSBzZXBhcmFyIGVsIGNvbXBvbmVuZXQgc2VnbWVudCBkZSBsb3MgdGVtYXMgKi9cclxuICAuc2VnbWVudHtcclxuICAgIG1hcmdpbi10b3A6IDQlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYm9yZGVyOiAzcHggc29saWQgcmdiYSgwLDAsMCwwLjIpO1xyXG4gICAgY29sb3I6IHJnYmEoMCwwLDAsMC45KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAzcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKiBFc3RpbG9zIGRlIGxhIGltYWdlbiBkZSBjYWRhIG5vdGljaWEgKi9cclxuICAuY29udGFpbmVyLWltZy1jYXJke1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC5pbWctY2FyZHtcclxuICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICBcclxuICAgIH1cclxuXHJcbiAgfVxyXG4gXHJcblxyXG4gIC8qIEVzdGlsb3MgZGVsIGljb25vIGRlIGRlIGNhZGEgdGVtYSAqL1xyXG4gIC5pbWctbm90aWNle1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAiXX0= */");

/***/ }),

/***/ "Rmbw":
/*!***********************************!*\
  !*** ./src/app/main/main.page.ts ***!
  \***********************************/
/*! exports provided: MainPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPage", function() { return MainPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_main_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./main.page.html */ "/UQe");
/* harmony import */ var _main_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main.page.scss */ "8QZa");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");





let MainPage = class MainPage {
    constructor(firebase) {
        this.firebase = firebase;
        /* Objeto para configurar el slide de las categorias */
        this.categoryConfig = {
            spaceBetween: 0,
            slidesPerView: 2.5,
        };
        /* Obejeto para configurar el slide de los banners   */
        this.bannerConfig = {
            spaceBetween: 10,
            slidesPerView: 1,
            autoplay: true,
        };
        //Se declara un atributo tipo string para almacenar el titulo que se va a presentar en el home.
        this.title = '';
        this.type = 'latest';
    }
    ngOnInit() {
        this.categoryQuery();
        this.lastFiveNewsQuery();
    }
    //Metodo para consultar las empresas dependiendo de los que escoja el usuario
    newsQuery(title) {
        this.title = title;
        this.firebase
            .collection('/notification/', (ref) => ref.where('category', '==', this.title)
            .orderBy("order", "asc"))
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.notification = res.map((e) => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        description: e.payload.doc.data()['description'],
                        creationDate: e.payload.doc.data()['creationDate'],
                        order: e.payload.doc.data()['order'],
                        priority: e.payload.doc.data()['priority'],
                        updateDate: this.tiempo(e.payload.doc.data()["updateDate"]),
                        img: e.payload.doc.data()['img'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    substr(text) {
        let textResult;
        textResult = text.substring(0, 100);
        return textResult;
    }
    /* Metodo para consultar las categorias */
    categoryQuery() {
        // const notificationRef = this.firebase.collection('/notification/', (ref) =>
        //ref.orderBy("updateDate","desc").limit(5))
        this.firebase
            .collection('/category', (ref) => ref.where('state', '==', 'public').orderBy("order", "asc"))
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.Categories = res.map((e, index) => {
                    if (index === 0) {
                        this.newsQuery(e.payload.doc.data()['name']);
                    }
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        description: e.payload.doc.data()['description'],
                        order: e.payload.doc.data()['order'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    /* Metodo para consultar las las prioridades en base de datos */
    priorityQuery() {
        // const notificationRef = this.firebase.collection('/notification/', (ref) =>
        //ref.orderBy("updateDate","desc").limit(5))
        this.firebase
            .collection('/priority', (ref) => ref.where('state', '==', 'public').orderBy("name", "asc"))
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.priorities = res.map((e, index) => {
                    if (index === 0) {
                        this.newsQuery(e.payload.doc.data()['name']);
                    }
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
    /* Metodo que me sirve para calcular el tiempo de publicación de cada noticia */
    tiempo(time) {
        let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
        this.publicado = '';
        this.trasformar = 0;
        //Segundos
        if (tiempo > 0 && tiempo < 60) {
            this.trasformar = tiempo;
            this.publicado = this.trasformar + " Seg";
        }
        //minustos 
        else if (tiempo > 60 && tiempo < 3600) {
            this.trasformar = (tiempo / 60).toFixed(0);
            this.publicado = this.trasformar + " Min";
        }
        //horas 
        else if (tiempo > 3600 && tiempo < 86400) {
            this.trasformar = (tiempo / 3600).toFixed(0);
            this.publicado = this.trasformar + " Horas";
        }
        //Dias 
        else if (tiempo > 86400 && tiempo < 604800) {
            this.trasformar = (tiempo / 86400).toFixed(0);
            this.publicado = this.trasformar + " Dias";
        }
        else if (tiempo > 604800 && tiempo < 2629743) {
            this.trasformar = (tiempo / 604800).toFixed(0);
            this.publicado = this.trasformar + " Semanas";
        }
        return this.publicado;
    }
    /* Metodo para consultar las ultimas 5 noticias publicadas */
    lastFiveNewsQuery() {
        const notificationRef = this.firebase.collection('/notification/', (ref) => ref.orderBy("priority", "asc").limit(5));
        notificationRef
            .snapshotChanges()
            .subscribe((res) => {
            if (res) {
                this.lastFivenotification = res.map((e) => {
                    return {
                        id: e.payload.doc.id,
                        name: e.payload.doc.data()['name'],
                        description: e.payload.doc.data()['description'],
                        creationDate: e.payload.doc.data()['creationDate'],
                        updateDate: this.tiempo(e.payload.doc.data()["updateDate"]),
                        img: e.payload.doc.data()['img'],
                        state: e.payload.doc.data()['state'],
                    };
                });
            }
        });
    }
};
MainPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] }
];
MainPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-main',
        template: _raw_loader_main_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_main_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MainPage);



/***/ }),

/***/ "XpXM":
/*!*************************************!*\
  !*** ./src/app/main/main.module.ts ***!
  \*************************************/
/*! exports provided: MainPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPageModule", function() { return MainPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _main_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main-routing.module */ "3hl/");
/* harmony import */ var _main_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./main.page */ "Rmbw");







let MainPageModule = class MainPageModule {
};
MainPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _main_routing_module__WEBPACK_IMPORTED_MODULE_5__["MainPageRoutingModule"]
        ],
        declarations: [_main_page__WEBPACK_IMPORTED_MODULE_6__["MainPage"]]
    })
], MainPageModule);



/***/ })

}]);
//# sourceMappingURL=main-main-module.js.map