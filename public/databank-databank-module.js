(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["databank-databank-module"],{

/***/ "0uDL":
/*!*****************************************************!*\
  !*** ./src/app/databank/databank-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: DatabankPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabankPageRoutingModule", function() { return DatabankPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _databank_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./databank.page */ "Z284");




const routes = [
    {
        path: '',
        component: _databank_page__WEBPACK_IMPORTED_MODULE_3__["DatabankPage"]
    }
];
let DatabankPageRoutingModule = class DatabankPageRoutingModule {
};
DatabankPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DatabankPageRoutingModule);



/***/ }),

/***/ "2hda":
/*!*********************************************!*\
  !*** ./src/app/databank/databank.module.ts ***!
  \*********************************************/
/*! exports provided: DatabankPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabankPageModule", function() { return DatabankPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _databank_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./databank-routing.module */ "0uDL");
/* harmony import */ var _databank_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./databank.page */ "Z284");







let DatabankPageModule = class DatabankPageModule {
};
DatabankPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _databank_routing_module__WEBPACK_IMPORTED_MODULE_5__["DatabankPageRoutingModule"]
        ],
        declarations: [_databank_page__WEBPACK_IMPORTED_MODULE_6__["DatabankPage"]]
    })
], DatabankPageModule);



/***/ }),

/***/ "3mc2":
/*!*******************************************!*\
  !*** ./src/app/services/banks.service.ts ***!
  \*******************************************/
/*! exports provided: BanksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksService", function() { return BanksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let BanksService = class BanksService {
    constructor(firebase) {
        this.firebase = firebase;
    }
    addBank(bank) {
        bank.createdAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').add(bank);
    }
    updateBank(bank) {
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').doc(bank.uid).update(bank);
    }
    deleteBank(uid) {
        this.firebase.doc(`/bank/${uid}`).delete();
    }
    getBankByUid(uid) {
        const ref = this.firebase
            .collection('/bank/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getBanks() {
        const ref = this.firebase
            .collection('bank')
            .valueChanges({ idField: 'uid' });
        return ref;
    }
};
BanksService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
BanksService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BanksService);



/***/ }),

/***/ "5Y9c":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/databank/databank.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <h1>Administración de Información Bancaria</h1>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Imagen de Ayuda al Estudiante</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div\r\n        class=\"container-icon\"\r\n        style=\"background-image: url({{bank.helpImg}});\" >\r\n        <ion-item lines=\"none\">\r\n          <ion-input\r\n            type=\"file\"\r\n            accept=\"image/*\"\r\n            id=\"file-input\"\r\n            #fileInp\r\n            (change)=\"uploadImageTemporary($event)\">\r\n          </ion-input>\r\n          <button class=\"btn-subir\">\r\n            <ion-icon name=\"arrow-up-outline\"></ion-icon>\r\n          </button>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-card\">\r\n    <ion-card class=\"blue\">\r\n      <ion-card-content class=\"form\">\r\n        <p>Nombre de la Institución</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"bank.name\"></ion-input>\r\n        </ion-item>\r\n        <p>Número de cuenta (Puede contener letras)</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"bank.accountNumber\"></ion-input>\r\n        </ion-item>\r\n        <p>Activo</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-select placeholder=\"Estado de Banco\" [(ngModel)]=\"bank.isActivate\" >\r\n              <ion-select-option value='yes'> Activo</ion-select-option>\r\n              <ion-select-option value='no'> Inactivo</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>    \r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn margindown\">\r\n    <ion-button (click)=\"addBank()\"> Guardar Banco </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"container-title\">\r\n      <h1>Carreras Registradas</h1>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <ion-grid fixed>\r\n        <ion-row class=\"container-card margindown\" *ngFor=\"let item of banks; let i = index\">\r\n          <ion-col>\r\n            <ion-card style=\"margin-top: 20px;\">\r\n\r\n              <ion-card-content class=\"form\">\r\n                <h1>{{item.name}}</h1>\r\n                <p> <strong>Nombre: </strong>{{item.name}}</p>\r\n                <p> <strong>Nro de Cuenta: </strong>{{item.accountNumber}}</p>\r\n                <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"{{item.helpImg}}\" alt=\"\" />\r\n                <p>\r\n                  <strong>Estado: </strong>\r\n                  <ion-note *ngIf=\"item.isActivate == 'Habilitado'\" color=\"success\">Habilitada</ion-note>\r\n                  <ion-note *ngIf=\"item.isActivate == 'Deshabilitado'\" color=\"danger\">Deshabilitada</ion-note>\r\n                </p>\r\n                <ion-row class=\"container-btns\">\r\n                  <!-- <ion-item\r\n                    (click)=\"UpdateCareer(item)\">\r\n                    <ion-label class=\"icon\">\r\n                      <ion-icon name=\"pencil-outline\"></ion-icon>\r\n                    </ion-label>\r\n                  </ion-item> -->\r\n                  <ion-item (click)=\"DeleteBank(item.uid)\">\r\n                    <ion-label class=\"icon\">\r\n                      <ion-icon name=\"trash-outline\"></ion-icon>\r\n                    </ion-label>\r\n                  </ion-item>\r\n                </ion-row>\r\n              </ion-card-content>\r\n            </ion-card>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>");

/***/ }),

/***/ "VZ/b":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/updatebank/updatebank.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\r\n  updatebank works!\r\n</p>\r\n");

/***/ }),

/***/ "Y2mO":
/*!*********************************************!*\
  !*** ./src/app/databank/databank.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n.btn-subir {\n  background-color: #94c9d4;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos del icono de subir categoria */\n\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n  border: 2px solid #979797;\n  width: 100%;\n  height: 300px;\n  border-radius: 21px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n}\n\n.container-icon ion-item {\n  --background:rgba(0,0,0,0);\n}\n\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n#file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n\n/* Estilos de la tarjeta de los inputs */\n\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n\n.container-card ion-card {\n  width: 90%;\n  box-shadow: none;\n  border-radius: 15px;\n}\n\n.container-card ion-card ion-card-content {\n  padding: 20px;\n  padding-top: 10px;\n}\n\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n\n.container-card ion-card ion-card-content .categories {\n  margin-top: 5%;\n}\n\n.container-card ion-card ion-card-content .categories ion-select {\n  --placeholder-opacity:1;\n}\n\n.select-black {\n  --background: #161616;\n  color: white;\n}\n\n.container-link {\n  padding: 0px 6%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.container-link p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n.container-link .circle {\n  width: 40px;\n  height: 40px;\n  background: #333333;\n  color: white;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.blue {\n  background-color: #DDF2FB;\n}\n\n.green {\n  background-color: #E8F8E5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGRhdGFiYW5rLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLFlBQUE7RUFDQSx3Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUFBSjs7QUFFQTtFQUNNLGNBQUE7QUFDTjs7QUFDQSx3QkFBQTs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBRUo7O0FBQUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUVSOztBQUFJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtBQUVSOztBQUVBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQSxzQ0FBQTs7QUFDQTtFQUNJLHNCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUFJO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUVSOztBQUFJO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBRVI7O0FBRUEsb0NBQUE7O0FBQ0E7RUFDSSxlQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUFJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBRVI7O0FBR0EseUNBQUE7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0FBQUo7O0FBQ0k7RUFDSSwwQkFBQTtBQUNSOztBQUNJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFDUjs7QUFJQTtFQUNJLFVBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0FBREo7O0FBTUEsd0NBQUE7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFISjs7QUFJSTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBRlI7O0FBR1E7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7QUFEWjs7QUFFWTtFQUNJLFdBQUE7RUFDQSx1QkFBQTtBQUFoQjs7QUFFYztFQUNJLGlCQUFBO0VBQ0EsWUFBQTtBQUFsQjs7QUFFYztFQUNFLG9CQUFBO0FBQWhCOztBQUljO0VBQ0ksY0FBQTtBQUZsQjs7QUFHa0I7RUFDRSx1QkFBQTtBQURwQjs7QUFTQTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtBQU5KOztBQVNBO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtBQU5KOztBQU9JO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0FBTFI7O0FBT0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFMUjs7QUFXQTtFQUNJLHlCQUFBO0FBUko7O0FBV0E7RUFDSSx5QkFBQTtBQVJKIiwiZmlsZSI6ImRhdGFiYW5rLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vZm9uZG8gZGUgbGEgaW1hZ2VuIGRlbCBjb250YWluZXJcclxuLmNvbnRhaW5lcntcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy8uLi9hc3NldHMvaW1nL2JhY2tncm91bmQyLnBuZ1wiKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIH1cclxuLmgzYXp1bHtcclxuICAgICAgY29sb3I6IzAxNDg5ODtcclxuICB9XHJcbi8qIEVzdGlsb3MgZGVsIGhlYWRlciAgKi9cclxuaW9uLXRvb2xiYXJ7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6NiU7XHJcbiAgICAtLXBhZGRpbmctZW5kOjYlO1xyXG4gICAgLS1wYWRkaW5nLXRvcDo2JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG5cclxuICAgIC5pbWctbG9nb3tcclxuICAgICAgICB3aWR0aDo1MXB4O1xyXG4gICAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICAgIC5jb250YWluZXItaW1nLWhlYWRlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjg5JTtcclxuICAgIH1cclxufVxyXG4vL0VzdGlsb3MgZGUgYm90b24gc3ViaXIgaW1nXHJcbi5idG4tc3ViaXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiM5NGM5ZDQ7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogNDVweDtcclxuICAgIGhlaWdodDogNDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiA1JTtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIH1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIG1lbnNhamUgZGUgYmllbnZlbmlkbyAqL1xyXG4uY29udGFpbmVyLXdlbGNvbWV7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAzM3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBFc3RpbG9zIGRlbCB0aXR1bG8gZGUgbGEgcGFnaW5hICovXHJcbi5jb250YWluZXItdGl0bGV7XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qIEVzdGlsb3MgZGVsIGljb25vIGRlIHN1YmlyIGNhdGVnb3JpYSAqL1xyXG4uY29udGFpbmVyLWljb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkICM5Nzk3OTc7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBpb24taXRlbXtcclxuICAgICAgICAtLWJhY2tncm91bmQ6cmdiYSgwLDAsMCwwKTtcclxuICAgIH1cclxuICAgIC5jaXJjbGV7XHJcbiAgICAgICAgd2lkdGg6IDUxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNDQkQ4REM7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgIFxyXG59XHJcblxyXG4jZmlsZS1pbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgei1pbmRleDogOTk5O1xyXG4gIH1cclxuXHJcblxyXG5cclxuLyogRXN0aWxvcyBkZSBsYSB0YXJqZXRhIGRlIGxvcyBpbnB1dHMgKi9cclxuLmNvbnRhaW5lci1jYXJke1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgaW9uLWNhcmR7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGlvbi1zZWxlY3Qge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaW9uLWl0ZW17XHJcbiAgICAgICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6MTBweDtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAuY2F0ZWdvcmllc3tcclxuICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgICAgICAgICAgICAgIGlvbi1zZWxlY3R7XHJcbiAgICAgICAgICAgICAgICAgICAgLS1wbGFjZWhvbGRlci1vcGFjaXR5OjE7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnNlbGVjdC1ibGFja3tcclxuICAgIC0tYmFja2dyb3VuZDogIzE2MTYxNjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmNvbnRhaW5lci1saW5re1xyXG4gICAgcGFkZGluZzogMHB4IDYlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgfVxyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzMzMzMzMztcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4uYmx1ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6I0RERjJGQjsgXHJcbn1cclxuXHJcbi5ncmVlbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6I0U4RjhFNTsgXHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG4iXX0= */");

/***/ }),

/***/ "YJrD":
/*!*****************************************************************!*\
  !*** ./src/app/components/updatebank/updatebank.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1cGRhdGViYW5rLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "Z284":
/*!*******************************************!*\
  !*** ./src/app/databank/databank.page.ts ***!
  \*******************************************/
/*! exports provided: DatabankPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabankPage", function() { return DatabankPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_databank_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./databank.page.html */ "5Y9c");
/* harmony import */ var _databank_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./databank.page.scss */ "Y2mO");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_updatebank_updatebank_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/updatebank/updatebank.component */ "eCZc");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/alert.service */ "3LUQ");
/* harmony import */ var _services_banks_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/banks.service */ "3mc2");
/* harmony import */ var _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/firebase-auth-service.service */ "1x2Z");
/* harmony import */ var _services_img_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/img.service */ "t3Fp");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/user.service */ "qfBg");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../shared/Errors/listErrors */ "Q98m");
/* harmony import */ var _shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../shared/utilities/formValidation */ "ejrE");














let DatabankPage = class DatabankPage {
    constructor(firebase, authService, toastController, modalController, userService, alertCtrl, alertService, bankService, imgService) {
        this.firebase = firebase;
        this.authService = authService;
        this.toastController = toastController;
        this.modalController = modalController;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.alertService = alertService;
        this.bankService = bankService;
        this.imgService = imgService;
        this.bank = {
            uid: '',
            name: '',
            accountNumber: '',
            helpImg: '',
            isActivate: '',
        };
        this.imgFile = null;
        this.validationRules = {
            required: ['name', 'accountNumber', 'helpImg', 'isActivate'],
        };
        this.errors = {
            name: null,
            accountNumber: null,
            helpImg: null,
            isActivate: null,
        };
    }
    ngOnInit() {
        this.getBanks();
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getCurrentUser();
            return userSession.uid;
        });
    }
    getUserByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userUid = yield this.getUserId();
            this.userService.getUserById(userUid).subscribe((res) => {
                this.user = res;
            });
        });
    }
    getBanks() {
        this.bankService.getBanks().subscribe(res => {
            this.banks = res;
        });
    }
    //Add bank (insert)
    addBank() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (!this.validForm()) {
                return;
            }
            this.bank.helpImg = yield this.imgService.uploadImage('bank', this.imgFile);
            //this.alertService.presentLoading('Agregando Banco ...')
            this.bankService.addBank(this.bank);
            this.clearData();
            //this.alertService.dismissLoading();
        });
    }
    //update
    UpdateBank(id, name, accountNumber, helpImg, isActivate, createdAt, updatedAt) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_updatebank_updatebank_component__WEBPACK_IMPORTED_MODULE_6__["UpdatebankComponent"],
                cssClass: 'my-custom-class',
                componentProps: {
                    'id': id,
                    'name': name,
                    'accountNumber': accountNumber,
                    'helpImg': helpImg,
                    'isActivate': isActivate,
                    'createdAt': createdAt,
                    'updatedAt': updatedAt,
                }
            });
            return yield modal.present();
        });
    }
    /**
       * Método para validar los campos
       * @returns Retorna verdadero si no hay errores y falso si hay algún error
       */
    validForm() {
        const errors = Object(_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_13__["validateForm"])(this.bank, this.validationRules);
        this.errors = errors;
        const validForm = Object.keys(errors).length;
        return !validForm;
    }
    //delete user
    DeleteBank(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const res = yield this.alertService.presentAlertConfirm('Eliminar', '¿Desea eliminar este Banco?');
            if (res) {
                this.alertService.presentLoading('Eliminando Información Bancaria');
                this.firebase.doc('/bank/' + id).delete().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.alertService.loading.dismiss();
                    const toast = yield this.toastController.create({
                        message: 'Eliminación de Banco/Cooperativa Exitosa 🤨',
                        duration: 2000
                    });
                    toast.present();
                })).catch((e) => {
                    this.alertService.loading.dismiss();
                    this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_12__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_12__["listErrors"]['app/general']);
                });
            }
        });
    }
    clearData() {
        this.bank = {
            uid: '',
            name: '',
            accountNumber: '',
            helpImg: '',
            isActivate: '',
        };
    }
    mensaje(title, messag) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                message: messag,
                subHeader: title,
                buttons: [{ text: 'Aceptar', role: 'cancel' }],
            });
            yield alert.present();
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.bank.helpImg = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
};
DatabankPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] },
    { type: _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_9__["FirebaseAuthServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_11__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_7__["AlertService"] },
    { type: _services_banks_service__WEBPACK_IMPORTED_MODULE_8__["BanksService"] },
    { type: _services_img_service__WEBPACK_IMPORTED_MODULE_10__["ImgService"] }
];
DatabankPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-databank',
        template: _raw_loader_databank_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_databank_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DatabankPage);



/***/ }),

/***/ "eCZc":
/*!***************************************************************!*\
  !*** ./src/app/components/updatebank/updatebank.component.ts ***!
  \***************************************************************/
/*! exports provided: UpdatebankComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatebankComponent", function() { return UpdatebankComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_updatebank_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./updatebank.component.html */ "VZ/b");
/* harmony import */ var _updatebank_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./updatebank.component.scss */ "YJrD");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let UpdatebankComponent = class UpdatebankComponent {
    constructor() { }
    ngOnInit() { }
};
UpdatebankComponent.ctorParameters = () => [];
UpdatebankComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-updatebank',
        template: _raw_loader_updatebank_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_updatebank_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpdatebankComponent);



/***/ })

}]);
//# sourceMappingURL=databank-databank-module.js.map