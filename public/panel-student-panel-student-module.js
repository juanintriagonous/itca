(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-student-panel-student-module"],{

/***/ "+OKA":
/*!*****************************************************!*\
  !*** ./src/app/panel-student/panel-student.page.ts ***!
  \*****************************************************/
/*! exports provided: PanelStudentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelStudentPage", function() { return PanelStudentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_panel_student_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./panel-student.page.html */ "zk5T");
/* harmony import */ var _panel_student_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./panel-student.page.scss */ "ApB9");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let PanelStudentPage = class PanelStudentPage {
    constructor() { }
    ngOnInit() {
    }
};
PanelStudentPage.ctorParameters = () => [];
PanelStudentPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-panel-student',
        template: _raw_loader_panel_student_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_panel_student_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PanelStudentPage);



/***/ }),

/***/ "ApB9":
/*!*******************************************************!*\
  !*** ./src/app/panel-student/panel-student.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYW5lbC1zdHVkZW50LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "hv3e":
/*!*******************************************************!*\
  !*** ./src/app/panel-student/panel-student.module.ts ***!
  \*******************************************************/
/*! exports provided: PanelStudentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelStudentPageModule", function() { return PanelStudentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _panel_student_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./panel-student-routing.module */ "sK2v");
/* harmony import */ var _panel_student_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./panel-student.page */ "+OKA");







let PanelStudentPageModule = class PanelStudentPageModule {
};
PanelStudentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _panel_student_routing_module__WEBPACK_IMPORTED_MODULE_5__["PanelStudentPageRoutingModule"]
        ],
        declarations: [_panel_student_page__WEBPACK_IMPORTED_MODULE_6__["PanelStudentPage"]]
    })
], PanelStudentPageModule);



/***/ }),

/***/ "sK2v":
/*!***************************************************************!*\
  !*** ./src/app/panel-student/panel-student-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: PanelStudentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelStudentPageRoutingModule", function() { return PanelStudentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _panel_student_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./panel-student.page */ "+OKA");




const routes = [
    {
        path: '',
        component: _panel_student_page__WEBPACK_IMPORTED_MODULE_3__["PanelStudentPage"]
    }
];
let PanelStudentPageRoutingModule = class PanelStudentPageRoutingModule {
};
PanelStudentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PanelStudentPageRoutingModule);



/***/ }),

/***/ "zk5T":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/panel-student/panel-student.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>panel-student</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=panel-student-panel-student-module.js.map