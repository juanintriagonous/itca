(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register-register-module"],{

/***/ "2t07":
/*!*****************************************************!*\
  !*** ./src/app/register/register-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: RegisterPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageRoutingModule", function() { return RegisterPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register.page */ "b0Bx");




const routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
    }
];
let RegisterPageRoutingModule = class RegisterPageRoutingModule {
};
RegisterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RegisterPageRoutingModule);



/***/ }),

/***/ "UgDh":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\r\n  <div class=\"header\">\r\n    <ion-row class=\"logo-container\">\r\n      <img class=\"img-signIn\" src=\"../../assets/img/logonuevo.png\" />\r\n    </ion-row>\r\n\r\n    <ion-segment [(ngModel)]=\"selectedAction\" (click)=\"selectedActionChange($event)\">\r\n      <ion-segment-button value=\"login\">\r\n        <ion-label>Iniciar sesión</ion-label>\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"register\">\r\n        <ion-label>Regístrate</ion-label>\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n  </div>\r\n\r\n  <ion-row class=\"container\">\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\">NOMBRES Y APELLIDOS</ion-label>\r\n      <ion-item>\r\n        <ion-input [(ngModel)]=\"user.fullname\" name=\"name\" type=\"text\"></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.fullname\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.fullname}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\"> CÉDULA</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"reg_number\" type=\"text\" [(ngModel)]=\"user.identificationDocument\"></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.identificationDocument\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.identificationDocument}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\">NRO CELULAR</ion-label>\r\n      <ion-item>\r\n        <ion-input\r\n            type=\"tel\"\r\n            placeholder=\"Numero de teléfono\"\r\n            name=\"phone\"\r\n            maxlength=\"10\"\r\n            [(ngModel)]=\"user.phoneNumber\"\r\n            required\r\n          ></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.phoneNumber\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.phoneNumber}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\">EMAIL INSTITUCIONAL</ion-label>\r\n      <ion-item>\r\n        <ion-input name=\"email\" type=\"text\" [(ngModel)]=\"user.email\"></ion-input>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.email\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.email}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\">CONTRASEÑA</ion-label>\r\n      <ion-item [ngClass]=\"{'ion-item-error': errors.password}\">\r\n        <ion-input [type]=\"passwordType\" name=\"password\" [(ngModel)]=\"password\"></ion-input>\r\n        <ion-icon [name]=\"eyeTypeIcon\" item-right (click)=\"showHiddePassword()\"></ion-icon>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.password\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.password}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\">TRATO</ion-label>\r\n      <ion-item class=\"select-ionItem\">\r\n        <ion-select placeholder=\"Escoja como desea ser tratado\" cancelText=\"Cancelar\" name=\"gender\" [(ngModel)]=\"user.gender\" value=\"F\" okText=\"Aceptar\">\r\n          <ion-select-option value=\"Srta\"><span>Srta.</span></ion-select-option>\r\n          <ion-select-option value=\"Señor\"><span>Señor</span></ion-select-option>\r\n          <ion-select-option value=\"Señora\"><span>Señora</span></ion-select-option>\r\n          <ion-select-option value=\"Otros\"><span>Otros</span></ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.gender\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.gender}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\">\r\n      <ion-label class=\"label\" position=\"stacked\">CARRERA</ion-label>\r\n      <ion-item class=\"select-ionItem\">\r\n        <ion-select placeholder=\"Seleccione su Carrera\"[(ngModel)]=\"user.career\" (ionChange)=\"onChangeCarrer($event)\">\r\n          <ion-select-option *ngFor=\"let item of careers\" value=\"{{item.uid}}\"><span>{{item.name}}</span>\r\n          </ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.career\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.career}}</span>\r\n      </div>\r\n    </div>\r\n    <div class=\"container-inputs\"> \r\n      <ion-label class=\"label\" position=\"stacked\">NIVEL</ion-label>\r\n      <ion-item class=\"select-ionItem\">\r\n        <ion-select placeholder=\"Seleccione su Nivel\" [(ngModel)]=\"user.level\">\r\n          <div *ngFor=\"let item of levels; let i = index\">\r\n            <ion-select-option value=\"{{item.uid}}\"><span>{{item.name}}</span>\r\n            </ion-select-option>\r\n          </div>\r\n     \r\n        </ion-select>\r\n      </ion-item>\r\n      <div class=\"input-error ion-margin-top\" *ngIf=\"errors.level\">\r\n        <span><ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.level}}</span>\r\n      </div>\r\n    </div>\r\n  </ion-row>\r\n  <button class=\"my_buttton\" (click)=\"registerUser()\">Registrate</button>\r\n</ion-content>");

/***/ }),

/***/ "b0Bx":
/*!*******************************************!*\
  !*** ./src/app/register/register.page.ts ***!
  \*******************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./register.page.html */ "UgDh");
/* harmony import */ var _register_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register.page.scss */ "x/mg");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/alert.service */ "3LUQ");
/* harmony import */ var _services_career_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/career.service */ "Uvke");
/* harmony import */ var _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/firebase-auth-service.service */ "1x2Z");
/* harmony import */ var _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/Errors/listErrors */ "Q98m");
/* harmony import */ var _shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/utilities/formValidation */ "ejrE");










let RegisterPage = class RegisterPage {
    constructor(router, authService, alertService, careerService) {
        this.router = router;
        this.authService = authService;
        this.alertService = alertService;
        this.careerService = careerService;
        this.selectedAction = 'register';
        this.passwordType = 'password';
        this.eyeTypeIcon = 'eye';
        this.user = {
            email: '',
            role: 'student',
            fullname: '',
            identificationDocument: '',
            gender: '',
            career: null,
            image: '',
            level: null,
            isActive: true,
        };
        this.career = {
            uid: '',
            name: '',
            description: '',
            tittleType: '',
            isActivate: '',
            createdAt: null,
            updatedAt: null,
        };
        this.password = '';
        this.validationRules = {
            required: ['email', 'identificationDocument', 'role', 'fullname', 'password', 'gender', 'career', 'level'],
            email: ['email'],
            emailitca: ['email'],
            document: [{ type: 'CI', field: 'identificationDocument' }],
            noNumber: ['fullname'],
            length: [
                { field: 'fullname', min: 10, max: 60 },
                { field: 'password', min: 8, max: 45 },
                //{ field: 'phoneNumber', min: 10, max: 10 },
                { field: 'identificationDocument', min: 10, max: 10 },
            ],
        };
        this.errors = {
            email: null,
            role: null,
            fullname: null,
            emailitca: null,
            password: null,
            //phoneNumber: null,
            gender: null,
            career: null,
            identificationDocument: null,
            level: null
        };
    }
    ngOnInit() {
        this.getCareer();
    }
    showHiddePassword() {
        this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off' : 'eye';
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    }
    registerUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (!this.validaForm()) {
                return;
            }
            yield this.alertService.presentLoading('Creando cuenta...');
            try {
                yield this.authService.signUp(this.user, this.password, this.user.identificationDocument, this.user.phoneNumber, this.user.gender, this.user.career, this.user.level);
                this.alertService.loading.dismiss();
                yield this.alertService.presentAlert('Su cuenta ha sido creada con éxito. Revise con correo para verificarla. 👻');
                this.router.navigate(['/login']);
            }
            catch (e) {
                this.alertService.loading.dismiss();
                yield this.alertService.presentAlert(_shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_8__["listErrors"][e.code] || _shared_Errors_listErrors__WEBPACK_IMPORTED_MODULE_8__["listErrors"]['app/general']);
            }
        });
    }
    onChangeCarrer({ detail }) {
        const auxCareer = this.careers.find((c) => (c.uid === detail.value));
        this.selectedCareer = auxCareer;
        this.levels = auxCareer.levels.filter((c) => (c.isActivate === true));
    }
    onChangeLevel({ detail }) {
        const auxLevel = this.levels[detail.value];
        this.selectedLevel = auxLevel;
    }
    validaForm() {
        const errors = Object(_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_9__["validateForm"])(Object.assign(Object.assign({}, this.user), { password: this.password }), this.validationRules);
        this.errors = errors;
        const validForm = Object.keys(errors).length;
        return !validForm;
    }
    selectedActionChange(event) {
        if (event.target.value !== 'register') {
            this.selectedAction = 'register';
            this.router.navigate(['/login']);
        }
    }
    getCareer() {
        this.careerService.getCareers().subscribe(res => {
            this.careers = res;
        });
    }
};
RegisterPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_7__["FirebaseAuthServiceService"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: _services_career_service__WEBPACK_IMPORTED_MODULE_6__["CareerService"] }
];
RegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-register',
        template: _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_register_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RegisterPage);



/***/ }),

/***/ "x/mg":
/*!*********************************************!*\
  !*** ./src/app/register/register.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".img-signIn {\n  width: 60%;\n  height: 60%;\n  margin-top: 0%;\n  margin-bottom: 3%;\n}\n\n#file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n\nh4 {\n  color: #3E4958;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 20px;\n  line-height: 28px;\n}\n\nion-row {\n  display: flex;\n  justify-content: center;\n  margin-top: 10%;\n}\n\n.container-inputs {\n  margin-bottom: 0%;\n  padding: 10px;\n  width: 90%;\n}\n\n/* Estilos del contenedor del select */\n\n.container-select-role {\n  justify-content: center;\n  margin-top: 10%;\n}\n\n.container-select-role p {\n  color: #97ADB6;\n  font-size: 15px;\n}\n\n.container-select-role p span {\n  color: #008D36;\n  text-decoration: underline;\n}\n\n/* Estilos del ion-item que contiene el ion-select */\n\n.select-ionItem {\n  width: 90%;\n  --background: #F7F8F9;\n  --border-radius: 15px;\n  --color: #008D36;\n  --border-color: #D5DDE0;\n  --border-style: solid;\n}\n\n.select-ionItem ion-select {\n  justify-content: center;\n  width: 100%;\n}\n\n.select-ionItem ion-select ion-select-option span {\n  color: #3E4958;\n}\n\n.container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n\n.logo-container {\n  display: flex;\n  justify-content: center;\n  margin-top: 5%;\n}\n\nion-item {\n  --background: #F2F2F2;\n  --highlight-color-focused:#014C9A;\n  --highlight-color-valid:#014C9A;\n  border: 0.5px solid #D5DDE0;\n  width: 100%;\n  border-radius: 15px;\n  margin-top: 3%;\n}\n\n.label {\n  color: #3E4958;\n  font-size: 14px;\n  opacity: 0.8;\n  font-weight: bold;\n  padding-left: 10px;\n}\n\nh5 {\n  color: #3E4958;\n}\n\n.my_buttton {\n  /* Green */\n  margin-top: 5%;\n  margin-left: 10%;\n  margin-right: 10%;\n  background: #014C9A;\n  height: 60px;\n  width: 80%;\n  box-shadow: 0px 4px 20px rgba(255, 255, 255, 0.25);\n  border-radius: 15px;\n  color: #ffffff;\n  font-size: 18px;\n  font-weight: bold;\n  margin-bottom: 5%;\n}\n\n.container-btn {\n  padding-top: 10%;\n  margin-left: 15%;\n  color: #97ADB6;\n}\n\n/* Solid border */\n\nhr.solid {\n  border-top: 3px solid #bbb;\n  padding: 5px;\n  margin-left: 5%;\n  margin-right: 5%;\n}\n\nh5 {\n  color: #3E4958;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 17px;\n  line-height: 28px;\n}\n\n.img-fb {\n  margin-top: 0%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHJlZ2lzdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQUNGOztBQUdFO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFBSjs7QUFHRTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7QUFBTjs7QUFJQTtFQUNFLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7QUFERjs7QUFJQSxzQ0FBQTs7QUFDQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtBQURGOztBQUdFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUFESjs7QUFHSTtFQUNFLGNBQUE7RUFDQSwwQkFBQTtBQUROOztBQU1BLG9EQUFBOztBQUNBO0VBQ0UsVUFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7QUFIRjs7QUFLRTtFQUNFLHVCQUFBO0VBQ0EsV0FBQTtBQUhKOztBQU1NO0VBQ0UsY0FBQTtBQUpSOztBQVlBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFUSjs7QUFZQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFUSjs7QUFhQTtFQUNJLHFCQUFBO0VBQ0EsaUNBQUE7RUFDQSwrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQVZKOztBQWFBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQVZGOztBQWFBO0VBQ0UsY0FBQTtBQVZGOztBQWFBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtEQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUFWRjs7QUFhQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBVkY7O0FBYUEsaUJBQUE7O0FBQ0E7RUFDRSwwQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFWRjs7QUFZQTtFQUNFLGNBQUE7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBVEo7O0FBWUE7RUFDRSxjQUFBO0FBVEYiLCJmaWxlIjoicmVnaXN0ZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZy1zaWduSW57XHJcbiAgd2lkdGg6IDYwJTtcclxuICBoZWlnaHQ6IDYwJTtcclxuICBtYXJnaW4tdG9wOiAwJTtcclxuICBtYXJnaW4tYm90dG9tOiAzJTtcclxuICAvL2JhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuI2ZpbGUtaW5wdXQge1xyXG4gIG9wYWNpdHk6IDA7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgbGVmdDogMDtcclxuICB6LWluZGV4OiA5OTk7XHJcbn1cclxuXHJcblxyXG4gIGg0IHtcclxuICAgIGNvbG9yOiAjM0U0OTU4O1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjhweDtcclxuICB9XHJcblxyXG4gIGlvbi1yb3d7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgfVxyXG5cclxuXHJcbi5jb250YWluZXItaW5wdXRzIHtcclxuICBtYXJnaW4tYm90dG9tOjAlO1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgd2lkdGg6IDkwJTtcclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgY29udGVuZWRvciBkZWwgc2VsZWN0ICovXHJcbi5jb250YWluZXItc2VsZWN0LXJvbGUge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDEwJTtcclxuXHJcbiAgcCB7XHJcbiAgICBjb2xvcjogIzk3QURCNjtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuXHJcbiAgICBzcGFuIHtcclxuICAgICAgY29sb3I6ICMwMDhEMzY7XHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLyogRXN0aWxvcyBkZWwgaW9uLWl0ZW0gcXVlIGNvbnRpZW5lIGVsIGlvbi1zZWxlY3QgKi9cclxuLnNlbGVjdC1pb25JdGVtIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIC0tYmFja2dyb3VuZDogI0Y3RjhGOTtcclxuICAtLWJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgLS1jb2xvcjogIzAwOEQzNjtcclxuICAtLWJvcmRlci1jb2xvcjogI0Q1RERFMDtcclxuICAtLWJvcmRlci1zdHlsZTogc29saWQ7XHJcblxyXG4gIGlvbi1zZWxlY3Qge1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICBpb24tc2VsZWN0LW9wdGlvbiB7XHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIGNvbG9yOiAjM0U0OTU4XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG5cclxufVxyXG5cclxuLmNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmxvZ28tY29udGFpbmVye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogNSU7XHJcbn1cclxuXHJcblxyXG5pb24taXRlbSB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNGMkYyRjI7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiMwMTRDOUE7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDojMDE0QzlBO1xyXG4gICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjRDVEREUwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMyU7XHJcbn1cclxuXHJcbi5sYWJlbCB7XHJcbiAgY29sb3I6ICMzRTQ5NTg7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIG9wYWNpdHk6IDAuODtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbn1cclxuXHJcbmg1e1xyXG4gIGNvbG9yOiMzRTQ5NTg7XHJcbn1cclxuXHJcbi5teV9idXR0dG9ue1xyXG4gIC8qIEdyZWVuICovXHJcbiAgbWFyZ2luLXRvcDogNSU7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICBiYWNrZ3JvdW5kOiMwMTRDOUE7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgYm94LXNoYWRvdzogMHB4IDRweCAyMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogNSU7XHJcbn1cclxuXHJcbi5jb250YWluZXItYnRue1xyXG4gIHBhZGRpbmctdG9wOiAxMCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICBjb2xvcjogIzk3QURCNjtcclxufVxyXG5cclxuLyogU29saWQgYm9yZGVyICovXHJcbmhyLnNvbGlkIHtcclxuICBib3JkZXItdG9wOiAzcHggc29saWQgI2JiYjtcclxuICBwYWRkaW5nOiA1cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIG1hcmdpbi1yaWdodDogNSU7XHJcbn1cclxuaDUge1xyXG4gIGNvbG9yOiAjM0U0OTU4O1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjhweDtcclxufVxyXG5cclxuLmltZy1mYnsgIFxyXG4gIG1hcmdpbi10b3A6IDAlO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "x5bZ":
/*!*********************************************!*\
  !*** ./src/app/register/register.module.ts ***!
  \*********************************************/
/*! exports provided: RegisterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _register_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register-routing.module */ "2t07");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register.page */ "b0Bx");







let RegisterPageModule = class RegisterPageModule {
};
RegisterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _register_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterPageRoutingModule"]
        ],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]]
    })
], RegisterPageModule);



/***/ })

}]);
//# sourceMappingURL=register-register-module.js.map