(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["upinvoice-upinvoice-module"],{

/***/ "04LR":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/upinvoice/upinvoice.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"panel-admin\"></ion-back-button>\r\n    <ion-toolbar>\r\n      <h3 class=\"h3azul\">ITCA-App</h3>\r\n      <img slot=\"end\" class=\"img-logo ion-text-center\" src=\"../../assets/img/logo.png\" alt=\"\" />\r\n    </ion-toolbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row class=\"container-welcome\">\r\n    <h2>Comprobantes de Pago</h2>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-title\">\r\n    <ion-col size=\"12\">\r\n      <p>Fotografía del comprobante</p>\r\n    </ion-col>\r\n    <ion-col size=\"12\">\r\n      <div class=\"container-icon\" style=\"background-image: url({{invoice.img}});\">\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"file\" accept=\"image/*\" id=\"file-input\" #fileInp (change)=\"uploadImageTemporary($event)\">\r\n          </ion-input>\r\n          <button class=\"btn-subir\">\r\n            <ion-icon name=\"arrow-up-outline\"></ion-icon>\r\n          </button>\r\n        </ion-item>\r\n      </div>\r\n    </ion-col>\r\n    <div class=\"input-error ion-margin-top\" *ngIf=\"errors.img\">\r\n      <span>\r\n        <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.img}}\r\n      </span>\r\n    </div>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"container-card\">\r\n    <ion-card class=\"blue\">\r\n      <ion-card-content class=\"form\">\r\n        <p>Nombre del estudiante</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"invoice.studentName\" readonly=\"true\" value='{{user?.fullname}}'>{{user?.fullname}}</ion-input>\r\n        </ion-item>\r\n        <p>Carrera del estudiante</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"invoice.career\" readonly=\"true\" value='{{career?.name}}'>{{career?.name}}</ion-input>\r\n        </ion-item>\r\n        <p>Nivel del estudiante</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"invoice.level\" readonly=\"true\" value='{{level?.name}}'></ion-input>\r\n        </ion-item>\r\n        <p>Paralelo</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"invoice.parallel\" readonly=\"true\" value='{{user?.parallel}}'></ion-input>\r\n        </ion-item>\r\n        <p>Detalles del pago</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-textarea [(ngModel)]=\"invoice.detail\"></ion-textarea>\r\n        </ion-item>\r\n        <div class=\"input-error ion-margin-top\" *ngIf=\"errors.detail\">\r\n          <span>\r\n            <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.detail}}\r\n          </span>\r\n        </div>      \r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Institución Financiera\" [(ngModel)]=\"invoice.bank\" (ionChange)=\"onChangeBank($event)\">\r\n            <ion-select-option *ngFor=\"let item of banks\" value=\"{{item.name}}\"><span>{{item.name}}</span>\r\n            </ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n        <div *ngIf=\"selectedBank?.helpImg != null\">\r\n          <p>Imagen de Ayuda</p>\r\n          <ion-slides [options]=\"sliderOpts\" zoom>\r\n            <ion-slide>\r\n              <ion-item>\r\n                <div class=\"swiper-zoom-container\">\r\n                  <img src=\"{{selectedBank?.helpImg}}\" width=\"110%\" height=\"110%\">\r\n                </div>\r\n            </ion-item>\r\n            </ion-slide>\r\n          </ion-slides>\r\n        </div>\r\n        <div class=\"input-error ion-margin-top\" *ngIf=\"errors.bank\">\r\n          <span>\r\n            <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.bank}}\r\n          </span>\r\n          \r\n        </div>\r\n        <p>Número ÚNICO del Comprobante</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"text\" [(ngModel)]=\"invoice.identifynumber\"></ion-input>\r\n        </ion-item>\r\n        <div class=\"input-error ion-margin-top\" *ngIf=\"errors.identifynumber\">\r\n          <span>\r\n            <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.indentifynumber}}\r\n          </span>\r\n        </div>\r\n        \r\n        \r\n        \r\n        <p>Valor</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"number\" [(ngModel)]=\"invoice.value\"></ion-input>\r\n        </ion-item>\r\n        <div class=\"input-error ion-margin-top\" *ngIf=\"errors.value\">\r\n          <span>\r\n            <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.value}}\r\n          </span>\r\n        </div>\r\n\r\n        <ion-item lines=\"none\" class=\"categories\">\r\n          <ion-select placeholder=\"Motivo del Abono\" [(ngModel)]=\"invoice.reason\">\r\n            <div>\r\n              <ion-select-option> Arancel Enero</ion-select-option>\r\n              <ion-select-option> Arancel Febrero</ion-select-option>\r\n              <ion-select-option> Arancel Marzo</ion-select-option>\r\n              <ion-select-option> Arancel Abril</ion-select-option>\r\n              <ion-select-option> Arancel Mayo</ion-select-option>\r\n              <ion-select-option> Arancel Junio</ion-select-option>\r\n              <ion-select-option> Arancel Julio</ion-select-option>\r\n              <ion-select-option> Arancel Agosto</ion-select-option>\r\n              <ion-select-option> Arancel Septiembre</ion-select-option>\r\n              <ion-select-option> Arancel Octubre</ion-select-option>\r\n              <ion-select-option> Arancel Noviembre</ion-select-option>\r\n              <ion-select-option> Arancel Diciembre</ion-select-option>\r\n              <ion-select-option> Abonos/Otros</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n        <div class=\"input-error ion-margin-top\" *ngIf=\"errors.reason\">\r\n          <span>\r\n            <ion-icon name=\"alert-circle-outline\"></ion-icon> {{errors.reason}}\r\n          </span>\r\n        </div>\r\n        <p>Términos de Manejo de información y Finanzas</p>\r\n        <ion-item lines=\"none\">\r\n          <ion-textarea class=\"ion-text-justify\" readonly=\"true\">Yo <strong>{{user?.fullname}} </strong>en calidad de\r\n            estudiante del ITCA, certifico que todos los fondos del que proviene la cantidad entregada son de licitud\r\n            legal, la información que proporciono sobre el mismo no es falsificada ni clonada, acepto las condiciones\r\n            legales en las que me encontrase si hiciere mal uso de esta plataforma y su contenido. Autorizo a ITCA APP a\r\n            validar la veracidad de la información que proporciono.\r\n          </ion-textarea>\r\n        </ion-item>\r\n        <p>\r\n          <ion-item>\r\n            <ion-checkbox [(ngModel)]=\"isChecked\" color=\"light\" slot=\"start\"></ion-checkbox> <ion-label>Aceptar</ion-label>\r\n          </ion-item>          \r\n        </p>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </ion-row>\r\n  <ion-row class=\"container-btn margindown\">\r\n    <ion-button (click)=\"addInvoice()\"> Guardar Comprobante </ion-button>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "3mc2":
/*!*******************************************!*\
  !*** ./src/app/services/banks.service.ts ***!
  \*******************************************/
/*! exports provided: BanksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksService", function() { return BanksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let BanksService = class BanksService {
    constructor(firebase) {
        this.firebase = firebase;
    }
    addBank(bank) {
        bank.createdAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').add(bank);
    }
    updateBank(bank) {
        bank.updatedAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firebase.collection('/bank').doc(bank.uid).update(bank);
    }
    deleteBank(uid) {
        this.firebase.doc(`/bank/${uid}`).delete();
    }
    getBankByUid(uid) {
        const ref = this.firebase
            .collection('/bank/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getBanks() {
        const ref = this.firebase
            .collection('bank')
            .valueChanges({ idField: 'uid' });
        return ref;
    }
};
BanksService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
BanksService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BanksService);



/***/ }),

/***/ "5CEN":
/*!***********************************************!*\
  !*** ./src/app/upinvoice/upinvoice.module.ts ***!
  \***********************************************/
/*! exports provided: UpinvoicePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpinvoicePageModule", function() { return UpinvoicePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _upinvoice_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./upinvoice-routing.module */ "PdWX");
/* harmony import */ var _upinvoice_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./upinvoice.page */ "qJlG");







let UpinvoicePageModule = class UpinvoicePageModule {
};
UpinvoicePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _upinvoice_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpinvoicePageRoutingModule"]
        ],
        declarations: [_upinvoice_page__WEBPACK_IMPORTED_MODULE_6__["UpinvoicePage"]]
    })
], UpinvoicePageModule);



/***/ }),

/***/ "9xfe":
/*!***********************************************!*\
  !*** ./src/app/upinvoice/upinvoice.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  height: 100%;\n  background-image: url('background2.png');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.h3azul {\n  color: #014898;\n}\n\n/* Estilos del header  */\n\nion-toolbar {\n  --padding-start:6%;\n  --padding-end:6%;\n  --padding-top:6%;\n  margin-bottom: 6%;\n}\n\nion-toolbar .img-logo {\n  width: 51px;\n  height: 51px;\n}\n\nion-toolbar .container-img-header {\n  display: flex;\n  justify-content: center;\n  width: 89%;\n}\n\n.btn-subir {\n  background-color: #94c9d4;\n  color: white;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 45px;\n  height: 45px;\n  border-radius: 100%;\n  padding: 5%;\n  font-size: 25px;\n}\n\n/* Estilos del mensaje de bienvenido */\n\n.container-welcome {\n  flex-direction: column;\n  padding: 0px 6%;\n}\n\n.container-welcome p {\n  margin: 0px;\n  font-size: 20px;\n  font-weight: 500;\n}\n\n.container-welcome h1 {\n  font-weight: 700;\n  font-size: 33px;\n}\n\n/* Estilos del titulo de la pagina */\n\n.container-title {\n  padding: 0px 6%;\n  margin-top: 10%;\n}\n\n.container-title p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n/* Estilos del icono de subir categoria */\n\n.container-icon {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  align-items: center;\n  border: 2px solid #979797;\n  width: 100%;\n  height: 200px;\n  border-radius: 21px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n}\n\n.container-icon ion-item {\n  --background:rgba(0,0,0,0);\n}\n\n.container-icon .circle {\n  width: 51px;\n  height: 51px;\n  background: #CBD8DC;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n#file-input {\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  z-index: 999;\n}\n\n/* Estilos de la tarjeta de los inputs */\n\n.container-card {\n  display: flex;\n  justify-content: center;\n}\n\n.container-card ion-card {\n  width: 90%;\n  box-shadow: none;\n  border-radius: 15px;\n}\n\n.container-card ion-card ion-card-content {\n  padding: 20px;\n  padding-top: 10px;\n}\n\n.container-card ion-card ion-card-content ion-select {\n  width: 100%;\n  justify-content: center;\n}\n\n.container-card ion-card ion-card-content p {\n  font-weight: bold;\n  color: black;\n}\n\n.container-card ion-card ion-card-content ion-item {\n  --border-radius:10px;\n}\n\n.container-card ion-card ion-card-content .categories {\n  margin-top: 5%;\n}\n\n.container-card ion-card ion-card-content .categories ion-select {\n  --placeholder-opacity:1;\n}\n\n.select-black {\n  --background: #161616;\n  color: white;\n}\n\n.container-link {\n  padding: 0px 6%;\n  display: flex;\n  justify-content: space-between;\n}\n\n.container-link p {\n  font-weight: bold;\n  font-size: 17px;\n}\n\n.container-link .circle {\n  width: 40px;\n  height: 40px;\n  background: #333333;\n  color: white;\n  border-radius: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.blue {\n  background-color: #DDF2FB;\n}\n\n.green {\n  background-color: #E8F8E5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHVwaW52b2ljZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxZQUFBO0VBQ0Esd0NBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBQUo7O0FBRUE7RUFDTSxjQUFBO0FBQ047O0FBQ0Esd0JBQUE7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUVKOztBQUFJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFFUjs7QUFBSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7QUFFUjs7QUFFQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUEsc0NBQUE7O0FBQ0E7RUFDSSxzQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFBSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFFUjs7QUFBSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUVBLG9DQUFBOztBQUNBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFBSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUVSOztBQUdBLHlDQUFBOztBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtBQUFKOztBQUNJO0VBQ0ksMEJBQUE7QUFDUjs7QUFDSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQ1I7O0FBSUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQURKOztBQU1BLHdDQUFBOztBQUNBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBSEo7O0FBSUk7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUZSOztBQUdRO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0FBRFo7O0FBRVk7RUFDSSxXQUFBO0VBQ0EsdUJBQUE7QUFBaEI7O0FBRWM7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUFBbEI7O0FBRWM7RUFDRSxvQkFBQTtBQUFoQjs7QUFJYztFQUNJLGNBQUE7QUFGbEI7O0FBR2tCO0VBQ0UsdUJBQUE7QUFEcEI7O0FBU0E7RUFDSSxxQkFBQTtFQUNBLFlBQUE7QUFOSjs7QUFTQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7QUFOSjs7QUFPSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQUxSOztBQU9JO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBTFI7O0FBV0E7RUFDSSx5QkFBQTtBQVJKOztBQVdBO0VBQ0kseUJBQUE7QUFSSiIsImZpbGUiOiJ1cGludm9pY2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy9mb25kbyBkZSBsYSBpbWFnZW4gZGVsIGNvbnRhaW5lclxyXG4uY29udGFpbmVye1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIucG5nXCIpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgfVxyXG4uaDNhenVse1xyXG4gICAgICBjb2xvcjojMDE0ODk4O1xyXG4gIH1cclxuLyogRXN0aWxvcyBkZWwgaGVhZGVyICAqL1xyXG5pb24tdG9vbGJhcntcclxuICAgIC0tcGFkZGluZy1zdGFydDo2JTtcclxuICAgIC0tcGFkZGluZy1lbmQ6NiU7XHJcbiAgICAtLXBhZGRpbmctdG9wOjYlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNiU7XHJcblxyXG4gICAgLmltZy1sb2dve1xyXG4gICAgICAgIHdpZHRoOjUxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lci1pbWctaGVhZGVye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6ODklO1xyXG4gICAgfVxyXG59XHJcbi8vRXN0aWxvcyBkZSBib3RvbiBzdWJpciBpbWdcclxuLmJ0bi1zdWJpcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6Izk0YzlkNDtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiA0NXB4O1xyXG4gICAgaGVpZ2h0OiA0NXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgIHBhZGRpbmc6IDUlO1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgfVxyXG5cclxuLyogRXN0aWxvcyBkZWwgbWVuc2FqZSBkZSBiaWVudmVuaWRvICovXHJcbi5jb250YWluZXItd2VsY29tZXtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBwe1xyXG4gICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgICAgICBmb250LXNpemU6IDMzcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qIEVzdGlsb3MgZGVsIHRpdHVsbyBkZSBsYSBwYWdpbmEgKi9cclxuLmNvbnRhaW5lci10aXRsZXtcclxuICAgIHBhZGRpbmc6IDBweCA2JTtcclxuICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgIHB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLyogRXN0aWxvcyBkZWwgaWNvbm8gZGUgc3ViaXIgY2F0ZWdvcmlhICovXHJcbi5jb250YWluZXItaWNvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgIzk3OTc5NztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAyMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGlvbi1pdGVte1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDpyZ2JhKDAsMCwwLDApO1xyXG4gICAgfVxyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0NCRDhEQztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuICAgXHJcbn1cclxuXHJcbiNmaWxlLWlucHV0IHtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB6LWluZGV4OiA5OTk7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4vKiBFc3RpbG9zIGRlIGxhIHRhcmpldGEgZGUgbG9zIGlucHV0cyAqL1xyXG4uY29udGFpbmVyLWNhcmR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBpb24tY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgICAgICAgICAgaW9uLXNlbGVjdCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpb24taXRlbXtcclxuICAgICAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czoxMHB4O1xyXG4gICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIC5jYXRlZ29yaWVze1xyXG4gICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1JTtcclxuICAgICAgICAgICAgICAgICAgaW9uLXNlbGVjdHtcclxuICAgICAgICAgICAgICAgICAgICAtLXBsYWNlaG9sZGVyLW9wYWNpdHk6MTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uc2VsZWN0LWJsYWNre1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMTYxNjE2O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uY29udGFpbmVyLWxpbmt7XHJcbiAgICBwYWRkaW5nOiAwcHggNiU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgcHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcbiAgICAuY2lyY2xle1xyXG4gICAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMzMzMzMzO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi5ibHVle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojRERGMkZCOyBcclxufVxyXG5cclxuLmdyZWVue1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojRThGOEU1OyBcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "PdWX":
/*!*******************************************************!*\
  !*** ./src/app/upinvoice/upinvoice-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: UpinvoicePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpinvoicePageRoutingModule", function() { return UpinvoicePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _upinvoice_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./upinvoice.page */ "qJlG");




const routes = [
    {
        path: '',
        component: _upinvoice_page__WEBPACK_IMPORTED_MODULE_3__["UpinvoicePage"]
    }
];
let UpinvoicePageRoutingModule = class UpinvoicePageRoutingModule {
};
UpinvoicePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpinvoicePageRoutingModule);



/***/ }),

/***/ "b8cs":
/*!***********************************************!*\
  !*** ./src/app/services/upinvoice.service.ts ***!
  \***********************************************/
/*! exports provided: UpinvoiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpinvoiceService", function() { return UpinvoiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/app */ "Jgta");




let UpinvoiceService = class UpinvoiceService {
    constructor(firestore) {
        this.firestore = firestore;
    }
    addInvoice(invoice) {
        invoice.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        invoice.createAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        invoice.date = invoice.createAt.toDateString();
        this.firestore.collection('/invoice').add(invoice);
    }
    updateInvoice(invoice) {
        invoice.updateAt = firebase_app__WEBPACK_IMPORTED_MODULE_3__["default"].firestore.Timestamp.now().toDate();
        this.firestore.collection('/invoice').doc(invoice.uid).update(invoice);
    }
    deleteInvoice(uid) {
        this.firestore.doc(`/invoice/${uid}`).delete();
    }
    getInvoiceByUser(uid) {
        const ref = this.firestore
            .collection('/invoice/', (ref) => ref.where('useruid', '==', uid))
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getInvoiceByUid(uid) {
        const ref = this.firestore
            .collection('/invoice/').doc(uid)
            .valueChanges({ idField: 'uid' });
        return ref;
    }
    getInvoicesPendding() {
        return this.firestore
            .collection('/invoice', (ref) => ref.where('resolution', '==', 'registered'))
            .valueChanges({ idField: 'uid' });
    }
    getAllInvoices() {
        return this.firestore
            .collection('invoice')
            .valueChanges({ idField: 'uid' });
    }
};
UpinvoiceService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
];
UpinvoiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UpinvoiceService);



/***/ }),

/***/ "qJlG":
/*!*********************************************!*\
  !*** ./src/app/upinvoice/upinvoice.page.ts ***!
  \*********************************************/
/*! exports provided: UpinvoicePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpinvoicePage", function() { return UpinvoicePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_upinvoice_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./upinvoice.page.html */ "04LR");
/* harmony import */ var _upinvoice_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./upinvoice.page.scss */ "9xfe");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_img_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/img.service */ "t3Fp");
/* harmony import */ var _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/firebase-auth-service.service */ "1x2Z");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/user.service */ "qfBg");
/* harmony import */ var _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/upinvoice.service */ "b8cs");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/alert.service */ "3LUQ");
/* harmony import */ var _services_career_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/career.service */ "Uvke");
/* harmony import */ var _shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/utilities/formValidation */ "ejrE");
/* harmony import */ var _services_banks_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/banks.service */ "3mc2");












let UpinvoicePage = class UpinvoicePage {
    constructor(imgService, authService, userService, upinvoiceService, alertService, careerService, bankService) {
        this.imgService = imgService;
        this.authService = authService;
        this.userService = userService;
        this.upinvoiceService = upinvoiceService;
        this.alertService = alertService;
        this.careerService = careerService;
        this.bankService = bankService;
        this.sliderOpts = {
            zoom: {
                maxRatio: 2
            }
        };
        this.invoice = {};
        this.bank = {};
        this.validationRules = {
            required: ['identifynumber', 'bank', 'img', 'reason', 'detail', 'value'],
        };
        this.errors = {
            identifynumber: null,
            bank: null,
            img: null,
            reason: null,
            detail: null,
            value: null,
        };
        this.imgFile = null;
        this.verifyNumber = true;
    }
    ngOnInit() {
        this.cleanInvoice();
        this.getUserId();
        this.getUserByUid();
        this.getInvoices();
        this.getBanks();
    }
    cleanInvoice() {
        this.invoice = {
            useruid: '',
            identifynumber: '',
            bank: '',
            career: '',
            studentName: '',
            level: '',
            img: '',
            reason: '',
            detail: '',
            value: '',
            resolution: 'registered',
            createAt: null,
            updateAt: null,
        };
    }
    loadCareer(careerUid) {
        this.careerService.getCareerByUid(careerUid).subscribe((data) => {
            this.career = data;
            this.level = this.career.levels.find((e) => (e.uid === this.user.level));
        });
    }
    getUserId() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userSession = yield this.authService.getCurrentUser();
            return userSession.uid;
        });
    }
    getUserByUid() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userUid = yield this.getUserId();
            this.userService.getUserById(userUid).subscribe((res) => {
                this.user = res;
                this.loadCareer(this.user.career);
            });
        });
    }
    getInvoices() {
        this.upinvoiceService.getAllInvoices().subscribe(res => {
            this.invoices = res;
            this.checkPaymentNumber();
        });
    }
    getBanks() {
        this.bankService.getBanks().subscribe(res => {
            this.banks = res;
        });
    }
    checkPaymentNumber() {
        this.invoices.map((res) => {
            if (res.identifynumber === this.invoice.identifynumber) {
                this.verifyNumber = false;
            }
        });
    }
    onChangeBank({ detail }) {
        const auxBank = this.banks.find((b) => (b.name === detail.value));
        this.selectedBank = auxBank;
    }
    addInvoice() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.checkPaymentNumber();
            if (this.verifyNumber) {
                if (this.isChecked) {
                    if (!this.validaForm()) {
                        return;
                    }
                    {
                        this.invoice.img = yield this.imgService.uploadImage('invoice', this.imgFile);
                        this.invoice.useruid = yield this.getUserId();
                        this.upinvoiceService.addInvoice(this.invoice);
                        this.cleanInvoice();
                        this.alertService.presentAlert("Guardado correctamente");
                    }
                }
                else {
                    this.alertService.presentAlert('Acepte los términos de manejo de información y finanzas');
                }
            }
            else {
                this.alertService.presentAlert('Este número de comprobante ya fue registrado, intente con otros Datos');
            }
            this.verifyNumber = true;
        });
    }
    uploadImageTemporary($event) {
        this.imgService.uploadImgTemporary($event).onload = (event) => {
            this.invoice.img = event.target.result;
            this.imgFile = $event.target.files[0];
        };
    }
    validaForm() {
        const errors = Object(_shared_utilities_formValidation__WEBPACK_IMPORTED_MODULE_10__["validateForm"])(Object.assign(Object.assign({}, this.invoice), { identifynumber: this.invoice.identifynumber }), this.validationRules);
        this.errors = errors;
        const validForm = Object.keys(errors).length;
        return !validForm;
    }
};
UpinvoicePage.ctorParameters = () => [
    { type: _services_img_service__WEBPACK_IMPORTED_MODULE_4__["ImgService"] },
    { type: _services_firebase_auth_service_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseAuthServiceService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _services_upinvoice_service__WEBPACK_IMPORTED_MODULE_7__["UpinvoiceService"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"] },
    { type: _services_career_service__WEBPACK_IMPORTED_MODULE_9__["CareerService"] },
    { type: _services_banks_service__WEBPACK_IMPORTED_MODULE_11__["BanksService"] }
];
UpinvoicePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-upinvoice',
        template: _raw_loader_upinvoice_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_upinvoice_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpinvoicePage);



/***/ })

}]);
//# sourceMappingURL=upinvoice-upinvoice-module.js.map