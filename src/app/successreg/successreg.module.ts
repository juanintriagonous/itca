import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessregPageRoutingModule } from './successreg-routing.module';

import { SuccessregPage } from './successreg.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessregPageRoutingModule
  ],
  declarations: [SuccessregPage]
})
export class SuccessregPageModule {}
