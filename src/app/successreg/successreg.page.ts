import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-successreg',
  templateUrl: './successreg.page.html',
  styleUrls: ['./successreg.page.scss'],
})
export class SuccessregPage implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }
  back() {
    this.router.navigate(['/main']);
  }

}
