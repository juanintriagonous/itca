import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessregPage } from './successreg.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessregPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessregPageRoutingModule {}
