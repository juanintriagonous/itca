import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';

import firebase from 'firebase/app';
import { PushService } from './services/push.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {
  login: Boolean;

  constructor(
    private menu: MenuController, private PushService: PushService
  ) {
    this.PushService.configuracionInicial();
    this.login = false;
    this.seccion();
  }

  toggleMenu() {
    this.menu.toggle(); //Add this method to your button click function

  }
  seccion() {
    firebase.auth().onAuthStateChanged((user: firebase.User) => {
      if (user) {
        this.login = true;
     

      } else {
        this.login = false;
        console.log('El usuario no está conectado' );

      }
    });
  }
  logoutUser(): Promise<void> {
    this.menu.toggle(); //Add this method to your button click function
    return firebase.auth().signOut();
  }
}
