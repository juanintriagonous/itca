import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListInvoiceRejectedPage } from './list-invoice-rejected.page';

describe('ListInvoiceRejectedPage', () => {
  let component: ListInvoiceRejectedPage;
  let fixture: ComponentFixture<ListInvoiceRejectedPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListInvoiceRejectedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListInvoiceRejectedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
