import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListInvoiceRejectedPageRoutingModule } from './list-invoice-rejected-routing.module';

import { ListInvoiceRejectedPage } from './list-invoice-rejected.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListInvoiceRejectedPageRoutingModule
  ],
  declarations: [ListInvoiceRejectedPage]
})
export class ListInvoiceRejectedPageModule {}
