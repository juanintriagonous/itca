import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListInvoiceRejectedPage } from './list-invoice-rejected.page';

const routes: Routes = [
  {
    path: '',
    component: ListInvoiceRejectedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListInvoiceRejectedPageRoutingModule {}
