import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.page.html',
  styleUrls: ['./notice.page.scss'],
})
export class NoticePage implements OnInit {
  id: any;

  notification = {
    uid: '',
    name: '',
    description: '',
    hyper: '',
    state: '',
    order: '',
    updateDate: '',
    category: '',
    img: '',
  };

  publicado: any;
  trasformar: any;
  constructor(
    private rutaActiva: ActivatedRoute,
    private firestore: AngularFirestore
  ) {
    this.id = this.rutaActiva.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    const ref = this.firestore.collection('/notification/').doc(this.id);
    ref.snapshotChanges().subscribe((res) => {
      if (res) {
        this.notification.uid = res.payload.id;
        this.notification.name = res.payload.data()['name'];
        this.notification.description = res.payload.data()['description'];
        this.notification.state = res.payload.data()['state'];
        this.notification.hyper = res.payload.data()['hyper'];
        this.notification.order = res.payload.data()['order'];
        this.notification.updateDate = this.tiempo(res.payload.data()["updateDate"]),
        this.notification.category = res.payload.data()['category'];
        this.notification.img = res.payload.data()['img'];
      }
      console.log(this.notification.hyper);
    });
  }
  /* Metodo que me sirve para calcular el tiempo de publicación de cada noticia */
  tiempo(time: any) {
    //  console.log(Date.now() + "");
    let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
    this.publicado = '';
    this.trasformar = 0;
    //Segundos
    if (tiempo > 0 && tiempo < 60) {
      this.trasformar = tiempo;
      this.publicado = this.trasformar + ' Seg';
    }
    //minustos
    else if (tiempo > 60 && tiempo < 3600) {
      this.trasformar = (tiempo / 60).toFixed(0);
      this.publicado = this.trasformar + ' Min';
    }
    //horas
    else if (tiempo > 3600 && tiempo < 86400) {
      this.trasformar = (tiempo / 3600).toFixed(0);
      this.publicado = this.trasformar + ' Horas';
    }
    //Dias
    else if (tiempo > 86400 && tiempo < 604800) {
      this.trasformar = (tiempo / 86400).toFixed(0);
      this.publicado = this.trasformar + ' Dias';
    } else if (tiempo > 604800 && tiempo < 2629743) {
      this.trasformar = (tiempo / 604800).toFixed(0);
      this.publicado = this.trasformar + ' Semanas';
    }
    return this.publicado;
  }
  ventana(valor:any){
    window.open(valor, '_system');
}
}
