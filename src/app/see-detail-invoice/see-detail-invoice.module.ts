import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeDetailInvoicePageRoutingModule } from './see-detail-invoice-routing.module';

import { SeeDetailInvoicePage } from './see-detail-invoice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeDetailInvoicePageRoutingModule
  ],
  declarations: [SeeDetailInvoicePage]
})
export class SeeDetailInvoicePageModule {}
