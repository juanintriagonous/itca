import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeDetailInvoicePage } from './see-detail-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: SeeDetailInvoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeDetailInvoicePageRoutingModule {}
