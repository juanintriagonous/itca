import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { bank } from '../interfaces/bank.interface';
import { invoice } from '../interfaces/invoice.interface';
import { User } from '../interfaces/user.interface';
import { BanksService } from '../services/banks.service';
import { UpinvoiceService } from '../services/upinvoice.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-see-detail-invoice',
  templateUrl: './see-detail-invoice.page.html',
  styleUrls: ['./see-detail-invoice.page.scss'],
})
export class SeeDetailInvoicePage implements OnInit {

  sliderOpts = {
    zoom: {
      maxRatio: 2
    }
  };

  invoiceUid: string
  invoice: invoice
  bank: bank
  user: User
  resolutionAux: string;

  constructor(
    private activateRoute: ActivatedRoute,
    private upinvoiceService: UpinvoiceService,
    private userService: UserService,
    private bankService: BanksService) {
    this.invoiceUid = this.activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getInvoiceByUid();
    this.getCurrentUser;
  }

  getInvoiceByUid() {
    this.upinvoiceService.getInvoiceByUid(this.invoiceUid).subscribe(res => {
      this.invoice = res as invoice
      this.getbankByUid(this.invoice.bank)
      this.resolutionAux = this.invoice.resolution
    })
  }

  getCurrentUser(){
    this.userService.getUserById(this.invoice.useruid).subscribe(res1 => {
      this.user = res1 as User
    })
  }

  getbankByUid(bankUid) {
    this.bankService.getBankByUid(bankUid).subscribe(res => {
      this.bank = res as bank
    })
  }
  updateBank(){
    this.invoice.resolution = this.resolutionAux
    this.upinvoiceService.updateInvoice(this.invoice)
  }

}
