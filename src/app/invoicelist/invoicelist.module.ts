import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvoicelistPageRoutingModule } from './invoicelist-routing.module';

import { InvoicelistPage } from './invoicelist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvoicelistPageRoutingModule
  ],
  declarations: [InvoicelistPage]
})
export class InvoicelistPageModule {}
