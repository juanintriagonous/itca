import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoicelistPage } from './invoicelist.page';

const routes: Routes = [
  {
    path: '',
    component: InvoicelistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoicelistPageRoutingModule {}
