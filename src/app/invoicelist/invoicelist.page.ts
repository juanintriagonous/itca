import { Component, OnInit } from '@angular/core';
import { invoice } from '../interfaces/invoice.interface';
import { UpinvoiceService } from '../services/upinvoice.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { Router } from '@angular/router';
import { BanksService } from '../services/banks.service';
import { bank } from '../interfaces/bank.interface';

@Component({
  selector: 'app-invoicelist',
  templateUrl: './invoicelist.page.html',
  styleUrls: ['./invoicelist.page.scss'],
})
export class InvoicelistPage implements OnInit {

  invoices:invoice[]
  bank: bank

  constructor( 
    private upinvoiceService: UpinvoiceService,
    private authService: FirebaseAuthServiceService, 
    private router:Router,
    private bankService: BanksService ) {
    
   }

  ngOnInit() {
    this.getUpInvoiceService();
  }

  async getUserId() {
    const userSession = await this.authService.getCurrentUser();
    return userSession.uid;
  }

  async getUpInvoiceService(){
    const userUid = await this.getUserId();
    console.log(userUid)
    this.upinvoiceService.getInvoiceByUser(userUid).subscribe(res=>{
      this.invoices =  res as invoice[]
      this.invoices = this.invoices.map(c=>(
        this.getbankById(c)
      ));
    })
    
    
  }
  goToSeeVoucher(invoice:invoice){
    this.router.navigate(['see-detail-invoice/',invoice.uid])
  }
  getbankById(invoice: invoice): invoice{
    this.bankService.getBankByUid(invoice.bank).subscribe(res =>{
      invoice.bankData = res as bank
    })
    return invoice;
  }

}
