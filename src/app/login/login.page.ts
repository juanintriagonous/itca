import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { listErrors } from '../shared/Errors/listErrors';
import { validateForm } from '../shared/utilities/formValidation';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  selectedAction = 'login';
  passwordType = 'password';
  eyeTypeIcon = 'eye';
  user = {
    email: '',
    password: '',
  };
  validationrules = {
    required: ['email', 'password'],
    email: ['email'],
  /*   emailitca: ['email'], */
  };
  errors: any = {
    email: null,
    password: null,
  };
  constructor(
    private router: Router,
    private authService: FirebaseAuthServiceService,
    private alertService: AlertService
  ) { }

  ngOnInit() {}
  showHiddePassword(){
    this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off': 'eye';
    this.passwordType = this.passwordType === 'text' ? 'password': 'text';
  }

  async loginUser() {
    if (!this.validForm()) {
      return;
    }
    await this.alertService.presentLoading('Espere un momento...');
    try {
      const data = await this.authService.signIn(this.user.email, this.user.password);
      this.alertService.loading.dismiss();
      this.goToHome();
      if (!data.user.emailVerified) {
        this.alertService.presentAlert(
          'Su cuenta no ha sido verificada. Por favor, revise su correo electrónico'
        );
        this.authService.logoutUser();
        this.alertService.loading.dismiss();
      }
    } catch (e) {
      this.alertService.loading.dismiss();
      this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
    }
  }

  validForm() {
    const errors = validateForm(this.user, this.validationrules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }

  selectedActionChange(event) {
    if (event.target.value !== 'login') {
      this.selectedAction = 'login';
      this.router.navigate(['/register']);
    }
  }

  goToHome() {
    this.router.navigate(['/main']);
  }
}
