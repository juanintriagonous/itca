export const ccValidator = (value: string) => {
    let newValue = value;
    // accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(newValue)) {
      return false;
    }
  
    //  The Luhn Algorithm. It's so pretty.
    let nCheck = 0;
    let nDigit = 0;
    let bEven = false;
    newValue = newValue.replace(/\D/g, '');
  
    for (let n = newValue.length - 1; n >= 0; n--) {
      const cDigit = newValue.charAt(n);
      nDigit = parseInt(cDigit, 10);
  
      if (bEven) {
        if ((nDigit *= 2) > 9) {
          nDigit -= 9;
        }
      }
  
      nCheck += nDigit;
      bEven = !bEven;
    }
  
    return nCheck % 10 === 0;
  };