export const listErrors = {
    'auth/user-not-found': `No hay registro de usuario correspondiente a este identificador. El usuario puede haber sido eliminado`,
    'auth/invalid-email:': `La dirección de correo electrónico está mal formateada`,
    'auth/wrong-password': `Credenciales incorrectas`,
    'auth/too-many-requests': `Demasiados intentos de inicio de sesión fallidos. Por favor, inténtelo de nuevo más tarde`,
    'app/general': `Ha ocurrido un error intentelo de nuevo por favor`,
    'auth/email-already-in-use':
      'La dirección de correo electrónico ya está siendo utilizada por otra cuenta.',
    'failed-precondition': 'La consulta requiere un índice.',
  };