import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { career } from '../interfaces/career';
import { levels } from '../interfaces/levels';
import { parallel } from '../interfaces/parallel.interface';
import { User } from '../interfaces/user.interface';
import { AlertService } from '../services/alert.service';
import { CareerService } from '../services/career.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { listErrors } from '../shared/Errors/listErrors';
import { validateForm } from '../shared/utilities/formValidation';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  selectedAction = 'register';
  passwordType = 'password';
  eyeTypeIcon = 'eye';

  user: User = {
    email: '',
    role: 'student',
    fullname: '',
    identificationDocument: '',
    gender: '',
    career: null,
    parallel: null,
    image: '',
    level: null,
    isActive: true,
  };

  career: career = {
    uid: '',
    name: '',
    description: '',
    tittleType: '',
    isActivate: '',
    createdAt: null,
    updatedAt: null,
  };
  password = '';

  validationRules = {
    required: ['email', 'identificationDocument', 'role', 'fullname', 'password', 'gender', 'career', 'level'],
    email: ['email'],
    emailitca: ['email'],
    document: [{ type: 'CI', field: 'identificationDocument' }],
    noNumber: ['fullname'],
    length: [
      { field: 'fullname', min: 10, max: 60 },
      { field: 'password', min: 8, max: 45 },
      //{ field: 'phoneNumber', min: 10, max: 10 },
      { field: 'identificationDocument', min: 10, max: 10 },
    ],
  };
  errors: any = {
    email: null,
    role: null,
    fullname: null,
    emailitca: null,
    password: null,
    //phoneNumber: null,
    gender: null,
    career: null,
    identificationDocument: null,
    level: null
  };
  selectedCareer: career;
  selectedLevel: levels;
  levels: levels[];
  parallels: parallel[];
  careers: career[];

  constructor(
    private router: Router,
    private authService: FirebaseAuthServiceService,
    private alertService: AlertService,
    private careerService: CareerService
  ) { }
  ngOnInit() {
    this.getCareer();
  }

  showHiddePassword() {
    this.eyeTypeIcon = this.eyeTypeIcon === 'eye' ? 'eye-off' : 'eye';
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
  }


  async registerUser() {
    if (!this.validaForm()) {
      return;
    }
    await this.alertService.presentLoading('Creando cuenta...');
    try {
      await this.authService.signUp(this.user, this.password, this.user.identificationDocument, this.user.phoneNumber, this.user.gender, this.user.career, this.user.level);
      this.alertService.loading.dismiss();
      await this.alertService.presentAlert(
        'Su cuenta ha sido creada con éxito. Revise con correo para verificarla. 👻'
      );
      this.router.navigate(['/login']);
    } catch (e) {
      this.alertService.loading.dismiss();
      await this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
    }
  }

  onChangeCarrer({ detail }) {
    const auxCareer = this.careers.find((c) => (
      c.uid === detail.value
    ))
    this.selectedCareer = auxCareer;
    this.levels = auxCareer.levels.filter((c) => (
      c.isActivate === true
    ));
    this.parallels = auxCareer.parallel.filter((c) =>(
      c.isActivate === true
    ));
  }

  

  validaForm() {
    const errors = validateForm({ ...this.user, password: this.password }, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }

  selectedActionChange(event) {
    if (event.target.value !== 'register') {
      this.selectedAction = 'register';
      this.router.navigate(['/login']);
    }
  }

  getCareer() {
    this.careerService.getCareers().subscribe(res => {
      this.careers = res as career[];
    })
  }


}
