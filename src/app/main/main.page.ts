import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  lessdescription: string; 
  Categories: {
    id: string;
    name: string;
    description: string;
    state: string;
  }[];

  priorities: {
    id: string;
    name: string;
    state: string;
  }[];

  notification: {
    id: string;
    name: string;
    description: string;
    creationDate: string,
    updateDate: string,
    img: string;
    state: string;
  }[];


  lastFivenotification: {
    id: string;
    name: string;
    description: string;
    creationDate: string,
    updateDate: string,
    img: string;
    state: string;
  }[];
  publicado: any;
  trasformar: any;

  /* Objeto para configurar el slide de las categorias */
  categoryConfig = {
    spaceBetween: 0,
    slidesPerView: 2.5,
  };
  /* Obejeto para configurar el slide de los banners   */
  bannerConfig = {
    spaceBetween: 10,
    slidesPerView: 1,
    autoplay: true,
  };
  /* Variable para varia el tipo del ion-segment */
  type: string;

  //Se declara un atributo tipo string para almacenar el titulo que se va a presentar en el home.
  title: string = '';

  constructor(private firebase: AngularFirestore) {
    this.type = 'latest';
  }

  ngOnInit() {
    this.categoryQuery();
    this.lastFiveNewsQuery();
  }

  //Metodo para consultar las empresas dependiendo de los que escoja el usuario
  newsQuery(title: string) {
    this.title = title;
    this.firebase
      .collection('/notification/', (ref) =>
        ref.where('category', '==', this.title)
          .orderBy("order", "asc")          
      )
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.notification = res.map((e) => {
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              description: e.payload.doc.data()['description'],
              creationDate: e.payload.doc.data()['creationDate'],
              order: e.payload.doc.data()['order'],
              priority: e.payload.doc.data()['priority'],
              updateDate: this.tiempo(e.payload.doc.data()["updateDate"]),
              img: e.payload.doc.data()['img'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });

  }

  substr(text) {
    let textResult: string;
    textResult = text.substring(0, 100);
    return textResult;
  }

  /* Metodo para consultar las categorias */
  categoryQuery() {
    // const notificationRef = this.firebase.collection('/notification/', (ref) =>
    //ref.orderBy("updateDate","desc").limit(5))
    this.firebase
      .collection('/category', (ref) => ref.where('state', '==', 'public').orderBy("order", "asc"))
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.Categories = res.map((e, index) => {
            if (index === 0) {
              this.newsQuery(e.payload.doc.data()['name'])
            }
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              description: e.payload.doc.data()['description'],
              order: e.payload.doc.data()['order'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });
  }

  /* Metodo para consultar las las prioridades en base de datos */
  priorityQuery() {
    // const notificationRef = this.firebase.collection('/notification/', (ref) =>
    //ref.orderBy("updateDate","desc").limit(5))
    this.firebase
      .collection('/priority', (ref) => ref.where('state', '==', 'public').orderBy("name", "asc"))
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.priorities = res.map((e, index) => {
            if (index === 0) {
              this.newsQuery(e.payload.doc.data()['name'])
            }
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });
  }

  /* Metodo que me sirve para calcular el tiempo de publicación de cada noticia */
  tiempo(time: any) {
    let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
    this.publicado = '';
    this.trasformar = 0;
    //Segundos
    if (tiempo > 0 && tiempo < 60) {
      this.trasformar = tiempo;
      this.publicado = this.trasformar + " Seg";
    }
    //minustos 
    else if (tiempo > 60 && tiempo < 3600) {
      this.trasformar = (tiempo / 60).toFixed(0);
      this.publicado = this.trasformar + " Min";
    }
    //horas 
    else if (tiempo > 3600 && tiempo < 86400) {
      this.trasformar = (tiempo / 3600).toFixed(0);
      this.publicado = this.trasformar + " Horas";
    }
    //Dias 
    else if (tiempo > 86400 && tiempo < 604800) {
      this.trasformar = (tiempo / 86400).toFixed(0);
      this.publicado = this.trasformar + " Dias";
    }
    else if (tiempo > 604800 && tiempo < 2629743) {
      this.trasformar = (tiempo / 604800).toFixed(0);
      this.publicado = this.trasformar + " Semanas";
    }
    return this.publicado;
  }

  /* Metodo para consultar las ultimas 5 noticias publicadas */
  lastFiveNewsQuery() {

    const notificationRef = this.firebase.collection('/notification/', (ref) =>
      ref.orderBy("priority", "asc").limit(5))

    notificationRef
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.lastFivenotification = res.map((e) => {

            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              description: e.payload.doc.data()['description'],
              creationDate: e.payload.doc.data()['creationDate'],
              updateDate: this.tiempo(e.payload.doc.data()["updateDate"]),
              img: e.payload.doc.data()['img'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });

  }

}
