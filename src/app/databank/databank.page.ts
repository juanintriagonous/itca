import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { UpdatebankComponent } from '../components/updatebank/updatebank.component';
import { bank } from '../interfaces/bank.interface';
import { User } from '../interfaces/user.interface';
import { AlertService } from '../services/alert.service';
import { BanksService } from '../services/banks.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { ImgService } from '../services/img.service';
import { UserService } from '../services/user.service';
import { listErrors } from '../shared/Errors/listErrors';
import { validateForm } from '../shared/utilities/formValidation';

@Component({
  selector: 'app-databank',
  templateUrl: './databank.page.html',
  styleUrls: ['./databank.page.scss'],
})

export class DatabankPage implements OnInit {

  bank: bank = {
    uid: '',
    name: '',
    accountNumber: '',
    helpImg: '',
    isActivate: '',
  };

  career: {
    id: string,
    name: string,
    description: string,
    levels: string,
    tittleType: string
  }[];

  banks: bank[];


  imgFile: File = null;

  user: User;

  validationRules = {
    required: ['name', 'accountNumber', 'helpImg', 'isActivate'],
  };

  errors: any = {
    name: null,
    accountNumber: null,
    helpImg: null,
    isActivate: null,
  }

  constructor(
    private firebase: AngularFirestore,
    private authService: FirebaseAuthServiceService,
    public toastController: ToastController,
    private modalController: ModalController,
    private userService: UserService,
    private alertCtrl: AlertController,
    private alertService: AlertService,
    private bankService: BanksService,
    private imgService: ImgService
  ) { }

  ngOnInit() {
    this.getBanks();
  }

  async getUserId() {
    const userSession = await this.authService.getCurrentUser();
    return userSession.uid;
  }

  async getUserByUid() {
    const userUid = await this.getUserId();
    this.userService.getUserById(userUid).subscribe((res) => {
      this.user = res as User;
    });
  }

  getBanks() {
    this.bankService.getBanks().subscribe(res => {
      this.banks = res as bank[];
    })
  }

  //Add bank (insert)
  async addBank() {
    if (!this.validForm()) {
      return;
    }
    this.bank.helpImg = await this.imgService.uploadImage(
      'bank',
      this.imgFile
    );
    //this.alertService.presentLoading('Agregando Banco ...')
    this.bankService.addBank(this.bank);
    this.clearData();
    //this.alertService.dismissLoading();

  }

  //update
  async UpdateBank(id, name, accountNumber, helpImg, isActivate, createdAt, updatedAt) {
    const modal = await this.modalController.create({
      component: UpdatebankComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'id': id,
        'name': name,
        'accountNumber': accountNumber,
        'helpImg': helpImg,
        'isActivate': isActivate,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
      }
    });
    return await modal.present();
  }

  /**
     * Método para validar los campos
     * @returns Retorna verdadero si no hay errores y falso si hay algún error
     */
  validForm() {
    const errors = validateForm(this.bank, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }

  //delete user
  async DeleteBank(id) {
    const res = await this.alertService.presentAlertConfirm('Eliminar', '¿Desea eliminar este Banco?');
    if (res) {
      this.alertService.presentLoading('Eliminando Información Bancaria');
      this.firebase.doc('/bank/' + id).delete().then(async () => {
        this.alertService.loading.dismiss();
        const toast = await this.toastController.create({
          message: 'Eliminación de Banco/Cooperativa Exitosa 🤨',
          duration: 2000
        });
        toast.present();
      }).catch((e) => {
        this.alertService.loading.dismiss();
        this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
      });
    }
  }

  clearData() {
    this.bank = {
      uid: '',
      name: '',
      accountNumber: '',
      helpImg: '',
      isActivate: '',
    };
  }

  async mensaje(title: string, messag: string) {
    const alert = await this.alertCtrl.create({
      message: messag,
      subHeader: title,
      buttons: [{ text: 'Aceptar', role: 'cancel' }],
    });
    await alert.present();
  }
  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.bank.helpImg = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }


}
