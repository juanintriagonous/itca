import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatabankPage } from './databank.page';

const routes: Routes = [
  {
    path: '',
    component: DatabankPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatabankPageRoutingModule {}
