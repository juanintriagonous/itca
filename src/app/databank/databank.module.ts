import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatabankPageRoutingModule } from './databank-routing.module';

import { DatabankPage } from './databank.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatabankPageRoutingModule
  ],
  declarations: [DatabankPage]
})
export class DatabankPageModule {}
