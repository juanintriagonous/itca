import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IconnselectorPageRoutingModule } from './iconnselector-routing.module';

import { IconnselectorPage } from './iconnselector.page';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IconnselectorPageRoutingModule,
    FontAwesomeModule
  ],
  declarations: [IconnselectorPage]
})
export class IconnselectorPageModule {}
