import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IconnselectorPage } from './iconnselector.page';

const routes: Routes = [
  {
    path: '',
    component: IconnselectorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IconnselectorPageRoutingModule {}
