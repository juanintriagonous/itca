import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as awesomeIcons from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-iconnselector',
  templateUrl: './iconnselector.page.html',
  styleUrls: ['./iconnselector.page.scss'],
})
export class IconnselectorPage implements OnInit {
 /* Atributo que almacena el icono elegido */
 @Input() icon: string;
 selectedIcon: string = '';

 /**
 * Lista completa de íconos.
 */
 awesome = [];
 auxAwesomeIcons = [];
 loadingIcons = true;
 skeletonNumber = [0, 1, 2, 3, 4];
 constructor(private modalController: ModalController) {
   this.icon = "";
 }

 ngOnInit() {
 }
 ionViewDidEnter() {
   this.loadIcons();
   this.selectedIcon = this.icon;
 }

 loadIcons() {
   for (const iconName in awesomeIcons) {
     if (iconName.substring(0, 2) === 'fa' && iconName !== 'fas') {
       const icon = awesomeIcons[iconName];
       icon.iconName && this.awesome.push(icon);
     }
   }
   this.auxAwesomeIcons = [...this.awesome];
   this.loadingIcons = false;
 }

 /* Metodo para cerrar el modal */
 closeModal() {
   this.modalController.dismiss({
     option: ""
   });
 }

 /* Metodo que envia el dato a la pagina principal */
 sendData() {
   this.modalController.dismiss({
     'option': this.icon || "",
   });
 }

 searchIcons({ detail }) {
   const searchText = detail.value.toLowerCase();
   this.awesome = this.auxAwesomeIcons.filter((s) => s.iconName.toLowerCase().includes(searchText))
 }

 handleSelectedIcon({ detail }) {
   this.icon = detail.value;
 }
}
