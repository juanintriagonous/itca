import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController, ModalController, AlertController } from '@ionic/angular';
import { UpdatenotificationComponent } from '../components/updatenotification/updatenotification.component';
import { Notification } from '../interfaces/notification.interface';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-admin-notifications',
  templateUrl: './admin-notifications.page.html',
  styleUrls: ['./admin-notifications.page.scss'],
})
export class AdminNotificationsPage implements OnInit {
  notifications: Notification[];


  publicado: any;

  trasformar: any;

  constructor(
    private firestore: AngularFirestore,
    private alertService: AlertService,
    public toastController: ToastController,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.getNotifications().subscribe(res =>{
      this.notifications = res as Notification[];
    });
  }

  getNotifications() {
    const ref = this.firestore
    .collection('notification').valueChanges({ idField: 'uid' });
    return ref
  }


 async delete(notificationUid:string) {
   
     const confirmDelete = await this.alertService.presentAlertConfirm(
       'ITCA App',
      'Desea Eliminar esta noticia?'
     );
     if(confirmDelete){
      this.deleteNotification(notificationUid);
      this.alertService.presentAlert('Notifica Eliminada🤯')
     }   
  }

  deleteNotification(uid: string){
    this.firestore.doc(`/notification/${uid}`).delete();
  }

  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000,
    });
    toast.present();
  }

  reduceNotificationDescription(notificationDescription: string): string {
    return `${notificationDescription.substring(0, 30)} ...`;
  }

  tiempo(time: any) {
    //  console.log(Date.now() + "");
    let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
    this.publicado = '';
    this.trasformar = 0;
    //Segundos
    if (tiempo > 0 && tiempo < 60) {
      this.trasformar = tiempo;
      this.publicado = this.trasformar + ' Seg';
    }
    //minustos
    else if (tiempo > 60 && tiempo < 3600) {
      this.trasformar = (tiempo / 60).toFixed(0);
      this.publicado = this.trasformar + ' Min';
    }
    //horas
    else if (tiempo > 3600 && tiempo < 86400) {
      this.trasformar = (tiempo / 3600).toFixed(0);
      this.publicado = this.trasformar + ' Horas';
    }
    //Dias
    else if (tiempo > 86400 && tiempo < 604800) {
      this.trasformar = (tiempo / 86400).toFixed(0);
      this.publicado = this.trasformar + ' Dias';
    } else if (tiempo > 604800 && tiempo < 2629743) {
      this.trasformar = (tiempo / 604800).toFixed(0);
      this.publicado = this.trasformar + ' Semanas';
    }
    return this.publicado;
  }

  async update(notification: Notification) {
    const modal = await this.modalController.create({
      component: UpdatenotificationComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        notification
      },
    });
    return await modal.present();
  }
}
