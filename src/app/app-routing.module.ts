import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { canActivate, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { GuardGuard } from '../app/services/guard.guard';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// Enviar usuarios no autorizados para iniciar sesión
const redirectUnauthorizedToLogin = () =>redirectUnauthorizedTo(['/login']);
//Iniciar sesión automáticamente en usuarios
const redirectLoggedInToHome = () => redirectLoggedInTo(['/main']);
const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: 'panel-admin',
    loadChildren: () => import('./panel-admin/panel-admin.module').then( m => m.PanelAdminPageModule),
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'categories',
    loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then( m => m.MainPageModule)
  },
  {
    path: 'notice/:id',
    loadChildren: () => import('./notice/notice.module').then( m => m.NoticePageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'contactus',
    loadChildren: () => import('./contactus/contactus.module').then( m => m.ContactusPageModule)
  },
  {
    path: 'admin-notifications',
    loadChildren: () => import('./admin-notifications/admin-notifications.module').then( m => m.AdminNotificationsPageModule)
  },
  {
    path: 'contacts',
    loadChildren: () => import('./contacts/contacts.module').then( m => m.ContactsPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'panel-student',
    loadChildren: () => import('./panel-student/panel-student.module').then( m => m.PanelStudentPageModule)
  },
  {
    path: 'successreg',
    loadChildren: () => import('./successreg/successreg.module').then( m => m.SuccessregPageModule)
  },
  {
    path: 'upinvoice',
    loadChildren: () => import('./upinvoice/upinvoice.module').then( m => m.UpinvoicePageModule)
    ,
    canActivate: [GuardGuard],
  },
  {
    path: 'invoicelist',
    loadChildren: () => import('./invoicelist/invoicelist.module').then( m => m.InvoicelistPageModule),
    canActivate: [GuardGuard],
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'list-invoice-admin',
    loadChildren: () => import('./list-invoice-admin/list-invoice-admin.module').then( m => m.ListInvoiceAdminPageModule)
  },
  {
    path: 'see-detail-invoice/:id',
    loadChildren: () => import('./see-detail-invoice/see-detail-invoice.module').then( m => m.SeeDetailInvoicePageModule)
  },
  {
    path: 'career',
    loadChildren: () => import('./career/career.module').then( m => m.CareerPageModule)
  },
  {
    path: 'iconnselector',
    loadChildren: () => import('./iconnselector/iconnselector.module').then( m => m.IconnselectorPageModule)
  },
  {
    path: 'databank',
    loadChildren: () => import('./databank/databank.module').then( m => m.DatabankPageModule)
  },
  {
    path: 'list-invoice-rejected',
    loadChildren: () => import('./list-invoice-rejected/list-invoice-rejected.module').then( m => m.ListInvoiceRejectedPageModule)
  },
  {
    path: 'list-invoice-accepted',
    loadChildren: () => import('./list-invoice-accepted/list-invoice-accepted.module').then( m => m.ListInvoiceAcceptedPageModule)
  },
  {
    path: 'list-invoice-incomplete',
    loadChildren: () => import('./list-invoice-incomplete/list-invoice-incomplete.module').then( m => m.ListInvoiceIncompletePageModule)
  },
  {
    path: 'list-invoice-all',
    loadChildren: () => import('./list-invoice-all/list-invoice-all.module').then( m => m.ListInvoiceAllPageModule)
  },  {
    path: 'reset-password',
    loadChildren: () => import('./reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
