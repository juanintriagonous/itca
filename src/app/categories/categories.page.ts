import { Component, OnInit } from '@angular/core';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
//Import AngularFirestore to make Queries.
import { AngularFirestore } from "@angular/fire/firestore";
import { ModalController, ToastController } from '@ionic/angular';
import { UpdatecategoryComponent } from '../components/updatecategory/updatecategory.component';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  Category:
    { name: string; description: string; order: string; state: string };

  Categories: {
    id: string;
    name: string;
    description: string;
    order: string
    state: string;
  }[];

  Orders: {
    id: string;
    value: string;
    state: string;
  }[];

  constructor(
    public toastController: ToastController,
    private authService: FirebaseAuthServiceService,
    private firebase: AngularFirestore,
    private modalController: ModalController) {

    this.Category = { name: "", description: "", order: "", state: "" };
  }

  ngOnInit() {
    this.infoOrder();
    this.firebase
      .collection("/category").snapshotChanges().subscribe(res => {
        if (res) {

          this.Categories = res.map(e => {
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()["name"],
              description: e.payload.doc.data()["description"],
              order: e.payload.doc.data()["order"],
              state: e.payload.doc.data()["state"]

            };
          });
        }
      });
  }

  AddCategory() {
    if (this.Category.name != '' && this.Category.description != '' && this.Category.state != '') {
      let addCategory = {}
      addCategory['name'] = this.Category.name;
      addCategory['description'] = this.Category.description;
      addCategory['order'] = this.Category.order;
      addCategory['state'] = this.Category.state;

      this.firebase.collection('/category/').add(addCategory).then(() => {
        this.Category = { name: '', description: '',order: '', state: '' };
        this.mensaje("Categoría Generada");
      });
    } else {
      this.mensaje("Ingrese todos los datos");
    }

  }

  infoOrder() {
    this.firebase.collection("/order", (ref) => ref.where("state", "==", "public").orderBy('value','asc')).snapshotChanges().subscribe(res => {
      if (res) {
        this.Orders = res.map(e => {
          return {
            id: e.payload.doc.id,
            value: e.payload.doc.data()["value"],
            state: e.payload.doc.data()["state"]
          };
        });
      }
    });
  }


  async update(id, name, order, description, state) {
    const modal = await this.modalController.create({
      component: UpdatecategoryComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'id': id,
        'name': name,
        'description': description,
        'order': order,
        'state': state
      }
    });
    return await modal.present();
  }
  delete(valor: any) {
    this.firebase.doc('/category/' + valor).delete();
    this.mensaje("Categoría eliminada");
  }
  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000
    });
    toast.present();
  }
}

