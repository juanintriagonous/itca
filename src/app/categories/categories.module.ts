import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriesPageRoutingModule } from './categories-routing.module';

import { CategoriesPage } from './categories.page';
import { UpdatecategoryComponent } from '../components/updatecategory/updatecategory.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriesPageRoutingModule
  ],
  entryComponents: [UpdatecategoryComponent],
  declarations: [UpdatecategoryComponent, CategoriesPage]
})
export class CategoriesPageModule {}
