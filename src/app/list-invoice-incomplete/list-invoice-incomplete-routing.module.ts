import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListInvoiceIncompletePage } from './list-invoice-incomplete.page';

const routes: Routes = [
  {
    path: '',
    component: ListInvoiceIncompletePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListInvoiceIncompletePageRoutingModule {}
