import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListInvoiceIncompletePage } from './list-invoice-incomplete.page';

describe('ListInvoiceIncompletePage', () => {
  let component: ListInvoiceIncompletePage;
  let fixture: ComponentFixture<ListInvoiceIncompletePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListInvoiceIncompletePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListInvoiceIncompletePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
