import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListInvoiceIncompletePageRoutingModule } from './list-invoice-incomplete-routing.module';

import { ListInvoiceIncompletePage } from './list-invoice-incomplete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListInvoiceIncompletePageRoutingModule
  ],
  declarations: [ListInvoiceIncompletePage]
})
export class ListInvoiceIncompletePageModule {}
