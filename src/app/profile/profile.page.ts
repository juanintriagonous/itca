import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { career } from '../interfaces/career';
import { levels } from '../interfaces/levels';
import { parallel } from '../interfaces/parallel.interface';
import { User } from '../interfaces/user.interface';
import { AlertService } from '../services/alert.service';
import { CareerService } from '../services/career.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { ImgService } from '../services/img.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})

export class ProfilePage implements OnInit {
  user: User = {
    email: '',
    role: '',
    fullname: '',
    identificationDocument: '',
    gender: '',
    address: '',
    career: null,
    image: '',
    level: null,
    parallel: '',
    isActive: true,
    updateAt: null
  };
  selectedCareer: career;
  selectedLevel: levels;
  levels: levels[];
  parallels: parallel[];
  careers: career[];
  file: File = null;
  fileName: string;
  observableList: any[] = [];
  constructor(
    private authService: FirebaseAuthServiceService,
    private userService: UserService,
    private imgService: ImgService,
    private alertService: AlertService,
    private router: Router,
    private careerService: CareerService
  ) { }
  ionViewWillEnter() {
    this.getSessionUser();
  }
  async getSessionUser() {
    const userSession = await this.authService.getCurrentUser();
    const userGetDataSubscribe = this.userService.getUserById(userSession.uid).subscribe((res) => {
      this.user = res as User;
    });
    this.observableList.push(userGetDataSubscribe);
  }

  getCareer() {
    this.careerService.getCareers().subscribe(res => {
      this.careers = res as career[];
    })
  }
  onChangeCarrer({ detail }) {
    const auxCareer = this.careers.find((c) => (
      c.name === detail.value
    ))
    this.selectedCareer = auxCareer;
    this.levels = auxCareer.levels.filter((c) => (
      c.isActivate === true
    ));
    this.parallels = auxCareer.parallel.filter((c) => (
      c.isActivate === true
    ));
  }
  onChangeLevel({ detail }) {
    const auxLevel = this.levels[detail.value]
    this.selectedLevel = auxLevel
  }

  async updateUser() {
    {
      if (this.file != null) {
        this.user.image = await this.imgService.uploadImage('ProfileUser', this.file);
      }
      const userSession = await this.authService.getCurrentUser();
      this.user.uid = userSession.uid;
      this.alertService.presentLoading('Actualizando Datos...');
      await this.userService.updateUserById(this.user);
      this.alertService.dismissLoading();
      this.alertService.presentAlert('Datos actualizados correctamente');
      this.router.navigate(['/panel-admin']);
    }
  }

  ionViewWillLeave() {
    this.observableList.map((optionSubcribre) => {
      optionSubcribre.unsubscribe();
    });
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.user.image = event.target.result;
      this.fileName = $event.target.files[0].name;
    };
  }

  ngOnInit() {
    this.getCareer();
  }

}
