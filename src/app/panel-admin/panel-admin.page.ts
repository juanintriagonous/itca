import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ToastController } from '@ionic/angular';
import { career } from '../interfaces/career';
import { levels } from '../interfaces/levels';
import { User } from '../interfaces/user.interface';
import { AlertService } from '../services/alert.service';
import { CareerService } from '../services/career.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-panel-admin',
  templateUrl: './panel-admin.page.html',
  styleUrls: ['./panel-admin.page.scss'],
})
export class PanelAdminPage implements OnInit {
  user: User = {
    uid: '',
    email: '',
    role: '',
    fullname: '',
    isActive: true,
  };
  career: career;
  level: levels;

  public isToggled: boolean;

  public loading: boolean;

  constructor(
    private router: Router,
    private toastController: ToastController,
    private alertService: AlertService,
    private userService: UserService,
    private authService: FirebaseAuthServiceService,
    private menu: MenuController,
    private careerService: CareerService
  ) {
    this.isToggled = false;
   }

  ngOnInit() {
    this.menu.enable(true);
  }

  ionViewWillEnter() {
    this.loadData();
    this.user = {
      uid: '',
      email: '',
      role: '',
      fullname: '',
      isActive: true,
    };
    this.getSessionUser();
  }

  
  async loadData() {
    this.user = (await this.getSessionUser()) as User;
    this.validateProfileStudent(this.user);
    this.loadCareer(this.user.career)
  }

  loadCareer(careerUid){
     this.careerService.getCareerByUid(careerUid).subscribe((data)=>{
      this.career = data
      this.level = this.career.levels.find((e)=>(
        e.name === this.user.level
      ))
    })
  }

  async getSessionUser() {
    const userSession = await this.authService.getCurrentUser();
    return new Promise((resolve, reject) => {
      this.userService.getUserById(userSession.uid).subscribe((res) => {
        resolve(res as User);
      });
    });
  }  
  
  async presentToast(valor) {
    const toast = await this.toastController.create({
      message: valor,
      duration: 2000,
    });
    toast.present();
  }

  validateProfileStudent(user: User) {
    if (
      user.role === 'student' &&
      (user.phoneNumber === '' ||
        user.address === '' ||
        user.image === '')
    ) {
      this.alertService
        .presentAlertConfirm(
          'Perfil incompleto!!!🤔',
          'Para la app funcione correctamente por favor, llene todos los datos del perfil.😎'
        )
        .then((res) => {
          if (res) {
            this.router.navigate(['/profile']);
          }
        });
    }
  }


  openFacebook() {
    window.open(
      'https://www.facebook.com/Ruah-Env%C3%ADos-Express-Servicio-de-Encomiendas-y-Entregas-a-domicilio-102467271649209'
    );
  }

  openInstagram() {
    window.open('https://www.instagram.com/ruahenviosexpress/');
  }

  openYoutube() {
    this.alertService.toastShow('Aún no contamos con youtube c:');
  }

}
