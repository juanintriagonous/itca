import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpinvoicePage } from './upinvoice.page';

const routes: Routes = [
  {
    path: '',
    component: UpinvoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpinvoicePageRoutingModule {}
