import { Component, OnInit } from '@angular/core';
import { invoice } from '../interfaces/invoice.interface';
import { ImgService } from '../services/img.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { UserService } from '../services/user.service';
import { User } from '../interfaces/user.interface';
import { UpinvoiceService } from '../services/upinvoice.service';
import { AlertService } from '../services/alert.service';
import { career } from '../interfaces/career';
import { levels } from '../interfaces/levels';
import { CareerService } from '../services/career.service';
import { validateForm } from '../shared/utilities/formValidation';
import { bank } from '../interfaces/bank.interface';
import { BanksService } from '../services/banks.service';

@Component({
  selector: 'app-upinvoice',
  templateUrl: './upinvoice.page.html',
  styleUrls: ['./upinvoice.page.scss'],
})
export class UpinvoicePage implements OnInit {

  sliderOpts = {
    zoom: {
      maxRatio: 2
    }
  };
  
  invoice: invoice = { };
  bank: bank = {};
  banks: bank[];
  selectedBank: bank;

  validationRules = {
    required: ['identifynumber', 'bank', 'img', 'reason', 'detail', 'value'],
  };
  errors: any = {
    identifynumber: null,
    bank: null,
    img: null,
    reason: null,
    detail: null,
    value: null,
  };

  career: career;
  level: levels;
  invoices: invoice[];
  isChecked: boolean;

  imgFile: File = null;

  user: User;

  verifyNumber = true;

  constructor(
    private imgService: ImgService,
    private authService: FirebaseAuthServiceService,
    private userService: UserService,
    private upinvoiceService: UpinvoiceService,
    private alertService: AlertService,
    private careerService: CareerService,
    private bankService: BanksService
  ) { }

  ngOnInit() {
    this.cleanInvoice();
    this.getUserId();
    this.getUserByUid();
    this.getInvoices();
    this.getBanks();
  }

  cleanInvoice(){
    this.invoice = {
      useruid: '',
      identifynumber: '',
      bank: '',
      career: '',
      studentName: '',
      level:'',
      img: '',
      reason: '',
      detail: '',
      value: '',
      resolution: 'registered',
      createAt: null,
      updateAt: null,
    };
  }
  loadCareer(careerUid) {
    this.careerService.getCareerByUid(careerUid).subscribe((data) => {
      this.career = data
      this.level = this.career.levels.find((e) => (
        e.uid === this.user.level
      ))
    })
  }

  async getUserId() {
    const userSession = await this.authService.getCurrentUser();
    return userSession.uid;
  }

  async getUserByUid() {
    const userUid = await this.getUserId();
    this.userService.getUserById(userUid).subscribe((res) => {
      this.user = res as User;
      this.loadCareer(this.user.career);
    });
  }

  getInvoices() {
    this.upinvoiceService.getAllInvoices().subscribe(res => {
      this.invoices = res as invoice[];
      this.checkPaymentNumber();
    })
  }

  getBanks(){
    this.bankService.getBanks().subscribe(res => {
      this.banks = res as bank[]
    })
  }

  checkPaymentNumber() {
    this.invoices.map((res: invoice) => {
      if (res.identifynumber === this.invoice.identifynumber) {
        this.verifyNumber = false
      }
    })
  }

  onChangeBank({ detail }) {
    const auxBank = this.banks.find((b) => (
      b.name === detail.value
    ))
    this.selectedBank = auxBank;
  }
  
  async addInvoice() {
    this.checkPaymentNumber();
    if (this.verifyNumber) {
      if (this.isChecked) {

        if (!this.validaForm()) {
          return;
        } {
          this.invoice.img = await this.imgService.uploadImage(
            'invoice',
            this.imgFile
          );
          this.invoice.useruid = await this.getUserId();
          this.upinvoiceService.addInvoice(this.invoice);
          this.cleanInvoice();
          this.alertService.presentAlert("Guardado correctamente");          
        }
      } else {
        this.alertService.presentAlert('Acepte los términos de manejo de información y finanzas')
      }
    }
    else {
      this.alertService.presentAlert('Este número de comprobante ya fue registrado, intente con otros Datos')
    }
    this.verifyNumber = true;
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.invoice.img = event.target.result;
      this.imgFile = $event.target.files[0];
    };
  }
  validaForm() {
    const errors = validateForm({ ...this.invoice, identifynumber: this.invoice.identifynumber }, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }
}
