import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpinvoicePageRoutingModule } from './upinvoice-routing.module';

import { UpinvoicePage } from './upinvoice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpinvoicePageRoutingModule
  ],
  declarations: [UpinvoicePage]
})
export class UpinvoicePageModule {}
