import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PanelStudentPageRoutingModule } from './panel-student-routing.module';

import { PanelStudentPage } from './panel-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PanelStudentPageRoutingModule
  ],
  declarations: [PanelStudentPage]
})
export class PanelStudentPageModule {}
