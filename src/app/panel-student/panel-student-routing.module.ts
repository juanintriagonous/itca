import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelStudentPage } from './panel-student.page';

const routes: Routes = [
  {
    path: '',
    component: PanelStudentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelStudentPageRoutingModule {}
