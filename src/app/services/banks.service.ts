import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import app from 'firebase/app';
import { bank } from '../interfaces/bank.interface';

@Injectable({
  providedIn: 'root'
})
export class BanksService {

  constructor(private firebase: AngularFirestore) { }

  addBank(bank:bank){
    bank.createdAt = app.firestore.Timestamp.now().toDate();
    bank.updatedAt = app.firestore.Timestamp.now().toDate();
    this.firebase.collection('/bank').add(bank);
  }
  
  updateBank(bank:bank){
    bank.updatedAt = app.firestore.Timestamp.now().toDate();
    this.firebase.collection('/bank').doc(bank.uid).update(bank);
  }

  deleteBank(uid: string){
    this.firebase.doc(`/bank/${uid}`).delete();
  }

  getBankByUid(uid: string){
    const ref = this.firebase
      .collection('/bank/').doc(uid)
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getBanks(){
    const ref = this.firebase
      .collection('bank')
      .valueChanges({ idField: 'uid' });
    return ref;
  }

}
