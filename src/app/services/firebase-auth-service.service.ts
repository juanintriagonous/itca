import { Injectable } from '@angular/core';
import app from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})

export class FirebaseAuthServiceService {

  constructor(
    private firestore: AngularFirestore) {
      
  }
  async getCurrentUser() {
    return await app.auth().currentUser;
  }
  async signIn(email: string, password: string) {
    return await app.auth().signInWithEmailAndPassword(email, password);
  }
  async signUp(user: User, password: string, nroident: string, phone: string, gender: string, career: string, level: string) {
    const newUser: app.auth.UserCredential = await app
      .auth()
      .createUserWithEmailAndPassword(user.email, password);
    const uid = newUser.user.uid;
    user.createAt = app.firestore.Timestamp.now().toDate();
    user.updateAt = app.firestore.Timestamp.now().toDate();
    user.identificationDocument = nroident;
    user.gender = gender;
    user.career = career;
    user.level = level;
    user.phoneNumber = phone;
    await this.firestore.doc(`users/${uid}`).set(user);
    await newUser.user.sendEmailVerification();
    await this.logoutUser();
  }
  async logoutUser() {
    return await app.auth().signOut();
  }
  async resetPassword(email: string): Promise<void> {
    return await app.auth().sendPasswordResetEmail(email);
  }
}
