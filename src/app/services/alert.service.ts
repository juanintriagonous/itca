import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';

const { Toast } = Plugins

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  loading: HTMLIonLoadingElement;

  constructor(
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController
  ) { }
  async presentLoading(message: string){
    this.loading = await this.loadingController.create({
      message,
    });
    await this.loading.present();
  }

  async dismissLoading(){
    this.loading.dismiss();
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
        },
      ],
    });
    await alert.present();
  }

  async presentAlertWithHeader(header: string, message: string){
    const alert = await this.alertController.create({
      header,
      message,
      buttons:[
        {
          text: 'Aceptar',
          role: 'cancel'
        },
      ],
    });
    await alert.present();
  }
  async presentAlertUpdated(header: string, message: string) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
          handler: () => {
            this.modalController.dismiss();
          },
        },
      ],
    });
    await alert.present();
  }
  async presentAlertConfirm(header: string, message: string) {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: header,
        message: message,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              return resolve(false);
            },
          },
          {
            text: 'Aceptar',
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await alert.present();
    });
  }

  async presentModal(modalPageName: any, data: any) {
    const modal = await this.modalController.create({
      component: modalPageName,
      componentProps: data,
    });
    await modal.present();
  }

  async presentAlertConfirmTest(categoryId: string, remove) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¡Confirme!',
      message: '¿Esta seguro de que desea <strong>eliminar</strong> esta categoría?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: remove(categoryId),
        },
      ],
    });
    await alert.present();
  }

  async toastShow(messenger: string) {
    await Toast.show({
      text: messenger,
    });
  }
}
