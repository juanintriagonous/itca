import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plugins } from '@capacitor/core';
import app from 'firebase/app';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user.interface';
import { FirebaseAuthServiceService } from './firebase-auth-service.service';
const { Toast } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private firestore: AngularFirestore, 
    private authService: FirebaseAuthServiceService
    ) { }
    //obtener usuario por id
  getUserById(uid: string): Observable<any> {
    return this.firestore.collection('users').doc(uid).valueChanges({ idField: 'uid' });
  }
  //obtener id de usuario
  async getUserId() {
    const userSession = await this.authService.getCurrentUser();
    return userSession.uid;
  }
  //obtener usuario por rol
  getUserByRole(namerole: string){
    return this.firestore.collection('/users', (ref) => ref.where('role', '==', namerole)
    .orderBy('fullname', 'asc')).valueChanges({idfield: 'uid'});
  }

  //actualizar el usuario eliminando la otra instancia
  async updateUserById(user: User){
    const uidupdt = user.uid;
    delete user.uid;
    user.updateAt = app.firestore.Timestamp.now().toDate();
    try {
      this.toastAlert('Perfil Actualizado');
      return await this.firestore.collection('users').doc(uidupdt).update(user);
    } catch (error) {
      this.toastAlert(error);
      return error;
    }
  }
  //alerta en toast (android)
  async toastAlert(message: string) {
    await Toast.show({
      text: message,
    });
  }
  //actualizar el rol
  updateRole(userId: string, role: string) {
    this.firestore.collection('users').doc(userId).update({ role: role });
  }

  //actualizar el estatus del usuario
  updateStatus(userId: string, isActive: boolean) {
    this.firestore.collection('users').doc(userId).update({ isActive: isActive });
  }
}
