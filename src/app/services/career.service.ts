import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import app from 'firebase/app';
import { career } from '../interfaces/career';

@Injectable({
  providedIn: 'root'
})
export class CareerService {

  constructor(
    private firebase: AngularFirestore
  ) { }
  addCareer(career:career){
    career.createdAt = app.firestore.Timestamp.now().toDate();
    career.updatedAt = app.firestore.Timestamp.now().toDate();
    this.firebase.collection('/career').add(career);
  }
  
  updateCareer(career:career){
    career.updatedAt = app.firestore.Timestamp.now().toDate();
  return  this.firebase.collection('/career').doc(career.uid).update(career);
  }

  deleteCareer(uid: string){
    this.firebase.doc(`/career/${uid}`).delete();
  }

  getCareers(){
    const ref = this.firebase
      .collection('career')
      .valueChanges({ idField: 'uid' });
    return ref;
  }
  
  getCareerByUid(careerUid){
    return  this.firebase
      .collection('career').doc(careerUid)
      .valueChanges({ idField: 'name' })

  }

  getCareerByName(careerUid){
    return  this.firebase
      .collection('career').doc(careerUid)
      .valueChanges({ idField: 'uid' })

  }
}
