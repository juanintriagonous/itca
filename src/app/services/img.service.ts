import { Injectable } from '@angular/core';
import app from 'firebase/app';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ImgService {
  constructor(public loadigController: LoadingController) { }
  //subida de imagen 
  async uploadImage(location: string, file: File):Promise<any>{
    const loading = await this.loadigController.create({
      cssClass: 'my-custom-class',
      message: 'Guardando Imagen'
    });
    await loading.present();
    try{
      const uploadTask = await app.storage().ref().child(`${location}/${file.name}`).put(file);
      loading.dismiss();
      return await uploadTask.ref.getDownloadURL();
    }catch(error){
      loading.dismiss();
      return error;
    }
  }
  //borrado de imagen
  async deleteImage(location: string, imageName: string): Promise <any>{
    return await app.storage().ref().child(`${location}/${imageName}`).delete();
  }
  //subida momentanea de imagen
  uploadImgTemporary($event){
    const reader = new FileReader();
    if($event.target.files && $event.target.files[0]){
      reader.readAsDataURL($event.target.files[0]);
    }
    return reader;
  }
}
