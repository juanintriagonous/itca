import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import app from 'firebase/app';
import { invoice } from '../interfaces/invoice.interface';

@Injectable({
  providedIn: 'root'
})
export class UpinvoiceService {

  constructor(private firestore: AngularFirestore) { }

  addInvoice(invoice:invoice){
    invoice.updateAt = app.firestore.Timestamp.now().toDate();
    invoice.createAt = app.firestore.Timestamp.now().toDate();
    invoice.date = invoice.createAt.toDateString();
    this.firestore.collection('/invoice').add(invoice);
  }

  updateInvoice(invoice:invoice){
    invoice.updateAt = app.firestore.Timestamp.now().toDate();
    this.firestore.collection('/invoice').doc(invoice.uid).update(invoice);
  }

  deleteInvoice(uid: string) {
    this.firestore.doc(`/invoice/${uid}`).delete();
  }

  getInvoiceByUser(uid: string){
    const ref = this.firestore
      .collection('/invoice/', (ref) => ref.where('useruid', '==', uid))
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getInvoiceByUid(uid: string){
    const ref = this.firestore
      .collection('/invoice/').doc(uid)
      .valueChanges({ idField: 'uid' });
    return ref;
  }

  getInvoicesPendding(){
    return this.firestore
      .collection('/invoice', (ref) => ref.where('resolution','==','registered'))
      .valueChanges({ idField: 'uid' });
  }

  getAllInvoices(){
    return this.firestore
      .collection('invoice')
      .valueChanges({ idField: 'uid' });
  }

}
