import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class PushService {
  useId: string;
  notiId: string;
  constructor(public oneSignal: OneSignal, public http: HttpClient,private router: Router) { }
  configuracionInicial() {

    this.oneSignal.startInit('b9ffa2ea-ff54-40ba-85fd-7f7f29ad32d0', '325051574126');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      // do something when notification is received
      console.log("Notificacion recibida " + noti.payload.additionalData["notification"]);
      this.notiId=noti.payload.additionalData["notification"]+"";
    });

    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      // do something when a notification is opened
     console.log("Notificacion abierta " +  this.notiId+noti); 
     this.router.navigate(['/notice/'+this.notiId]);
    });
    //obtener uid de onsignal
    this.oneSignal.getIds().then(info => {
      this.useId = info.userId;
    });

    this.oneSignal.endInit();

  }

  public onesignal(title: String, subtitle: any, detalle: string, uidnoti: string) {
    //dato a mandar
    let datos = {
      app_id: "b9ffa2ea-ff54-40ba-85fd-7f7f29ad32d0", included_segments: ["Active Users", "Inactive Users"],
      headings: { "en": title }, subtitle: { "en": subtitle },
      data: { "notification": uidnoti, "fecha": "100-210-250" }, contents: { "en": detalle }
    }

    const headers = {
      'Authorization': 'Basic MjFlYzExNmItNzNlNy00OTVmLTg5ZjUtZmYwODU0NzNkZjky'
    }

    console.log(JSON.stringify(datos))
    var url = 'https://onesignal.com/api/v1/notifications';
    return new Promise(resolve => {
      this.http.post(url, datos, { headers })
        .subscribe(data => {
          console.log(data)
          resolve(data);
        });
    });
  }
}
