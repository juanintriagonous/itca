import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import app from 'firebase/app';
import { PushService } from './push.service';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  constructor(private router: Router, private PushService: PushService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> {
    return new Promise((resolve, reject) => {
      app.auth().onAuthStateChanged((user: app.User) => {
        if (user) {
          if (user.emailVerified) {           
           
            resolve(true);
          } else {
            this.router.navigate(['/login']);
          }
        } else {
          this.router.navigate(['/login']);
          resolve(false);
        }
      });
    });
  }
  
}
