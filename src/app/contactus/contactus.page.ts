import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.page.html',
  styleUrls: ['./contactus.page.scss'],
})
export class ContactusPage implements OnInit {

    /* Obejeto para configurar el slide */
    categoryConfig = {
      spaceBetween: 1,
      slidesPerView:2.5,
      
    };
    publicado: any;
    trasformar: any;
    /* Obejeto para configurar el slide de los banners   */
    bannerConfig = {
      spaceBetween: 10,
      slidesPerView: 1,
      autoplay:true,
    };

    contacts: {
      id: string;
      categoria: string;
      hiper: string;
      icon: String;
      state: string;
    }[];

    lastFivenotification: {
      id: string;
      name: string;
      description: string;
      creationDate:string,
      updateDate:string,
      img: string;
      state: string;
    }[];

    constructor(private firestore: AngularFirestore)  { }

  ngOnInit() {
    this.contactsQuery();
    this.lastFiveNewsQuery();
  }

ventana(valor:any){
    window.open(valor, '_system');
}

substr(text) {
    let textResult: string;
    textResult = text.substring(0, 100);
    return textResult;
  }

/* Metodo para consultar los contactos */
contactsQuery(){
  this.firestore
      .collection("/contacts",(ref) =>
      ref.where("state","==","public")).snapshotChanges().subscribe(res => {
        if (res) {

          this.contacts = res.map(e => {
            return {
              id: e.payload.doc.id,
              categoria: e.payload.doc.data()["categoria"],
              hiper: e.payload.doc.data()["hiper"],
              icon: e.payload.doc.data()["icon"],
              state: e.payload.doc.data()["state"]

            };
          });
        }
      });

}

/* Metodo para consultar las ultimas 5 noticias publicadas */
lastFiveNewsQuery(){
  const notificationRef = this.firestore.collection('/notification/', (ref) =>
 ref.orderBy("updateDate","desc").limit(5))

      notificationRef
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.lastFivenotification = res.map((e) => {
           
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              description: e.payload.doc.data()['description'],
              creationDate: e.payload.doc.data()['creationDate'],
              updateDate: this.tiempo(e.payload.doc.data()["updateDate"]),
              img: e.payload.doc.data()['img'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });

}
tiempo(time: any) {
  //  console.log(Date.now() + "");
  let tiempo = parseInt((Date.now() / 1000 - time['seconds']).toFixed(0));
  this.publicado = '';
  this.trasformar = 0;
  //Segundos
  if (tiempo > 0 && tiempo < 60) {
    this.trasformar = tiempo ;
    this.publicado = this.trasformar + " Seg";
  }
  //minustos 
  else if (tiempo > 60 && tiempo < 3600) {
    this.trasformar = (tiempo / 60).toFixed(0);
    this.publicado = this.trasformar + " Min";
  }
  //horas 
  else if (tiempo > 3600 && tiempo < 86400) {
    this.trasformar = (tiempo / 3600).toFixed(0);
    this.publicado = this.trasformar + " Horas";
  }
  //Dias 
  else if (tiempo > 86400 && tiempo < 604800) {
    this.trasformar = (tiempo / 86400).toFixed(0);
    this.publicado =  this.trasformar +" Dias";
  }
  else if (tiempo > 604800  && tiempo < 2629743 ) {
    this.trasformar = (tiempo / 604800).toFixed(0);
    this.publicado =  this.trasformar +" Semanas";
  }
  return this.publicado;
}

}
