import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { FirebaseAuthServiceService } from '../services/firebase-auth-service.service';
import { listErrors } from '../shared/Errors/listErrors';
import { validateForm } from '../shared/utilities/formValidation';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  user = {
    email: '',
  };
  validationRules = {
    required: ['email'],
    email: ['email'],
  };
  errors: any = {
    email: null,
  };
  constructor(
    private router: Router,
    private authService: FirebaseAuthServiceService,
    private alertService: AlertService
  ) {}

  ngOnInit() {}
  /**
   * Método para iniciar sesión
   */
  async resetPasswordUser() {
    if (!this.validForm()) {
      return;
    }
    await this.alertService.presentLoading('Espere un momento...');
    try {
      await this.authService.resetPassword(this.user.email);
      this.alertService.loading.dismiss();
      await this.alertService.presentAlert(
        'Revise su correo institucional para restablecer su contraseña. 👻'
      );
      this.alertService.loading.dismiss();
      this.router.navigate(['/login']);
    } catch (e) {
      this.alertService.loading.dismiss();
      this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
    }
  }
  /**
   * Método para validar los campos
   * @returns Retorna verdadero si no hay errores y falso si hay algún error
   */
  validForm() {
    const errors = validateForm(this.user, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }

}
