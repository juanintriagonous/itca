import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { PushService } from '../services/push.service';
import { Notification } from '../interfaces/notification.interface';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})

export class NotificationPage implements OnInit {

  notification: Notification;
  validationRules = {
    required: [
      'name',
      'description',
      'order',
      'img',
      'priority',
      'category'],
  };

  errors; any = {
    name: null,
    description: null,
    order: null,
    img: null,
    priority: null,
    category: null
  }


  notifications: Notification[];

  file: File = null;
  
  
  Categories: {
    id: string;
    name: string;
    description: string;
    state: string;
  }[];

  Orders: {
    id: string;
    value: string;
    state: string;
  }[];

  Priorities: {
    id: string;
    name: string;
    state: string;
  }[];

  constructor(
    public pushService: PushService,
    public toastController: ToastController,
    public loadingController: LoadingController,
    private firestore: AngularFirestore) {
    this.notification = {
      uid: '',
      name: '',
      description: '',
      state: '',
      creationDate: null,
      hyper: '',
      priority: '',
      updateDate: null,
      category: '',
      img: '',
      order: ''
    };

  }

  ngOnInit() {
    this.infocategory();
    this.infoOrder();
    this.infoPriority();
  }

  /* Método para cambiar la imagen de la notificación temporalemnte, recibe un evento como parametro */
  fileChange($event): void {
    this.file = $event.target.files[0];
    if ($event.target.files && $event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.notification.img = event.target.result;
      }
      reader.readAsDataURL($event.target.files[0]);
    }

  }

  infocategory() {
    this.firestore.collection("/category", (ref) => ref.where("state", "==", "public")).snapshotChanges().subscribe(res => {
      if (res) {
        this.Categories = res.map(e => {
          return {
            id: e.payload.doc.id,
            name: e.payload.doc.data()["name"],
            description: e.payload.doc.data()["description"],
            state: e.payload.doc.data()["state"]
          };
        });
      }
    });
  }

  infoOrder() {
    this.firestore.collection("/order", (ref) => ref.where("state", "==", "public").orderBy('value', 'asc')).snapshotChanges().subscribe(res => {
      if (res) {
        this.Orders = res.map(e => {
          return {
            id: e.payload.doc.id,
            value: e.payload.doc.data()["value"],
            state: e.payload.doc.data()["state"]
          };
        });
      }
    });
  }

  infoPriority() {
    this.firestore.collection("/priority", (ref) => ref.where("state", "==", "public").orderBy("name", "asc")).snapshotChanges().subscribe(res => {
      if (res) {
        this.Priorities = res.map(e => {
          return {
            id: e.payload.doc.id,
            name: e.payload.doc.data()["name"],
            state: e.payload.doc.data()["state"]
          };
        });
      }
    });
  }

  async addNotification(): Promise<any> {
    const loading = await this.loadingController.create();
    await loading.present();
    if (this.file == null) {
      this.mensaje('Por favor ingrese la imgen');
      console.log("no hay" + this.file);
      loading.dismiss();
    } else {
      console.log(this.file);
      console.log("uploadProfileImage");
      const uploadTask = await firebase.storage().ref('notification/' + this.file.name);
      uploadTask.put(this.file).then(snapshot => {
        return uploadTask.getDownloadURL().then(url => {
          loading.dismiss();
          console.log(url)
          this.updatein(url);
        })
      }).catch(error => {
        loading.dismiss();
        console.log(error);
      })

    }
  }

  updatein(valor: any) {
    if (this.notification.name !== ''
      && this.notification.description !== ''
      && this.notification.category !== ''
      && this.notification.order !== ''
      && this.notification.state !== ''
    ) {

      let addNotification = {}
      addNotification['name'] = this.notification.name;
      addNotification['description'] = this.notification.description;
      addNotification['state'] = this.notification.state;
      addNotification['order'] = this.notification.order;
      addNotification['hyper'] = this.notification.hyper;
      addNotification['priority'] = this.notification.priority;
      addNotification['creationDate'] = firebase.firestore.Timestamp.now();
      addNotification['updateDate'] = firebase.firestore.Timestamp.now();
      addNotification['category'] = this.notification.category;
      addNotification['img'] = valor;
      this.firestore.collection('/notification/').add(addNotification).then((ids) => {

        this.pushService.onesignal(this.notification.name, this.notification.name, this.notification.description, ids.id + "");
        this.notification = {
          name: '',
          description: '',
          state: '',
          creationDate: null,
          updateDate: null,
          category: '',
          hyper: '',
          priority: '',
          img: '',
          order: ''
        };
        this.mensaje('Notificación enviada');
      })

    } else {
      this.mensaje('Por favor ingrese todos los datos');
    }


  }
  
  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000
    });
    toast.present();
  }


}
