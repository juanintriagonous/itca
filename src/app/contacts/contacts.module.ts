import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactsPageRoutingModule } from './contacts-routing.module';

import { ContactsPage } from './contacts.page';

import { ChooseIconComponent } from '../components/choose-icon/choose-icon.component';
import { UpdatecontactsComponent } from '../components/updatecontacts/updatecontacts.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactsPageRoutingModule
  ],
  entryComponents:[ChooseIconComponent,UpdatecontactsComponent],
  declarations: [ContactsPage,UpdatecontactsComponent,ChooseIconComponent]
})
export class ContactsPageModule {}
