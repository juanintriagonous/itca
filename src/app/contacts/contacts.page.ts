import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { ChooseIconComponent } from '../components/choose-icon/choose-icon.component';
import { UpdatecontactsComponent } from '../components/updatecontacts/updatecontacts.component';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit {


  contact:
    { categoria: string; hiper: string; icon: String, state: string };

  contacts: {
    id: string;
    categoria: string;
    hiper: string;
    icon: String;
    state: string;
  }[];

  constructor(
    private firestore: AngularFirestore, 
    public toastController: ToastController,
    private modalController: ModalController) {

    this.contact = { categoria: "", hiper: "", icon: "", state: "" };



  }

  ngOnInit() {

    this.firestore
      .collection("/contacts").snapshotChanges().subscribe(res => {
        if (res) {

          this.contacts = res.map(e => {
            return {
              id: e.payload.doc.id,
              categoria: e.payload.doc.data()["categoria"],
              hiper: e.payload.doc.data()["hiper"],
              icon: e.payload.doc.data()["icon"],
              state: e.payload.doc.data()["state"]

            };
          });
        }
      });
  }

  Addcontact() {
    if (this.contact.categoria != "" && this.contact.hiper != "" && this.contact.icon != "" && this.contact.state != "") {
      let addcontact = {}
      addcontact['categoria'] = this.contact.categoria;
      addcontact['hiper'] = this.contact.hiper;
      addcontact['icon'] = this.contact.icon;
      addcontact['state'] = this.contact.state;
      this.firestore.collection('/contacts/').add(addcontact).then(() => {
        this.contact = { categoria: "", hiper: "", icon: "", state: "" };
        this.mensaje("Contacto agregado");
      })
    } else {
      this.mensaje("Ingrese todos los datos");
    }
  }

  delete(valor: any) {
    this.firestore.doc('/contacts/' + valor).delete();
    this.mensaje("Contacto eliminada");

  }

  ventana(valor: any) {
    window.open(valor, '_system');
  }
  
  async update(id, categoria, hiper, icon,state) {
    console.log(icon)
    const modal = await this.modalController.create({
      component: UpdatecontactsComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'id': id,
        'categoria': categoria,
        'hiper': hiper,
        'icon': icon,
        'state': state
      }
    });
    return await modal.present();
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ChooseIconComponent,
      cssClass: 'my-custom-class-2',
      showBackdrop: false,
      backdropDismiss: false,
    });

    modal.onDidDismiss().then((nav) => {

      this.contact.icon = nav.data.option;
    });

    return await modal.present();
  }

  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000
    });
    toast.present();
  }

}

