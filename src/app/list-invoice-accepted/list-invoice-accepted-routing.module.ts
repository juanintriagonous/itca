import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListInvoiceAcceptedPage } from './list-invoice-accepted.page';

const routes: Routes = [
  {
    path: '',
    component: ListInvoiceAcceptedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListInvoiceAcceptedPageRoutingModule {}
