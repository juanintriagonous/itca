import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListInvoiceAcceptedPageRoutingModule } from './list-invoice-accepted-routing.module';

import { ListInvoiceAcceptedPage } from './list-invoice-accepted.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListInvoiceAcceptedPageRoutingModule
  ],
  declarations: [ListInvoiceAcceptedPage]
})
export class ListInvoiceAcceptedPageModule {}
