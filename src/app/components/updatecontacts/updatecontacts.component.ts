import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController, ModalController } from '@ionic/angular';
import { ChooseIconComponent } from '../../components/choose-icon/choose-icon.component';

@Component({
  selector: 'app-updatecontacts',
  templateUrl: './updatecontacts.component.html',
  styleUrls: ['./updatecontacts.component.scss'],
})
export class UpdatecontactsComponent implements OnInit {
  @Input() id: string;
  @Input() categoria: string;
  @Input() hiper: string
  @Input() icon: string;  
  @Input() state: string;

  constructor(
    private firebase: AngularFirestore, 
    public toastController: ToastController, 
    private modalController: ModalController) { }

  ngOnInit() {}

  quit() {
    this.modalController.dismiss();
  }

  update() {
    if (this.categoria != '' && this.hiper != '' && this.state != '') {
      let contact = {};
      contact['categoria'] = this.categoria;
      contact['hiper'] = this.hiper;
      contact['icon'] = this.icon;
      contact['state'] = this.state;
      this.firebase.doc('/contacts/' + this.id).update(contact).then(() => {
        this.quit();
        this.mensaje("Contacto Actualizada");
      });
    } else {

      this.mensaje("Todos los datos debe estar llenos");
    }

  }

  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000
    });
    toast.present();
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ChooseIconComponent,
      cssClass: 'my-custom-class-2',
      showBackdrop: false,
      backdropDismiss: false,
    });

    modal.onDidDismiss().then((nav) => {
      console.log(nav.data.option)
      this.icon = nav.data.option;
    });

    return await modal.present();
  }


  ventana(valor: any) {
    window.open(valor, '_system');
  }
}
