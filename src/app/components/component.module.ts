import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ChooseIconComponent } from './choose-icon/choose-icon.component';
@NgModule({
  declarations: [ChooseIconComponent],
  exports: [ChooseIconComponent],
  imports: [
    CommonModule,
    FontAwesomeModule
  ]
})
export class ComponentsModule {

}
