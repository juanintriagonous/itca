import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-choose-icon',
  templateUrl: './choose-icon.component.html',
  styleUrls: ['./choose-icon.component.scss'],
})
export class ChooseIconComponent implements OnInit {

/* Atributo que almacena el icono elegido */
  icon:String;

  constructor(private modalController: ModalController) {
    this.icon = "";
  }

  ngOnInit() {}

  /* Metodo para cerrar el modal */
  closeModal(){
    this.modalController.dismiss({
      option:""
    });
  }

  /* Metodo que envia el dato a la pagina principal */
  sendData() {
    this.modalController.dismiss({
      'option': this.icon || "",
    });
  }

}
