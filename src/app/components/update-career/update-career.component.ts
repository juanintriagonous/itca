import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { career } from 'src/app/interfaces/career';
import { AlertService } from 'src/app/services/alert.service';
import { CareerService } from 'src/app/services/career.service';
import { listErrors } from 'src/app/shared/Errors/listErrors';
import { validateForm } from 'src/app/shared/utilities/formValidation';

@Component({
  selector: 'app-update-career',
  templateUrl: './update-career.component.html',
  styleUrls: ['./update-career.component.scss'],
})
export class UpdateCareerComponent implements OnInit {

  @Input() career: career;

  validationRules = {
    required: ['name', 'description', 'levels', 'parallel', 'tittleType', 'isActivate'],
  };

  errors: any = {
    id: null,
    name: null,
    description: null,
    levels: null,
    parallel: null,
    tittleType: null,
    isActivate: null
  };

  constructor(
    private modalController: ModalController,
    public loadingController: LoadingController,
    private alertService: AlertService,
    private careerService: CareerService
  ) { }

  ngOnInit() {
  }

  async UpdateCareer() {
    if (!this.validForm(this.career)) {
      return;
    } try {
      await this.alertService.presentLoading('Actualizando Carrera ...');
      this.careerService.updateCareer(this.career).then(()=>{
        this.alertService.dismissLoading();
        this.closeModal()
      }).catch(
        
      )
      
    } catch (e) {
      this.alertService.loading.dismiss();
      this.alertService.presentAlert(listErrors[e.code] || listErrors['app/general']);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }
  /**
     * Método para validar los campos
     * @returns Retorna verdadero si no hay errores y falso si hay algún error
     */
  validForm(formValues) {
    const errors = validateForm(formValues, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }
}
