import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  ModalController,
  LoadingController,
  ToastController,
} from '@ionic/angular';
import firebase from 'firebase/app';
import { PushService } from '../../services/push.service';
import { Notification } from '../../interfaces/notification.interface';
import { ImgService } from '../../services/img.service';
@Component({
  selector: 'app-updatenotification',
  templateUrl: './updatenotification.component.html',
  styleUrls: ['./updatenotification.component.scss'],
})
export class UpdatenotificationComponent implements OnInit {
  @Input() notification: Notification;

  validationRules = {
    required: ['name', 'description', 'order', 'priority', 'state'],
  };

  file: File = null;
  fileName: string;

  Categoris: {
    id: string;
    name: string;
    description: string;
    state: string;
  }[];
  Orders: {
    id: string;
    value: string;
    state: string;
  }[];

  Priorities: {
    id: string;
    name: string;
    state: string;
  }[];
  constructor(
    public pushService: PushService,
    public toastController: ToastController,
    private firestore: AngularFirestore,
    private modalController: ModalController,
    public loadingController: LoadingController,
    public imgService: ImgService
  ) {}

  ngOnInit() {
    this.infocategory();
    this.infoOrder();
    this.infoPriority();
  }
  infocategory() {
    this.firestore
      .collection('/category', (ref) => ref.where('state', '==', 'public'))
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.Categoris = res.map((e) => {
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              description: e.payload.doc.data()['description'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });
  }
  infoPriority() {
    this.firestore
      .collection('/priority', (ref) =>
        ref.where('state', '==', 'public').orderBy('name', 'asc')
      )
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.Priorities = res.map((e) => {
            return {
              id: e.payload.doc.id,
              name: e.payload.doc.data()['name'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });
  }

  infoOrder() {
    this.firestore
      .collection('/order', (ref) =>
        ref.where('state', '==', 'public').orderBy('value', 'asc')
      )
      .snapshotChanges()
      .subscribe((res) => {
        if (res) {
          this.Orders = res.map((e) => {
            return {
              id: e.payload.doc.id,
              value: e.payload.doc.data()['value'],
              state: e.payload.doc.data()['state'],
            };
          });
        }
      });
  }
  async updateNotification() {
    if (this.file !== null) {
      this.notification.img = await this.imgService.uploadImage(
        'notification',
        this.file
      );
    }

    this.notification.updateDate = firebase.firestore.Timestamp.now().toDate();
    this.firestore
      .doc('/notification/' + this.notification.uid)
      .update(this.notification)
      .then(() => {
        this.mensaje("Notificación actualizada correctamente")
        this.quit();
        this.mensaje('Noticia Actualizada');
      });
  }

  quit() {
    this.modalController.dismiss();
  }

  enviar() {
    this.pushService.onesignal(
      this.notification.name,
      this.notification.name,
      this.notification.description,
      this.notification.uid
    );
    this.quit();
    this.mensaje('Notificación enviada');
  }

  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000,
    });
    toast.present();
  }

  uploadImageTemporary($event) {
    this.imgService.uploadImgTemporary($event).onload = (event: any) => {
      this.notification.img = event.target.result;
      this.file = $event.target.files[0];
      this.fileName = $event.target.files[0].name;
    };
  }
}
