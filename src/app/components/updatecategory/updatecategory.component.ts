import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, ToastController } from '@ionic/angular';
@Component({
  selector: 'app-updatecategory',
  templateUrl: './updatecategory.component.html',
  styleUrls: ['./updatecategory.component.scss'],
})
export class UpdatecategoryComponent implements OnInit {
  @Input() id: string;
  @Input() name: string;
  @Input() description: string;
  @Input() state: string;

  constructor(private firebase: AngularFirestore, public toastController: ToastController, private modalController: ModalController) { }

  ngOnInit() { }

  update(name, description, state) {
    if (this.name != '' && this.description != '' && this.state != '') {
      let category = {};
      category['name'] = name;
      category['description'] = description;
      category['state'] = state;

      this.firebase.doc('/category/' + this.id).update(category).then(() => {
        this.quit();
        this.mensaje("Categoria Actualizada");
      });
    } else {

      this.mensaje("Todos los datos debe estar llenos");
    }

  }
  quit() {
    this.modalController.dismiss();
  }

  async mensaje(parametro: string) {
    const toast = await this.toastController.create({
      message: parametro,
      duration: 2000
    });
    toast.present();
  }
}
