import { levels } from "./levels";
import { parallel } from "./parallel.interface";

export interface career {
    uid?: string;
    name?: string;
    description?: string;
    levels?: levels[];
    parallel?: parallel[];
    tittleType?: string;
    isActivate?: string;
    createdAt?: Date;
    updatedAt?: Date;
}
