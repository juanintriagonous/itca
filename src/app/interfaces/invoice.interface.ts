import { bank } from "./bank.interface";

export interface invoice{
    uid?: string;//id de la institucion
    useruid?: string;//id del usuario que registra el comprobante
    studentName?: string;//nombre del estudiante***********************
    identifynumber?: string;//nro de identificacion
    bank?: string;//nombre del banco al que pertenece
    bankData?: bank; //dato tipo bank
    img?: string;//url de la fotografia del comprobante
    detail?: string;//detalle del comprobante
    reason?: string;//Motivo del pago
    date?: string;// fecha de ingreso
    career?: string;//carrera del estudiante***************************
    parallel?: string;//paralelo de la carrera
    resolution?: string;//estado de revision***************************
    level?: string;//nivel del estudiante******************************
    value?: string;//valor del comprobante
    createAt?: Date;//fecha de creacion********************************
    updateAt?: Date;//fecha de modificacion
}