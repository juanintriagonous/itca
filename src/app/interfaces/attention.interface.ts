export interface Attention{
    opening: Date;
    closing: Date;
    closed: boolean;
    close: boolean;
}