export interface Notification {
    uid?: string;
    name?: string;
    description?: string;
    order?: string;
    hyper?: string;
    category?: string;
    img?: string;
    priority?: string;
    creationDate?: Date;
    updateDate?: Date;
    state?: string;
}
