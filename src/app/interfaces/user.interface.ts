import { Attention} from './attention.interface';
import { career } from './career';
import { levels } from './levels';

export interface User {
    uid?: string;
    fullname?: string;//nombres completos del usuario
    identificationDocument?: string;//nro de cedula
    email: string;//email del usuario
    phoneNumber?: string;//numero telefonico
    role?: string;//tipo de usuario estudiante - admin 
    address?: string;//direccion 
    dateOfBirth?: Date;//fecha de nacimiento    
    image?: string;//fotografia 
    gender?: string;//genero
    career?: string;//carrera 
    level?: string;//nivel
    parallel?: string;//
    isActive?: boolean;//estado del usuario dentor del sistema
    createAt?: Date;//fecha de creacion
    updateAt?: Date;//fecha de actualizacion
  }