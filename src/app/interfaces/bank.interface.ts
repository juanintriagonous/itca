export interface bank{
    uid?: string;//id de la institucion
    name?: string;//nombre de la institucion
    accountNumber?: string;//tipo de identificador de numero por ejemplo nro de comprobante, nro de referencia etc
    helpImg?: string;//imagen de 
    isActivate?: string;//estado de la isntitucion dentro del sistema
    createdAt?: Date;//fecha de creacion
    updatedAt?: Date;//fecha de modificacion
}