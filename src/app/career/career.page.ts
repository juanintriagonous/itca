import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { UpdateCareerComponent } from '../components/update-career/update-career.component';
import { career } from '../interfaces/career';
import { AlertService } from '../services/alert.service';
import { CareerService } from '../services/career.service';
import { validateForm } from '../shared/utilities/formValidation';


@Component({
  selector: 'app-career',
  templateUrl: './career.page.html',
  styleUrls: ['./career.page.scss'],
})
export class CareerPage implements OnInit {

  career: career;
  validationRules = {
    required: ['name'
      , 'description'
      , 'tittleType'
      , 'isActivate'],
  };
  errors: any = {
    name: null,
    description: null,
    tittleType: null,
    isActivate: null
  }


  careers: career[];


  modalIcon: HTMLIonModalElement

  constructor(
    private firebase: AngularFirestore,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private alertCtrl: AlertController,
    private router: Router,
    private alertService: AlertService,
    private toastController: ToastController,
    private careerService: CareerService
  ) {
    this.initCareer();
  }

  ngOnInit() {    
    this.careerService.getCareers().subscribe(res =>{
      this.careers = res as career[];
      if(!this.careers){
        const message = 'No exiten Careeras registrados 😣';
      }
    })
  }

  initCareer() {
    //careras obj
    this.career = {
      name: '',
      description: '',
      levels: [
        { uid: '1', name: 'Primer Periodo', isActivate: false },
        { uid: '2',  name: 'Segundo Periodo', isActivate: false },
        { uid: '3',  name: 'Tercer Periodo', isActivate: false },
        { uid: '4',  name: 'Cuarto Periodo', isActivate: false },
        { uid: '5',  name: 'Quinto Periodo', isActivate: false },
        { uid: '6',  name: 'Sexto Periodo', isActivate: false }
      ],
      parallel: [
        { uid: 'A',  name: 'Paralelo A', isActivate: false },
        { uid: 'B',  name: 'Paralelo B', isActivate: false },
        { uid: 'C',  name: 'Paralelo C', isActivate: false },
        { uid: 'D',  name: 'Paralelo D', isActivate: false },
        { uid: 'E',  name: 'Paralelo E', isActivate: false },
        { uid: 'F',  name: 'Paralelo F', isActivate: false }
      ],
      tittleType: '',
      isActivate: '',
      createdAt: null,
      updatedAt: null,
    }
  }

  //Add career (insert)
  async addCareer() {
    if (!this.validForm()) {
      return;
    }
    await this.alertService.presentLoading('Creando Carrera...')
    this.careerService.addCareer(this.career);
    this.alertService.loading.dismiss();
    this.initCareer();
  }

  //clear data
  clearCareer() {

  }

  //update
  async UpdateCareer(career: career) {    
    const modal = await this.modalController.create({
      component: UpdateCareerComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        career
      }
    });
    return await modal.present();
  }

  /**
     * Método para validar los campos
     * @returns Retorna verdadero si no hay errores y falso si hay algún error
     */
  validForm() {
    const errors = validateForm(this.career, this.validationRules);
    this.errors = errors;
    const validForm = Object.keys(errors).length;
    return !validForm;
  }
  //delete career
   async DeleteCareer(carrerUid) {
     const confirmDelete = await this.alertService.presentAlertConfirm(
       'ITCA App',
      'Desea Eliminar esta carrera?'
     );
     if(confirmDelete){
      this.careerService.deleteCareer(carrerUid);
      this.alertService.presentAlert('Carrera Eliminada🤯')
     }    
  }

  async mensaje(title: string, messag: string) {
    const alert = await this.alertCtrl.create({
      message: messag,
      subHeader: title,
      buttons: [{ text: 'Aceptar', role: 'cancel' }],
    });
    await alert.present();
  }

  goBack() {
    this.modalIcon && this.modalIcon.dismiss();
    this.router.navigate(['/main'])
  }

}
