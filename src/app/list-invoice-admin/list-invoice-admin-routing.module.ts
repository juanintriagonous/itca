import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListInvoiceAdminPage } from './list-invoice-admin.page';

const routes: Routes = [
  {
    path: '',
    component: ListInvoiceAdminPage
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListInvoiceAdminPageRoutingModule {}
