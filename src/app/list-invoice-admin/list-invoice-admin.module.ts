import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListInvoiceAdminPageRoutingModule } from './list-invoice-admin-routing.module';

import { ListInvoiceAdminPage } from './list-invoice-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListInvoiceAdminPageRoutingModule
  ],
  declarations: [ListInvoiceAdminPage]
})
export class ListInvoiceAdminPageModule {}
