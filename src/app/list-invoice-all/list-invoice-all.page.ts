import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { InvoicedetailsComponent } from '../components/invoicedetails/invoicedetails.component';
import { bank } from '../interfaces/bank.interface';
import { invoice } from '../interfaces/invoice.interface';
import { BanksService } from '../services/banks.service';
import { UpinvoiceService } from '../services/upinvoice.service';
//xls export
import { XlsService } from '../services/xls.service';

@Component({
  selector: 'app-list-invoice-all',
  templateUrl: './list-invoice-all.page.html',
  styleUrls: ['./list-invoice-all.page.scss'],
})
export class ListInvoiceAllPage implements OnInit {

 
  //idbank  = document.getElementById('bankid');
  bank: bank;
  invoice: invoice;
  invoices: invoice[];
  banks: bank[];
  banco: string;

  constructor(
    private upinvoiceService: UpinvoiceService,
    private router: Router,
    private modalController: ModalController,
    private bankService: BanksService,
    private xlsService: XlsService
  ) { }

  ngOnInit() {
    this.getInvoices();
    this.getBankbyId(this.invoice?.bank);
  }

  getInvoices() {
    this.upinvoiceService.getAllInvoices().subscribe(res => {
      this.invoices = res as invoice[]
      
      // if (!this.invoices) {
      //   const message = 'No exiten Comprobantes registrados 😣';
      // }
    })
  }
  getBankbyId(bankUid: string) {
   return this.bankService.getBankByUid(bankUid).subscribe(res => {
      this.bank = res as bank
      this.banco = this.bank.name
      console.log(this.banco);      
    })
  }

  exportXLS(){
    this.xlsService.exportToExcel(this.invoices, 'pagos recibidos');
  }

  //update
  async UpdateInvoice(id, name, description, levels, tittleType, isActivate) {
    const modal = await this.modalController.create({
      component: InvoicedetailsComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'id': id,
        'name': name,
        'description': description,
        'levels': levels,
        'tittleType': tittleType,
        'isActivate': isActivate,
      }
    });
    return await modal.present();
  }

  goToSeeVoucher(invoice: invoice) {
    this.router.navigate(['see-detail-invoice/', invoice.uid])
  }

}
