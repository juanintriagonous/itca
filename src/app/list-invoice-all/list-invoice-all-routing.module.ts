import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListInvoiceAllPage } from './list-invoice-all.page';

const routes: Routes = [
  {
    path: '',
    component: ListInvoiceAllPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListInvoiceAllPageRoutingModule {}
