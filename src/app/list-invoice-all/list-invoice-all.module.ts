import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListInvoiceAllPageRoutingModule } from './list-invoice-all-routing.module';

import { ListInvoiceAllPage } from './list-invoice-all.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListInvoiceAllPageRoutingModule
  ],
  declarations: [ListInvoiceAllPage]
})
export class ListInvoiceAllPageModule {}
