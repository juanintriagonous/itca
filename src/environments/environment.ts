// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyClCc1klRd99Jg3MpsnRJpcAHRLraXDZT0",
    authDomain: "itca-2674a.firebaseapp.com",
    projectId: "itca-2674a",
    storageBucket: "itca-2674a.appspot.com",
    messagingSenderId: "325051574126",
    appId: "1:325051574126:web:2cd6862052229a09e6037f",
    measurementId: "G-P33JE48VY4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
